-- MySQL dump 10.13  Distrib 5.6.47, for Linux (x86_64)
--
-- Host: localhost    Database: letsgo
-- ------------------------------------------------------
-- Server version	5.6.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `letsgo`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `letsgo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `letsgo`;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
                       `id_car` int(10) unsigned NOT NULL AUTO_INCREMENT,
                       `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
                       `category` enum('economy','standard','comfort') COLLATE utf8mb4_unicode_ci NOT NULL,
                       `seat_count` tinyint(3) unsigned NOT NULL DEFAULT '1',
                       `status` enum('pending_order','in_cruise','inactive') COLLATE utf8mb4_unicode_ci NOT NULL,
                       PRIMARY KEY (`id_car`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,'AE1235AA','economy',4,'inactive'),(2,'AE1236AA','standard',2,'in_cruise'),(3,'AE1237AA','economy',4,'inactive'),(4,'AE1238AA','comfort',1,'pending_order'),(5,'AE1239AA','comfort',4,'inactive'),(6,'AE1240AA','economy',3,'pending_order'),(7,'AE1241AA','standard',4,'inactive'),(8,'AE1242AA','economy',1,'pending_order'),(9,'AE1243AA','economy',4,'inactive'),(10,'AE1244AA','standard',2,'pending_order'),(11,'AE1245AA','standard',4,'inactive'),(12,'AE1246AA','economy',4,'inactive'),(13,'AE1247AA','standard',3,'pending_order'),(14,'AE1248AA','economy',4,'inactive'),(15,'AE1249AA','economy',2,'inactive'),(16,'AE1250AA','standard',4,'inactive'),(17,'AE1251AA','economy',3,'pending_order'),(18,'AE1252AA','comfort',4,'inactive'),(19,'AE1253AA','economy',3,'pending_order'),(20,'AE1254AA','economy',4,'inactive'),(21,'AE1255AA','comfort',2,'pending_order'),(22,'AE1256AA','economy',4,'inactive'),(23,'AE1257AA','standard',1,'pending_order'),(24,'AE1258AA','economy',2,'pending_order'),(25,'AE1259AA','comfort',4,'pending_order');
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER car_before_delete
    BEFORE DELETE
    ON car FOR EACH ROW
BEGIN
    DECLARE orderCount INT;

    SET orderCount = (SELECT COUNT(*) FROM `order` WHERE `order`.`id_car` = OLD.`id_car`);

    IF (orderCount > 0) THEN
        CALL FORBIDDEN_TO_DELETE_CARS;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
                            `id_location` int(10) unsigned NOT NULL AUTO_INCREMENT,
                            `name_en` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `name_ru` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `x` smallint(6) NOT NULL,
                            `y` smallint(6) NOT NULL,
                            PRIMARY KEY (`id_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Shevchenko ln.','Шевченко Инн.',1,1),(2,'Vokzalna sq.','Вокзальная площадь',2,1),(3,'Uspenska sq.','Успенская кв.',3,1),(4,'Kniahyni Olhy st.','Княгини Ольги',4,1),(5,'Yuliusha Slovatskoho st.','Юлиуша Словацкого, ул.',5,1),(6,'Tsentralna st.','Центральная, ул.',6,1),(7,'Lyvarna st.','Лыварна.',7,1),(8,'Pivdennyi ln.','Пивденный.',8,1),(9,'Kharkivska st.','Харьковская ул.',9,1),(10,'Hlinky st.','Глинки.',10,1),(11,'Volodymyra Monomakha st.','Владимира Мономаха.',1,2),(12,'Sholom-Aleikhema st.','Шолом-Алейхема, ул.',2,2),(13,'Vokzalna st.','Вокзальная ул.',3,2),(14,'Voskresenska st.','Воскресенская ул.',4,2),(15,'Soborna sq.','Соборная площадь',5,2),(16,'Kniazia Yaroslava Mudroho st.','Князя Ярослава Мудрого, ул.',6,2),(17,'Starokozatska st.','Старокозацкая ул.',7,2),(18,'Lamana st.','Ламана',8,2),(19,'Andriia Fabra st.','Андрия Фабра ул.',9,2),(20,'Mostova st.','Мостова ул.',10,2),(21,'Stoliarova st.','Столярова ул.',1,3),(22,'Yevropeiskyi blvd.','Европейский бульвар.',2,3),(23,'Riznychna st.','Ризничная ул.',3,3),(24,'Korolenko st.','Короленко, ул.',4,3),(25,'Sichovykh Striltsiv st.','Сичовых Стрильцов.',5,3),(26,'Mahdeburskoho prava st.','Махдебурское Права, ул.',6,3),(27,'Cheliuskina st.','Челюскина.',7,3),(28,'Serhiia Yefremova st.','Сергея Ефремова.',8,3),(29,'Polia st.','Поля, ул.',9,3),(30,'Dmytra Yavornytskoho ave.','Дмитрия Яворницкого пр.',10,3),(31,'Pysarzhevskoho st.','Писаржевского.',1,4),(32,'Pivdenna st.','Пивденна ул.',2,4),(33,'Hoholia st.','Хохолия ул.',3,4),(34,'Yevhena Konovaltsia ln.','Евгения Коновальця.',4,4),(35,'Shmidta st.','Шмидта, ул.',5,4),(36,'Mechnikova st.','Мечникова ул.',6,4),(37,'Viacheslava Lypynskoho st.','Вячеслава Лыпинского, ул.',7,4),(38,'Yavornytskoho st.','Яворницкого.',8,4),(39,'Dmytra Dontsova st.','Дмитрия Донцова, ул.',9,4),(40,'Yevropeiska st.','Европеиска, ул.',10,4),(41,'Volodymyra Vernatskoho st.','Владимира Вернацкого.',1,5),(42,'Olesia Honchara st.','Олеся Гончара ул.',2,5),(43,'Vasylia Chaplenko st.','Василия Чапленко, ул.',3,5),(44,'Volodymyra Mosokovskoho st.','Владимира Мосоковского, ул.',4,5),(45,'Arkhitektora Dolnyka st.','Архитектора Долника.',5,5),(46,'Pastera st.','Пастера',6,5),(47,'Sicheslavska Naberezhna st.','Сичеславская Набережная, ул.',7,5),(48,'Kurchatova st.','ул. Курчатова',8,5),(49,'Troitska sq.','Троицкая площадь',9,5),(50,'Vosmoho bereznia st.','Восмого березня ул.',10,5),(51,'Troitska st.','Троицкая ул.',1,6),(52,'Mykhaila Hrushevskoho st.','Михаила Грушевского.',2,6),(53,'Pryvokzalna st.','Привокзальная ул.',3,6),(54,'Shevchenko st.','Шевченко',4,6),(55,'Vasylia Zhukovskoho st.','Василия Жуковского.',5,6),(56,'Mykhaila Kotsiubynskoho st.','Михаила Коцюбинского, ул.',6,6),(57,'Pavla Nirinberha st.','Павла Ниринберга ул.',7,6),(58,'Feodosiia Makarevskoho ln.','Феодосия Макаревского.',8,6),(59,'Skorykovskyi ln.','Скориковский л.',9,6),(60,'Patorzhynskoho st.','Паторжинского ул.',10,6),(61,'Volodymyra Vynnychenka st.','Владимира Винниченко.',1,7),(62,'Barrykadna st.','Баррикадна ул.',2,7),(63,'Krutohirnyi desc.','Крутогирный спуск.',3,7),(64,'Staromostova sq.','Старомостова, кв.',4,7),(65,'Chernyshevskoho st.','Чернышевского, ул.',5,7),(66,'Sviatoslava Khorobroho st.','Святослава Хороброго.',6,7),(67,'Ivana Akinfieva st.','Ивана Акинфиева.',7,7);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER location_before_delete
    BEFORE DELETE
    ON location FOR EACH ROW
BEGIN
    CALL FORBIDDEN_TO_DELETE_LOCATIONS;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
                         `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `id_user` int(10) unsigned NOT NULL,
                         `id_car` int(10) unsigned NOT NULL,
                         `id_location_from` int(10) unsigned NOT NULL,
                         `id_location_to` int(10) unsigned NOT NULL,
                         `price` decimal(10,2) NOT NULL DEFAULT '0.00',
                         `discount_price` decimal(10,2) NOT NULL DEFAULT '0.00',
                         `distance` float NOT NULL DEFAULT '0',
                         `status` enum('pending','process','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
                         `created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `arrived_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                         `completed_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                         PRIMARY KEY (`id_order`),
                         KEY `order_FK` (`id_location_from`),
                         KEY `order_FK_1` (`id_location_to`),
                         KEY `order_FK_2` (`id_user`),
                         KEY `order_FK_3` (`id_car`),
                         CONSTRAINT `order_FK` FOREIGN KEY (`id_location_from`) REFERENCES `location` (`id_location`),
                         CONSTRAINT `order_FK_1` FOREIGN KEY (`id_location_to`) REFERENCES `location` (`id_location`),
                         CONSTRAINT `order_FK_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
                         CONSTRAINT `order_FK_3` FOREIGN KEY (`id_car`) REFERENCES `car` (`id_car`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER order_before_insert
    BEFORE INSERT
    ON `order` FOR EACH ROW
BEGIN

    IF (new.`status` <> 'pending') THEN
        CALL WRONG_ORDER_STATUS_PASSED;
    END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER order_after_insert
    AFTER INSERT
    ON `order` FOR EACH ROW
BEGIN
    IF (new.`status` = 'pending') THEN
        UPDATE `car` SET `status` = 'in_cruise' WHERE `car`.`id_car` = NEW.`id_car`;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER order_before_update
    BEFORE UPDATE
    ON `order` FOR EACH ROW
BEGIN
    if (new.`id_car` <> old.`id_car`) THEN
        call forbidden_to_change_id_car;
    end IF;

    if (new.`id_user` <> old.`id_user`) THEN
        call forbidden_to_change_id_user;
    end IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER order_after_update
    AFTER UPDATE
    ON `order` FOR EACH ROW
BEGIN
    IF (NEW.`status` = 'completed') THEN
        UPDATE `car` SET `status` = 'pending_order' WHERE `car`.`id_car` = OLD.`id_car`;
    END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER order_before_delete
    BEFORE DELETE
    ON `order` FOR EACH ROW
BEGIN
    CALL FORBIDDEN_TO_DELETE_ORDERS;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
                        `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `password` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `role` enum('admin','user') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                        `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        PRIMARY KEY (`id_user`),
                        UNIQUE KEY `user_UN` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@gmail.com','��g2��\"=�b�Co[','admin','2021-02-13 10:51:15');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER user_before_delete
    BEFORE DELETE
    ON `user` FOR EACH ROW
BEGIN
    DECLARE orderCount INT;

    SET orderCount = (SELECT COUNT(*) FROM `order` WHERE `order`.`id_user` = OLD.`id_user`);

    IF (orderCount > 0) THEN
        CALL FORBIDDEN_TO_DELETE_USERS;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-23 10:20:45
