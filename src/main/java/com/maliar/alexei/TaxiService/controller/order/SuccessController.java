package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SuccessController extends ActionController {
    private final OrderMaker orderMaker;

    public SuccessController(ServletContext servletContext, OrderMaker orderMaker) {
        super(servletContext);
        this.orderMaker = orderMaker;
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Entity order = this.orderMaker.getActiveOrder(request);

        if (null == order) {
            this.redirect("/order/booking", response);
            return;
        }

        Date d;

        if (((Order) order).getStatus().toString().equals("pending")) {
            d = ((Order) order).getArrivedDatetime();
        } else {
            d = ((Order) order).getCompletedDatetime();
        }

        long diffInMills = Math.abs(new Date().getTime() - d.getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMills, TimeUnit.MILLISECONDS);

        request.setAttribute("order", order);
        request.setAttribute("time", diff);

        this.renderJsp("order/success", request, response);
    }
}
