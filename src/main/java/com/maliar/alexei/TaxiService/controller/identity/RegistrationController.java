package com.maliar.alexei.TaxiService.controller.identity;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.dto.UserDto;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class RegistrationController extends ActionController {
    private final ObjectMapper userObjectMapper;

    private final AccessProvider accessProvider;

    private final DtoValidation dtoValidation;

    public RegistrationController(ServletContext servletContext, ObjectMapper userObjectMapper, DtoValidation dtoValidation, AccessProvider accessProvider) {
        super(servletContext);
        this.userObjectMapper = userObjectMapper;
        this.accessProvider = accessProvider;
        this.dtoValidation = dtoValidation;
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        this.renderJsp("identity/registration", request, response);
    }

    @Override
    public void postAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        UserDto userDto = (UserDto) this.userObjectMapper.dtoFromRequest(request);

        this.saveSubmitDataToSession(request, userDto);

        Map<String, List<String>> map = this.dtoValidation.validate(userDto, "en");

        if (!map.isEmpty()) {
            this.saveSubmitErrorsToSession(request, map);
        } else {
            try {
                this.accessProvider.register(this.userObjectMapper.entityFromDto(userDto), request);

                this.redirect("/identity/authentication", response);
            } catch (LogicException ex) {
                this.errorMessage(request, ex.getMessage());
            }
        }
    }
}
