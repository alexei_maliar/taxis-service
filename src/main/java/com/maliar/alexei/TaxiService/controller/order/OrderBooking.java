package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

interface OrderBooking {
    String CORRESPONDING_CAR_KEY = "corresponding_car";
    String SIMILAR_CARS_KEY = "similar_car";
    String ORDER_DTO_KEY = "order_dto";

    default Entity getCorrespondingCar(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException();
        }

        Object car = request.getSession(true).getAttribute(CORRESPONDING_CAR_KEY);

        if (car instanceof Entity) {
            return (Entity) car;
        }

        return null;
    }

    default List<Entity> getSimilarCars(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException();
        }

        Object res = request.getSession(true).getAttribute(SIMILAR_CARS_KEY);

        if ((res instanceof List)) {
            return (List<Entity>) request.getSession(true).getAttribute(SIMILAR_CARS_KEY);
        }

        return null;
    }

    default Dto getOrderDto(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException();
        }

        Object res = request.getSession(true).getAttribute(ORDER_DTO_KEY);

        if (res instanceof Dto) {
            return (Dto) res;
        }

        return null;
    }

    default void setCorrespondingCar(Entity car, HttpServletRequest request) {
        if (car == null || request == null) {
            throw new IllegalArgumentException();
        }

        request.getSession(true).setAttribute(CORRESPONDING_CAR_KEY, car);
    }

    default void setSimilarCars(List<Entity> cars, HttpServletRequest request) {
        if (cars == null || request == null) {
            throw new IllegalArgumentException();
        }

        request.getSession(true).setAttribute(SIMILAR_CARS_KEY, cars);
    }

    default void setOrderDto(Dto order, HttpServletRequest request) {
        if (order == null || request == null) {
            throw new IllegalArgumentException();
        }

        request.getSession(true).setAttribute(ORDER_DTO_KEY, order);
    }

    default void clearOrderBookingFromSession(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException();
        }

        HttpSession session = request.getSession(true);

        session.removeAttribute(CORRESPONDING_CAR_KEY);
        session.removeAttribute(SIMILAR_CARS_KEY);
        session.removeAttribute(ORDER_DTO_KEY);
    }
}
