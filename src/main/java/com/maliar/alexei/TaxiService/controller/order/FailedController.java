package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.controller.ActionController;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FailedController extends ActionController {
    public FailedController(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.renderJsp("order/failed", request, response);
    }
}
