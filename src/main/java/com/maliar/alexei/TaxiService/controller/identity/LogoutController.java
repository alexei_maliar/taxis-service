package com.maliar.alexei.TaxiService.controller.identity;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutController extends ActionController {
    private AccessProvider accessProvider;

    public LogoutController(ServletContext servletContext, AccessProvider accessProvider) {
        super(servletContext);
        this.accessProvider = accessProvider;
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.accessProvider.hasPrincipal(request)) {
            this.accessProvider.logout(request);
        }

        this.redirect("/identity/authentication", response);
    }
}
