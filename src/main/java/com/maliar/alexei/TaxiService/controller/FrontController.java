package com.maliar.alexei.TaxiService.controller;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.exception.ControllerNotFoundException;
import com.maliar.alexei.TaxiService.router.Router;
import org.apache.logging.log4j.LogManager;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 * Implementation of Front Controller pattern
 */
public final class FrontController extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServiceContainer container = ServiceContainer.getInstance();

        Router router = (Router) container.get(Router.class.getName());

        try {
            ActionController controller = router.match(req);

            controller.beforeDispatch(req, resp);

            if (req.getMethod().equalsIgnoreCase("post")) {
                controller.postAction(req, resp);
                // prg
                if (!resp.isCommitted()) {
                    resp.sendRedirect(req.getRequestURI());
                }
            } else if (req.getMethod().equalsIgnoreCase("get")) {
                controller.saveSessionDataToRequestAndFlush(req);
                controller.getAction(req, resp);
            } else {
                throw new ControllerNotFoundException();
            }
        } catch (ControllerNotFoundException ex) {
            LogManager.getRootLogger().debug(ex.getMessage(), ex);
            req.getRequestDispatcher("/WEB-INF/jsp/404.jsp").forward(req, resp);
        } catch (Exception ex) {
            LogManager.getRootLogger().error(ex.getMessage(), ex);
            req.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(req, resp);
        }
    }
}