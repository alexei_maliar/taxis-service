package com.maliar.alexei.TaxiService.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainController extends ActionController {
    public MainController(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        this.renderJsp("index", request, response);
    }
}
