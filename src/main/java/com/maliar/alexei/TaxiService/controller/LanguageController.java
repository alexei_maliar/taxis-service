package com.maliar.alexei.TaxiService.controller;

import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LanguageController extends ActionController {
    private  final MultiLanguageProvider multiLanguageProvider;

    public LanguageController(ServletContext servletContext, MultiLanguageProvider multiLanguageProvider) {
        super(servletContext);
        this.multiLanguageProvider = multiLanguageProvider;
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().contains("/ru")) {
            this.multiLanguageProvider.setLang(MultiLanguageProvider.RU, response);
        } else {
            this.multiLanguageProvider.setLang(MultiLanguageProvider.EN, response);
        }

        this.redirect("/", response);
    }
}
