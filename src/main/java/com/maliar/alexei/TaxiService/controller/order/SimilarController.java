package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.model.dto.OrderDto;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;
import com.maliar.alexei.TaxiService.service.trip.exception.CarNotFoundException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimilarController extends ActionController implements OrderBooking {
    private final OrderMaker orderMaker;

    public SimilarController(ServletContext servletContext, OrderMaker orderMaker) {
        super(servletContext);
        this.orderMaker = orderMaker;
    }

    @Override
    public void beforeDispatch(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Entity order = this.orderMaker.getActiveOrder(request);

        if (null != order) {
            this.redirect("/order/success?id_order=" + ((Order) order).getIdOrder(), response);
        }
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Entity> similarCars = this.getSimilarCars(request);

        if (null == similarCars || similarCars.isEmpty() || null == this.getOrderDto(request)) {
            this.redirect("/order/booking", response);
            return;
        }

        OrderDto orderDto = (OrderDto) this.getOrderDto(request);

        List<Car> similarCategoryCars = new ArrayList<>();
        List<Car> similarSeatCountCars = new ArrayList<>();

        for (Entity car: similarCars) {
            Car c = (Car) car;

            if (!c.getCategory().toString().equals(orderDto.getCategory())) {
                similarSeatCountCars.add(c);
            } else {
                similarCategoryCars.add(c);
            }
        }

        request.setAttribute("similarCategoryCars", similarCategoryCars);
        request.setAttribute("similarSeatCountCars", similarSeatCountCars);

        this.renderJsp("order/similar", request, response);
    }

    @Override
    public void postAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (null == request.getParameter("id_car")  || null == this.getOrderDto(request)) {
            return;
        }

        OrderDto orderDto = (OrderDto) this.getOrderDto(request);

        try {
            Entity car = this.orderMaker.findCorrespondingCar(Long.parseLong(request.getParameter("id_car")));

            this.clearOrderBookingFromSession(request);

            this.setOrderDto(orderDto, request);
            this.setCorrespondingCar(car, request);

            this.redirect("/order/confirmation", response);
        } catch (CarNotFoundException ignored) {
        }
    }
}
