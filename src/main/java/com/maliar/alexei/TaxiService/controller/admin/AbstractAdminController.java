package com.maliar.alexei.TaxiService.controller.admin;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.exception.ControllerNotFoundException;
import com.maliar.alexei.TaxiService.model.Paginator;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

abstract public class AbstractAdminController extends ActionController {
    public static final String CONTROLLER_KEY = "current_controller";

    public static final String PAGINATOR_KEY = "paginator";

    private static final String ADD_ACTION_URL_PART = "/add";
    private static final String EDIT_ACTION_URL_PART = "/edit";
    private static final String DELETE_ACTION_URL_PART = "/delete";

    public AbstractAdminController(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public void beforeDispatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(CONTROLLER_KEY, this);
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.isGridAction(request)) {
            this.gridAction(request, response);
        } else if (this.isEditAction(request)) {
            this.editAction(request, response);
        } else if (this.isAddAction(request)) {
            this.addAction(request, response);
        } else if (this.isDeleteAction(request)) {
            this.deleteAction(request, response);
        } else {
            throw new ControllerNotFoundException();
        }
    }

    @Override
    public void postAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.isGridAction(request)) {
            this.gridAction(request, response);
        } else if (this.isEditAction(request)) {
            this.editAction(request, response);
        } else if (this.isAddAction(request)) {
            this.addAction(request, response);
        } else if (this.isDeleteAction(request)) {
            this.deleteAction(request, response);
        } else {
            throw new ControllerNotFoundException();
        }
    }

    public void gridAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ControllerNotFoundException();
    }

    public void addAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ControllerNotFoundException();
    }

    public void editAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ControllerNotFoundException();
    }

    public void deleteAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        throw new ControllerNotFoundException();
    }

    protected void setSelectResultToView(HttpServletRequest request, Paginator paginator) {
        request.setAttribute(PAGINATOR_KEY, paginator);
    }

    protected boolean isGridAction(HttpServletRequest request) {
        return !this.isEditAction(request) && !this.isDeleteAction(request) && !this.isAddAction(request);
    }

    protected boolean isAddAction(HttpServletRequest request) {
        return this.getCurrentUrl(request).contains(ADD_ACTION_URL_PART);
    }

    protected boolean isEditAction(HttpServletRequest request) {
        return this.getCurrentUrl(request).contains(EDIT_ACTION_URL_PART);
    }

    protected boolean isDeleteAction(HttpServletRequest request) {
        return this.getCurrentUrl(request).contains(DELETE_ACTION_URL_PART);
    }

    protected boolean isPost(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase("post");
    }

    protected String getCurrentUrl(HttpServletRequest request) {
        return request.getRequestURI().substring(request.getContextPath().length());
    }
}
