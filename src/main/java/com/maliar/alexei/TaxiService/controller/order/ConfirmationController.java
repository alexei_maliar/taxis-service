package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;
import com.maliar.alexei.TaxiService.service.trip.exception.UnavailableOrderCreateException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ConfirmationController extends ActionController implements OrderBooking {
    private final OrderMaker orderMaker;

    public ConfirmationController(ServletContext servletContext, OrderMaker orderMaker) {
        super(servletContext);
        this.orderMaker = orderMaker;
    }

    @Override
    public void beforeDispatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Entity order = this.orderMaker.getActiveOrder(request);

        if (null != order) {
            this.redirect("/order/success?id_order=" + ((Order) order).getIdOrder(), response);
            return;
        }

        if (null == this.getCorrespondingCar(request) || null == this.getOrderDto(request)) {
            this.redirect("/order/booking", response);
        }
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Entity order = this.makeNewOrderOrRedirectToFailed(request, response);

        if (null != order) {
            long diffInMills = Math.abs(new Date().getTime() - ((Order) order).getArrivedDatetime().getTime());
            long diff = TimeUnit.MINUTES.convert(diffInMills, TimeUnit.MILLISECONDS);

            request.setAttribute("order", order);
            request.setAttribute("time", diff);
        }

        this.renderJsp("order/confirmation", request, response);
    }

    @Override
    public void postAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Entity newOrder = this.makeNewOrderOrRedirectToFailed(request, response);

        try {
            this.orderMaker.saveOrder(newOrder);
            this.clearOrderBookingFromSession(request);
        } catch (UnavailableOrderCreateException ex) {
            this.redirect("/order/failed", response);
        }
    }

    private Entity makeNewOrderOrRedirectToFailed(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            return this.orderMaker.makeOrder(this.getOrderDto(request), this.getCorrespondingCar(request), request);
        } catch (UnavailableOrderCreateException ex) {
            this.redirect("/order/failed", response);
            return null;
        }
    }
}
