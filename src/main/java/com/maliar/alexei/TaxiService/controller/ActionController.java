package com.maliar.alexei.TaxiService.controller;

import com.maliar.alexei.TaxiService.model.dto.Dto;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Map;

abstract public class ActionController {
    public static final String SUBMIT_ERRORS_MAP_KEY = "submitErrorsMap";
    public static final String SUBMIT_DATA_KEY = "submitData";
    public static final String ERROR_MESSAGE_KEY = "errorMessage";
    public static final String INFO_MESSAGE_KEY = "infoMessage";

    private final ServletContext servletContext;

    public ActionController(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public void beforeDispatch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // default method impl
    }

    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.dispatch404Error(request, response);
    }

    public void postAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        this.dispatch404Error(request, response);
    }

    public void saveSubmitDataToSession(HttpServletRequest request, Dto dto) {
        request.getSession(true).setAttribute(SUBMIT_DATA_KEY, dto);
    }

    protected ServletContext getServletContext() {
        return this.servletContext;
    }

    protected void renderJsp(String viewPath, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!response.isCommitted()) {
            request.getRequestDispatcher(getViewPath(viewPath)).forward(request, response);
        }
    }

    protected void redirect(String url, HttpServletResponse response) throws IOException {
        if (!response.isCommitted()) {
            response.sendRedirect(this.servletContext.getContextPath() + url);
        }
    }
    protected String getViewPath(String path) {
        return "/WEB-INF/jsp/" + path + ".jsp";
    }

    protected void dispatch404Error(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(getViewPath("404")).forward(request, response);
    }

    protected void saveSubmitErrorsToSession(HttpServletRequest request, Map<String, List<String>> errorsMap) {
        request.getSession(true).setAttribute(SUBMIT_ERRORS_MAP_KEY, errorsMap);
    }

    protected void errorMessage(HttpServletRequest request, String message) {
        request.getSession(true).setAttribute(ERROR_MESSAGE_KEY, message);
    }

    protected void infoMessage(HttpServletRequest request, String message) {
        request.getSession(true).setAttribute(INFO_MESSAGE_KEY, message);
    }

    protected void saveSessionDataToRequestAndFlush(HttpServletRequest request) {
        HttpSession session = request.getSession(true);

        // submit data
        if (session.getAttribute(SUBMIT_DATA_KEY) != null) {
            request.setAttribute(SUBMIT_DATA_KEY, session.getAttribute(SUBMIT_DATA_KEY));
            session.removeAttribute(SUBMIT_DATA_KEY);
        }

        // submit errors
        if (session.getAttribute(SUBMIT_ERRORS_MAP_KEY) != null) {
            request.setAttribute(SUBMIT_ERRORS_MAP_KEY, session.getAttribute(SUBMIT_ERRORS_MAP_KEY));
            session.removeAttribute(SUBMIT_ERRORS_MAP_KEY);
        }

        // error
        if (session.getAttribute(ERROR_MESSAGE_KEY) != null) {
            request.setAttribute(ERROR_MESSAGE_KEY, session.getAttribute(ERROR_MESSAGE_KEY));
            session.removeAttribute(ERROR_MESSAGE_KEY);
        }

        // info ...
        if (session.getAttribute(INFO_MESSAGE_KEY) != null) {
            request.setAttribute(INFO_MESSAGE_KEY, session.getAttribute(INFO_MESSAGE_KEY));
            session.removeAttribute(INFO_MESSAGE_KEY);
        }
    }
}
