package com.maliar.alexei.TaxiService.controller.admin;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.maliar.alexei.TaxiService.exception.ControllerNotFoundException;
import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;
import com.maliar.alexei.TaxiService.service.OrderExporter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class OrderController extends AbstractAdminController {
    public static final String FILTER_KEY = "filterMap";
    public static final String SORT_KEY = "sortMap";

    private final OrderDao orderDao;

    private final UserDao userDao;

    private final List<String> allowedFilterFields = new ArrayList<>();
    private final List<String> allowedSortFields = new ArrayList<>();

    public OrderController(ServletContext servletContext, OrderDao orderDao, UserDao userDao) {
        super(servletContext);
        this.orderDao = orderDao;
        this.userDao = userDao;

        allowedFilterFields.add("id_user");
        allowedFilterFields.add("created_datetime");
        allowedFilterFields.add("arrived_datetime");
        allowedFilterFields.add("completed_datetime");

        allowedSortFields.add("id_order");
        allowedSortFields.add("created_datetime");
        allowedSortFields.add("price");
        allowedSortFields.add("discount_price");
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.getCurrentUrl(request).contains("/filter")) {
            this.filterAction(request, response);
        } else if (this.getCurrentUrl(request).contains("/sort")) {
            this.sortAction(request, response);
        } else if (this.getCurrentUrl(request).contains("/export")) {
            this.exportAction(request, response);
        } else if (this.isGridAction(request)) {
            this.gridAction(request, response);
        } else {
            throw new ControllerNotFoundException();
        }
    }

    @Override
    public void gridAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;

        if (null != request.getParameter("page")) {
            try {
                page = Math.abs(Integer.parseInt(request.getParameter("page")));
            } catch (Exception ignored) {
            }
        }

        Map<Long, String> userMap = new HashMap<>();
        for (Entity user: this.userDao.selectAll()) {
            userMap.put(((User) user).getIdUser(), ((User) user).getEmail());
        }

        request.setAttribute("userEntrySet", userMap.entrySet());
        request.setAttribute(FILTER_KEY, this.getFilterMap(request));
        request.setAttribute(SORT_KEY, this.getSortMap(request));

        this.setSelectResultToView(
                request,
                this.orderDao.selectAllWithPaginator(
                        Paginator.getInstance(page),
                        this.getFilterMap(request),
                        this.getSortMap(request)
                )
        );
        this.renderJsp("admin/order/grid", request, response);
    }

    public void filterAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.setFilterMap(request);
        this.redirect("/admin/order", response);
    }

    public void sortAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.setSortMap(request);
        this.redirect("/admin/order", response);
    }

    public void exportAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String type = request.getParameter("type");

        if (null == type || (!type.equals("csv") && !type.equals("pdf"))) {
            throw new ControllerNotFoundException();
        }

        String filename;

        List<Entity> entities = this.orderDao.selectAll(this.getFilterMap(request), this.getSortMap(request));

        if (type.equals("csv")) {
            // csv
            filename = OrderExporter.exportCsv(entities, request);
        } else {
            // pdf
            filename = OrderExporter.exportPdf(entities, request);
        }

        if (null == filename) {
            this.redirect("/admin/order", response);
            return;
        }

        if (type.equals("csv")) {
            response.setContentType("text/plain");
        } else {
            response.setContentType("application/pdf");
        }

        response.setHeader("Content-disposition", "attachment; filename=" + filename);

        try (InputStream in = request.getServletContext().getResourceAsStream("/temp_data/" + filename);
             OutputStream out = response.getOutputStream()
        ) {
            byte[] buffer = new byte[1048];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        } catch (Exception ignored) {
        }
    }

    private Map<String, Object> getFilterMap(HttpServletRequest request) {
        if (request.getSession(true).getAttribute(FILTER_KEY) instanceof Map) {
            return (Map<String, Object>) request.getSession(true).getAttribute(FILTER_KEY);
        }

        return new HashMap<>();
    }

    private void setFilterMap(HttpServletRequest request) {
        Map<String, Object> filterMap = new HashMap<>();

        if (null == request.getParameter("clear")) {
            Enumeration<String> paramNames = request.getParameterNames();

            while (paramNames.hasMoreElements()) {
                String name = paramNames.nextElement();
                String val = request.getParameter(name);

                if (this.allowedFilterFields.contains(name)) {
                    if (name.equals("id_user") && !val.isEmpty()) {
                        filterMap.put(name, val);
                    } else {
                        try {
                            filterMap.put(name, new SimpleDateFormat("yyyy-MM-dd").parse(val));
                        } catch (Exception ignored) {
                        }
                    }
                }
            }
        }

        request.getSession(true).setAttribute(FILTER_KEY, filterMap);
    }
    
    private Map<String, String> getSortMap(HttpServletRequest request) {
        if (request.getSession(true).getAttribute(SORT_KEY) instanceof Map) {
            return (Map<String, String>) request.getSession(true).getAttribute(SORT_KEY);
        }

        return new HashMap<>();
    }

    private void setSortMap(HttpServletRequest request) {
        Map<String, Object> sortMap = new HashMap<>();

        Enumeration<String> paramNames = request.getParameterNames();

        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            String val = request.getParameter(name);

            if (this.allowedSortFields.contains(name) && !val.isEmpty()) {
                sortMap.put(name, val);
            }
        }

        request.getSession(true).setAttribute(SORT_KEY, sortMap);
    }
 }
