package com.maliar.alexei.TaxiService.controller.order;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.dto.OrderDto;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Location;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;
import com.maliar.alexei.TaxiService.service.trip.exception.CarNotFoundException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BookingController extends ActionController implements OrderBooking {
    private final OrderMaker orderMaker;

    private final ObjectMapper orderObjectMapper;

    private final DtoValidation dtoValidation;

    public BookingController(ServletContext servletContext, OrderMaker orderMaker, ObjectMapper orderObjectMapper, DtoValidation dtoValidation) {
        super(servletContext);
        this.orderMaker = orderMaker;
        this.orderObjectMapper = orderObjectMapper;
        this.dtoValidation = dtoValidation;
    }

    @Override
    public void beforeDispatch(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Entity order = this.orderMaker.getActiveOrder(request);

        if (null != order) {
            this.redirect("/order/success?id_order=" + ((Order) order).getIdOrder(), response);
        }
    }

    @Override
    public void getAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.clearOrderBookingFromSession(request);

        request.setAttribute("categoryList", Arrays.asList(Car.Category.values()));
        request.setAttribute("locationList", orderMaker.getAvailableLocationList());

        this.renderJsp("order/booking", request, response);
    }

    @Override
    public void postAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        OrderDto orderDto = (OrderDto) this.orderObjectMapper.dtoFromRequest(request);

        this.saveSubmitDataToSession(request, orderDto);

        Map<String, List<String>> map = this.dtoValidation.validate(orderDto, "en");

        List<Long> idLocationList = orderMaker.getAvailableLocationList()
                .stream()
                .map(e -> ((Location) e).getIdLocation())
                .collect(Collectors.toList());

        if (!idLocationList.contains(orderDto.getIdLocationTo()) || !idLocationList.contains(orderDto.getIdLocationFrom())) {
            this.errorMessage(request, "invalid_location_passed");
            return;
        }

        try {
            Car.Category.valueOf(orderDto.getCategory());
        } catch (IllegalArgumentException ex) {
            this.errorMessage(request, "invalid_category_passed");
            return;
        }

        if (map.isEmpty()) {
            try {
                Entity car = this.orderMaker.findCorrespondingCar(orderDto);

                this.setCorrespondingCar(car, request);
                this.setOrderDto(orderDto, request);

                this.redirect("/order/confirmation", response);
                return;
            } catch (CarNotFoundException ignored) {
            }

            try {
                List<Entity> similarCars = this.orderMaker.findSimilarCars(orderDto);

                this.setSimilarCars(similarCars, request);
                this.setOrderDto(orderDto, request);

                this.redirect("/order/similar", response);
                return;
            } catch (CarNotFoundException ignored) {
            }

            this.redirect("/order/failed", response);
            return;
        }

        this.saveSubmitErrorsToSession(request, map);
    }
}
