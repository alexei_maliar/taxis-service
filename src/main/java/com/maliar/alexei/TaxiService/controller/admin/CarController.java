package com.maliar.alexei.TaxiService.controller.admin;

import com.maliar.alexei.TaxiService.exception.DtoValidationException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.service.crud.CarCrudService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CarController extends AbstractAdminController {
    private final CarCrudService carCrudService;

    public CarController(ServletContext servletContext, CarCrudService carCrudService) {
        super(servletContext);
        this.carCrudService = carCrudService;
    }

    @Override
    public void gridAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.setSelectResultToView(request, this.carCrudService.readAll(request));
        this.renderJsp("admin/car/grid", request, response);
    }

    @Override
    public void addAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.isPost(request)) {
            try {
                this.carCrudService.create(request);
            } catch (DtoValidationException ex) {
                this.saveSubmitErrorsToSession(request, ex.getErrorMessagesMap());
                return;
            } catch (LogicException ex) {
                this.errorMessage(request, ex.getMessage());
            }

            this.redirect("/admin/car", response);
        } else {
            List<Car.Status> statusList = new ArrayList<>();
            statusList.add(Car.Status.inactive);
            statusList.add(Car.Status.pending_order);

            request.setAttribute("statusList", statusList);
            request.setAttribute("seatCountList", IntStream.range(1, 4).boxed().collect(Collectors.toList()));
            request.setAttribute("categoryList", Arrays.asList(Car.Category.values()));

            this.renderJsp("admin/car/add", request, response);
        }
    }

    @Override
    public void editAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.isPost(request)) {
            try {
                this.carCrudService.update(request);
            } catch (DtoValidationException ex) {
                this.saveSubmitErrorsToSession(request, ex.getErrorMessagesMap());
                this.redirect("/admin/car/edit?id_car=" + request.getParameter("id_car"), response);
                return;
            } catch (LogicException ex) {
                this.errorMessage(request, ex.getMessage());
            }

            this.redirect("/admin/car", response);
        } else {
            Entity entity = this.carCrudService.readOne(request);

            if (null == entity) {
                this.redirect("/admin/car", response);
                return;
            }

            List<Car.Status> statusList = new ArrayList<>();
            statusList.add(Car.Status.inactive);
            statusList.add(Car.Status.pending_order);

            request.setAttribute("entity", entity);

            request.setAttribute("statusList", statusList);
            request.setAttribute("categoryList", Arrays.asList(Car.Category.values()));
            request.setAttribute("seatCountList", IntStream.range(1, 4).boxed().collect(Collectors.toList()));

            this.renderJsp("admin/car/edit", request, response);
        }
    }

    @Override
    public void deleteAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            this.carCrudService.delete(request);
        } catch (LogicException ex) {
            this.errorMessage(request, ex.getMessage());
        }

        this.redirect("/admin/car", response);
    }
}
