package com.maliar.alexei.TaxiService.controller.admin;

import com.maliar.alexei.TaxiService.exception.DtoValidationException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;
import com.maliar.alexei.TaxiService.service.crud.UserCrudService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class UserController extends AbstractAdminController {
    private final UserCrudService userCrudService;

    public UserController(ServletContext servletContext, UserCrudService userCrudService) {
        super(servletContext);
        this.userCrudService = userCrudService;
    }

    @Override
    public void gridAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.setSelectResultToView(request, this.userCrudService.readAll(request));
        this.renderJsp("admin/user/grid", request, response);
    }

    @Override
    public void editAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.isPost(request)) {
            try {
                this.userCrudService.update(request);
            } catch (DtoValidationException ex) {
                this.saveSubmitErrorsToSession(request, ex.getErrorMessagesMap());
                this.redirect("/admin/user/edit?id_user=" + request.getParameter("id_user"), response);
                return;
            } catch (LogicException ex) {
                this.errorMessage(request, ex.getMessage());
            }

            this.redirect("/admin/user", response);
        } else {
            Entity entity = this.userCrudService.readOne(request);

            if (null == entity) {
                this.redirect("/admin/user", response);
                return;
            }

            request.setAttribute("entity", entity);

            request.setAttribute("roleList", Arrays.asList(User.Role.values()));

            this.renderJsp("admin/user/edit", request, response);
        }
    }
}
