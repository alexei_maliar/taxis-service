package com.maliar.alexei.TaxiService.view;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class FormElementValueTag extends SimpleTagSupport {
    private String getterSubmitData;

    private String getterEntity;

    public void setGetterEntity(String getterEntity) {
        this.getterEntity = getterEntity;
    }

    public void setGetterSubmitData(String getterSubmitData) {
        this.getterSubmitData = getterSubmitData;
    }

    @Override
    public void doTag() throws JspException, IOException {
        HttpServletRequest request = this.getHttpRequest();

        StringBuilder res = new StringBuilder();

        if (null != this.getHttpRequest().getAttribute("entity") && null != this.getterEntity) {
            try {
                Entity entity = (Entity) this.getHttpRequest().getAttribute("entity");
                res = new StringBuilder();
                res.append(entity.getClass().getDeclaredMethod(this.getterEntity).invoke(entity));
            } catch (Exception ex) {
                LogManager.getRootLogger().error(ex);
            }
        }

        if (null != this.getHttpRequest().getAttribute("submitData")) {
            try {
                Dto dto = (Dto) this.getHttpRequest().getAttribute("submitData");
                res = new StringBuilder();
                res.append(dto.getClass().getDeclaredMethod(this.getterSubmitData).invoke(dto));
            } catch (Exception ex) {
                LogManager.getRootLogger().error(ex);
            }
        }

        this.getJspContext().getOut().println(res.toString());
    }

    private PageContext getPageContext() {
        return (PageContext) this.getJspContext();
    }

    private HttpServletRequest getHttpRequest() {
        return (HttpServletRequest) this.getPageContext().getRequest();
    }
}
