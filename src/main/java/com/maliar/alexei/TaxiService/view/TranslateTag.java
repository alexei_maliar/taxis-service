package com.maliar.alexei.TaxiService.view;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class TranslateTag  extends SimpleTagSupport {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if (null == this.message) {
            throw new JspException("Message should be passed");
        }

        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        PageContext pageContext = (PageContext) this.getJspContext();

        this.getJspContext().getOut().println(multiLanguageProvider.translate(this.message, (HttpServletRequest) pageContext.getRequest()));
    }
}
