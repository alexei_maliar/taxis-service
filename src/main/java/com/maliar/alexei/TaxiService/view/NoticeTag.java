package com.maliar.alexei.TaxiService.view;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class NoticeTag extends SimpleTagSupport {
    @Override
    public void doTag() throws JspException, IOException {
        StringBuilder html = new StringBuilder();

        HttpServletRequest request = (HttpServletRequest) ((PageContext) this.getJspContext()).getRequest();

        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        if (null != request.getAttribute(ActionController.INFO_MESSAGE_KEY)) {
            html.append("<div class=\"notice-message-wrapper\">");
            html.append("<div class=\"notice-message info\">");
            html.append("<div class=\"icon\"><span></span></div>");
            html.append(multiLanguageProvider.translate((String) request.getAttribute(ActionController.INFO_MESSAGE_KEY), request));
            html.append("</div>");
            html.append("</div>");
        }

        if (null != request.getAttribute(ActionController.ERROR_MESSAGE_KEY)) {
            html.append("<div class=\"notice-message-wrapper\">");
            html.append("<div class=\"notice-message error\">");
            html.append("<div class=\"icon\"><span></span></div>");
            html.append(multiLanguageProvider.translate((String) request.getAttribute(ActionController.ERROR_MESSAGE_KEY), request));
            html.append("</div>");
            html.append("<script>");
            html.append("setTimeout(() => {\n" +
                    "        $('.notice-message-wrapper').remove();\n" +
                    "    }, 2000);");
            html.append("</script>");
            html.append("</div>");
        }
        this.getJspContext().getOut().println(html);
    }
}
