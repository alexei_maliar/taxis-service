package com.maliar.alexei.TaxiService.view;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class LinkTag extends SimpleTagSupport {
    private String className;

    private String url;

    private String value;

    public void setClassName(String className) {
        this.className = className;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringBuilder html = new StringBuilder();

        HttpServletRequest request = (HttpServletRequest) ((PageContext) this.getJspContext()).getRequest();

        if (null == className) {
            className = "";
        }

        MultiLanguageProvider ml = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        html.append("<a href=\"");
        html.append(request.getContextPath());
        if (null != this.url) {
            html.append(this.url);
        } else {
            StringWriter writer = new StringWriter();
            this.getJspBody().invoke(writer);
            html.append(writer.toString().trim());
        }

        html.append("\" class=\"");
        html.append(this.className);
        html.append("\">");
        html.append(ml.translate(this.value, request));
        html.append("</a>");


        this.getJspContext().getOut().println(html);
    }
}
