package com.maliar.alexei.TaxiService.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FormElementErrorTag extends SimpleTagSupport {
    private String name;


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringBuilder html = new StringBuilder();

        HttpServletRequest r = this.getHttpRequest();
        if (null != r.getAttribute("submitErrorsMap") && (r.getAttribute("submitErrorsMap") instanceof Map)) {
            Map<String, List<String>> errors = (Map<String, List<String>>) r.getAttribute("submitErrorsMap");

            if (errors.containsKey(this.name)) {
                html.append("<div class=\"input-error-wrapper\">");

                for (String msg : errors.get(name)) {
                    html.append("<div class=\"input-error\">");
                    html.append(msg);
                    html.append("</div>");
                }

                html.append("</div>");
            }
        }

        this.getJspContext().getOut().println(html.toString());
    }

    private PageContext getPageContext() {
        return (PageContext) this.getJspContext();
    }

    private HttpServletRequest getHttpRequest() {
        return (HttpServletRequest) this.getPageContext().getRequest();
    }
}
