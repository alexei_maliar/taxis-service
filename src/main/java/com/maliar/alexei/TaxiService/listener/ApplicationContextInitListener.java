package com.maliar.alexei.TaxiService.listener;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.di.ConfigParser;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.router.Router;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;
import com.maliar.alexei.TaxiService.service.trip.TripCalculator;
import com.maliar.alexei.TaxiService.service.trip.TripDispatcher;
import org.apache.logging.log4j.LogManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ApplicationContextInitListener implements ServletContextListener {
    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            this.initConfigRegistry(sce.getServletContext());

            // init di
            ServiceContainer serviceContainer = ServiceContainer.getInstance();
            serviceContainer.setConfigMap(ConfigParser.parse());
            serviceContainer.setServletContext(sce.getServletContext());

            // init router
            serviceContainer.get(Router.class.getName());

            // init database connection
            serviceContainer.get(DBManager.class.getName());

            // init access provider
            serviceContainer.get(AccessProvider.class.getName());

            // init translations
            serviceContainer.get(MultiLanguageProvider.class.getName());

            this.initTripCalculator(serviceContainer);

            this.initTripDispatcher(serviceContainer);
        } catch (Exception ex) {
            LogManager.getRootLogger().error(ex.getMessage(), ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.scheduledExecutorService.shutdownNow();
    }

    private void initConfigRegistry(ServletContext context) {
        ConfigRegistry.setProduction(Boolean.parseBoolean(context.getInitParameter("production-mode")));

        ConfigRegistry.setRoutesConfigFilename(context.getInitParameter("routes-config-file"));
        ConfigRegistry.setDiConfigFilename(context.getInitParameter("di-config-file"));
        ConfigRegistry.setAccessConfigFilename(context.getInitParameter("access-config-file"));
        ConfigRegistry.setCalculationConfigFilename(context.getInitParameter("calculation-config-file"));
        ConfigRegistry.setInternalizationConfigFilename(context.getInitParameter("internalization-config-file"));
    }

    private void initTripCalculator(ServiceContainer serviceContainer) {
        Properties calculationProperties = new Properties();

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(ConfigRegistry.getCalculationConfigFilename());

        try {
            calculationProperties.load(inputStream);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unavailable to init trip calculator", ex);
        }

        float tariffRate = Float.parseFloat(calculationProperties.getProperty("calculation.tariffRate", "10"));
        int averageSpeed = Integer.parseInt(calculationProperties.getProperty("calculation.averageSpeed", "40"));


        TripCalculator tripCalculator = (TripCalculator) serviceContainer.get(TripCalculator.class.getName());

        tripCalculator.setTariffRate(tariffRate);
        tripCalculator.setAverageSpeed(averageSpeed);
    }

    private void initTripDispatcher(ServiceContainer serviceContainer) {
        TripDispatcher tripDispatcher = (TripDispatcher) serviceContainer.get(TripDispatcher.class.getName());

        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        this.scheduledExecutorService.scheduleAtFixedRate(tripDispatcher, 25, 45, TimeUnit.SECONDS);
    }
}
