package com.maliar.alexei.TaxiService.filter;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceContainerImpl;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter which allows or disallows access(and redirect) to resource, based on authenticated user or not
 */
public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        AccessProvider accessProvider = (AccessProvider) ServiceContainer.getInstance().get(AccessProvider.class.getName());

        if (!accessProvider.isAuthenticationEndpoint(request)
                && !accessProvider.isRegistrationEndpoint(request)
                && !accessProvider.hasPrincipal(request)
                && !this.isResourcePath(request)
        ) {
            response.sendRedirect(request.getContextPath() + accessProvider.getAuthenticationEndpoint());
            return;
        }

        if (accessProvider.hasPrincipal(request)) {
            request.setAttribute("user", accessProvider.getPrincipal(request).getUser());

            if (!this.isResourcePath(request)
                    && (accessProvider.isAuthenticationEndpoint(request) || accessProvider.isRegistrationEndpoint(request))
            ) {
                response.sendRedirect(request.getContextPath() + "/");
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    protected boolean isResourcePath(HttpServletRequest request) {
        String requestedUri = request.getRequestURI().substring(request.getContextPath().length());

        return requestedUri.endsWith(".js") || requestedUri.endsWith(".css") || requestedUri.startsWith("/ru") || requestedUri.startsWith("/en");
    }
}
