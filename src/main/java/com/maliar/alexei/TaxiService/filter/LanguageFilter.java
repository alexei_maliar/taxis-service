package com.maliar.alexei.TaxiService.filter;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for defining ui language
 */
public class LanguageFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        if (null == multiLanguageProvider.getLang(request, false)) {
            multiLanguageProvider.setLang(
                    multiLanguageProvider.getLang(request, true),
                    response
            );
        }

        request.setAttribute("lang", multiLanguageProvider.getLang(request, false));

        filterChain.doFilter(request, response);
    }
}
