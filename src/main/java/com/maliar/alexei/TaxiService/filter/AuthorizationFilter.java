package com.maliar.alexei.TaxiService.filter;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceContainerImpl;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter allows or disallows access(and redirects) to resources, based on user role.
 * If user does not have according role, user will be redirected
 */
public class AuthorizationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        AccessProvider accessProvider = (AccessProvider) ServiceContainer.getInstance().get(AccessProvider.class.getName());

        boolean check = request.getRequestURI().substring(request.getContextPath().length()).startsWith("/en")
                || request.getRequestURI().substring(request.getContextPath().length()).startsWith("/ru");

        if (!accessProvider.isEndpointAllowed(request) && !check) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        filterChain.doFilter(request, response);
    }
}
