package com.maliar.alexei.TaxiService;

public class ConfigRegistry {
    private static boolean isProduction;

    private static String routesConfigFilename;

    private static String diConfigFilename;

    private static String accessConfigFilename;

    private static String calculationConfigFilename;

    private static String internalizationConfigFilename;

    public static boolean isProduction() {
        return isProduction;
    }

    public static void setProduction(boolean production) {
        isProduction = production;
    }

    public static String getRoutesConfigFilename() {
        if (null == routesConfigFilename) {
            throw new IllegalStateException("Routes config filename should be initialized");
        }

        return routesConfigFilename;
    }

    public static void setRoutesConfigFilename(String routesConfigFilename) {
        if (null == routesConfigFilename) {
            throw new IllegalArgumentException("Routes config filename should not be empty");
        }

        ConfigRegistry.routesConfigFilename = routesConfigFilename;
    }

    public static String getDiConfigFilename() {
        if (null == diConfigFilename) {
            throw new IllegalStateException("DI config filename should be initialized");
        }

        return diConfigFilename;
    }

    public static void setDiConfigFilename(String diConfigFilename) {
        if (null == diConfigFilename) {
            throw new IllegalArgumentException("DI config filename should not be empty");
        }

        ConfigRegistry.diConfigFilename = diConfigFilename;
    }

    public static String getAccessConfigFilename() {
        if (null == accessConfigFilename) {
            throw new IllegalStateException("Access config filename should be initialized");
        }

        return accessConfigFilename;
    }

    public static void setAccessConfigFilename(String accessConfigFilename) {
        if (null == accessConfigFilename) {
            throw new IllegalArgumentException("Access config filename should not be empty");
        }

        ConfigRegistry.accessConfigFilename = accessConfigFilename;
    }

    public static String getCalculationConfigFilename() {
        if (null == calculationConfigFilename) {
            throw new IllegalStateException("Calculation config filename should be initialized");
        }

        return calculationConfigFilename;
    }

    public static void setCalculationConfigFilename(String calculationConfigFilename) {
        if (null == calculationConfigFilename) {
            throw new IllegalArgumentException("Calculation config filename should not be empty");
        }

        ConfigRegistry.calculationConfigFilename = calculationConfigFilename;
    }

    public static String getInternalizationConfigFilename() {
        if (null == internalizationConfigFilename) {
            throw new IllegalStateException("Internalization config filename should be initialized");
        }

        return internalizationConfigFilename;
    }

    public static void setInternalizationConfigFilename(String internalizationConfigFilename) {
        if (null == internalizationConfigFilename) {
            throw new IllegalArgumentException("Internalization config filename should not be empty");
        }

        ConfigRegistry.internalizationConfigFilename = internalizationConfigFilename;
    }
}
