package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstraction for crud operation on DB
 */
abstract public class AbstractDao {
    protected final DBManager dbManager;

    protected final ObjectMapper mapper;

    public AbstractDao(DBManager dbManager, ObjectMapper mapper) {
        this.dbManager = dbManager;
        this.mapper = mapper;
    }

    abstract public void insert(Entity entity);

    abstract public void update(Entity entity);

    abstract public Entity select(long id);

    abstract public List<Entity> selectAll();

    abstract public Paginator selectAllWithPaginator(Paginator paginator);

    abstract public boolean delete(Entity entity);

    protected List<Entity> getResultList(PreparedStatement preparedStatement) {
        List<Entity> res = new ArrayList<>();

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                res.add(this.mapper.entityFromResultSet(resultSet));
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }

        return res;
    }

    protected long lastInsertId(PreparedStatement preparedStatement) {
        try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
            throw new SQLException("Result set is empty");
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    protected Entity selectById(String tableName, String primaryColName, long id) {
        if (id < 1) {
            throw new IllegalArgumentException("Id must be greater then 0");
        }

        return this.selectByCol(tableName, primaryColName, id);
    }

    protected Entity selectByCol(String tableName, String colName, Object val) {
        String query = "SELECT * FROM `" + tableName + "` WHERE `" + colName + "` = ? LIMIT 1";
        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setObject(1, val);

                List<Entity> res = this.getResultList(preparedStatement);

                if (res.size() > 0) {
                    return res.get(0);
                }
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }

        return null;
    }

    protected List<Entity> selectAllByTableName(String tableName) {
        String query = "SELECT * FROM `" + tableName + "`";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                return this.getResultList(preparedStatement);
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    protected Paginator selectAllByTableNameWithPaginator(String tableName, Paginator paginator) {
        try (Connection connection = this.dbManager.getConnection()) {
            paginator = this.initPaginator(tableName, paginator, connection);

            String query = "SELECT * FROM `" + tableName + "` LIMIT ?, ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, paginator.getOffset());
                preparedStatement.setInt(2, paginator.getItemsCountPerPage());

                paginator.setItems(this.getResultList(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }

        return paginator;
    }

    protected boolean deleteById(String tableName, String primaryColName, long id) {
        if (id < 1) {
            return false;
        }

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "DELETE FROM `" + tableName + "` WHERE `" + primaryColName + "` = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);

                return preparedStatement.executeUpdate() > 0;
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    protected Paginator initPaginator(String tableName, Paginator paginator, Connection connection) throws SQLException {
        int rowsCount;

        String countQuery = "SELECT COUNT(*) FROM `" + tableName + "`";

        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(countQuery)) {
                if (resultSet.next()) {
                    rowsCount = resultSet.getInt(1);
                } else {
                    rowsCount = 0;
                }

            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new SQLException(ex);
        }

        paginator = this.calculatePropertiesForPaginator(rowsCount, paginator);

        return paginator;
    }

    protected Paginator calculatePropertiesForPaginator(int rowsCount, Paginator paginator) {
        paginator.setPageCount((int) Math.ceil((double) rowsCount / paginator.getItemsCountPerPage()));

        if (paginator.getPageCount() == 0) {
            paginator.setItems(new ArrayList<>());

            return paginator;
        }

        int limit = 0;

        if (paginator.getPageCount() == 1) {
            paginator.setCurrentPage(1);
        }

        if (paginator.getCurrentPage() > paginator.getPageCount()) {
            paginator.setCurrentPage(paginator.getPageCount());
        }

        if (paginator.getCurrentPage() > 1) {
            limit = (paginator.getCurrentPage() - 1) * paginator.getItemsCountPerPage();
        }

        paginator.setOffset(limit);

        return paginator;
    }
}