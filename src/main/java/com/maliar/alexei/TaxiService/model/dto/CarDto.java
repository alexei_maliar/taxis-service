package com.maliar.alexei.TaxiService.model.dto;

import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;
import com.maliar.alexei.TaxiService.model.validator.MaxValidator;
import com.maliar.alexei.TaxiService.model.validator.MinValidator;
import com.maliar.alexei.TaxiService.model.validator.NotNullValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarDto implements Dto {
    private long idCar;

    private String name;

    private String category;

    private int seatCount;

    private String status;

    public long getIdCar() {
        return idCar;
    }

    public void setIdCar(long idCar) {
        this.idCar = idCar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Map<String, List<AbstractValidator>> getValidatorsMap() {
        Map<String, List<AbstractValidator>> map = new HashMap<>();

        List<AbstractValidator> nameValidators = new ArrayList<>();
        nameValidators.add(new NotNullValidator());
        nameValidators.add(new MinValidator(3));
        nameValidators.add(new MaxValidator(120));

        map.put("name", nameValidators);

        List<AbstractValidator> seatCountValidators = new ArrayList<>();
        seatCountValidators.add(new MinValidator(1));
        seatCountValidators.add(new MaxValidator(4));

        map.put("seat_count", seatCountValidators);

        List<AbstractValidator> categoryValidators = new ArrayList<>();
        categoryValidators.add(new NotNullValidator());

        map.put("category", categoryValidators);

        List<AbstractValidator> statusValidators = new ArrayList<>();
        statusValidators.add(new NotNullValidator());

        map.put("status", statusValidators);

        return map;
    }
}
