package com.maliar.alexei.TaxiService.model.validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class EmailValidator extends AbstractValidator {
    private static final String ERROR_INVALID_EMAIL = "email_invalid_format";

    @Override
    public Set<String> validate(Object object, String lang) {
        Set<String> messagesSet = Collections.synchronizedSet(new HashSet<>());

        String val = (String) object;

        if (!val.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$")) {
            messagesSet.add(getErrorMessage(ERROR_INVALID_EMAIL, lang));
        }

        return messagesSet;
    }
}
