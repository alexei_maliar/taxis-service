package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Location;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

import java.sql.*;
import java.util.List;

public class LocationDao extends AbstractDao {

    public LocationDao(DBManager dbManager, ObjectMapper mapper) {
        super(dbManager, mapper);
    }

    @Override
    public void insert(Entity entity) {
        Location location = (Location) entity;

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "INSERT INTO `location` (`id_location`, `name_en`, `name_ru`, `x`, `y`) VALUES (null, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, location.getNameEn());
                preparedStatement.setString(2, location.getNameRu());
                preparedStatement.setInt(3, location.getX());
                preparedStatement.setInt(4, location.getY());

                preparedStatement.executeUpdate();

                location.setIdLocation(this.lastInsertId(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException();
            }
        } catch (Exception ex) {
            throw new InternalException();
        }
    }

    @Override
    public void update(Entity entity) {
        Location location = (Location) entity;

        if (location.getIdLocation() < 1) {
            return;
        }

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "UPDATE `location` SET `name_en` = ?, `name_ru` = ?, `x` = ?, `y` = ? WHERE `id_location` = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, location.getNameEn());
                preparedStatement.setString(2, location.getNameRu());
                preparedStatement.setInt(3, location.getX());
                preparedStatement.setInt(4, location.getY());
                preparedStatement.setLong(5, location.getIdLocation());

                preparedStatement.executeUpdate();
            } catch (Exception ex) {
                throw new SQLException();
            }
        } catch (Exception ex) {
            throw new InternalException();
        }
    }

    @Override
    public Entity select(long id) {
        return this.selectById("location", "id_location", id);
    }

    @Override
    public List<Entity> selectAll() {
        return this.selectAllByTableName("location");
    }

    @Override
    public Paginator selectAllWithPaginator(Paginator paginator) {
        return this.selectAllByTableNameWithPaginator("location", paginator);
    }

    @Override
    public boolean delete(Entity entity) {
        Location location = (Location) entity;

        return this.deleteById("location", "id_location", location.getIdLocation());
    }

    public Entity selectCurrentLocationForCar(long idCar) {
        if (idCar < 1) {
            throw new IllegalArgumentException("Invalid car ident passed");
        }

        String query = "SELECT * FROM `location` "
                + "WHERE `id_location` = "
                + "(SELECT `id_location_to` FROM `order` WHERE `id_car` = ? AND `status` = ? "
                + "ORDER BY `completed_datetime` DESC LIMIT 1)";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, idCar);
                preparedStatement.setString(2, Order.Status.completed.toString());

                List<Entity> res = this.getResultList(preparedStatement);

                if (!res.isEmpty()) {
                    return res.get(0);
                }
            } catch (Exception ex) {
                throw new SQLException();
            }

            connection.setAutoCommit(true);
        } catch (Exception ex) {
            throw new InternalException();
        }

        return null;
    }
}
