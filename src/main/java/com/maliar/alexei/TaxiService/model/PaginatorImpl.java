package com.maliar.alexei.TaxiService.model;

import com.maliar.alexei.TaxiService.model.entity.Entity;

import java.util.List;

/**
 * Implementation of pagination for result of selecting data from database
 */
public class PaginatorImpl implements Paginator {
    private final static int DEFAULT_ITEMS_COUNT_PER_PAGE = 10;

    private int itemsCountPerPage;

    private int pageCount = 0;

    private int currentPage;

    private int offset = 0;

    private List<Entity> items;

    public PaginatorImpl(int currentPage, int itemsCountPerPage) {
        if (currentPage < 1 || itemsCountPerPage < 1) {
            throw new IllegalArgumentException("Params should be greater than 0");
        }

        this.itemsCountPerPage = itemsCountPerPage;
        this.currentPage = currentPage;
    }

    public PaginatorImpl(int currentPage) {
        this(currentPage, DEFAULT_ITEMS_COUNT_PER_PAGE);
    }

    @Override
    public void setCurrentPage(int page) {
        this.currentPage = page;
    }

    @Override
    public int getCurrentPage() {
        return this.currentPage;
    }

    @Override
    public void setPageCount(int count) {
        this.pageCount = count;
    }

    @Override
    public int getPageCount() {
        return this.pageCount;
    }

    @Override
    public void setItemsCountPerPage(int count) {
        this.itemsCountPerPage = count;
    }

    @Override
    public int getItemsCountPerPage() {
        return this.itemsCountPerPage;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public int getOffset() {
        return this.offset;
    }

    @Override
    public void setItems(List<Entity> items) {
        this.items = items;
    }

    @Override
    public List<Entity> getItems() {
        return this.items;
    }
}
