package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Order;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import org.apache.logging.log4j.LogManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OrderDao extends FilterAndSortDao {
    public OrderDao(DBManager dbManager, ObjectMapper mapper) {
        super(dbManager, mapper);
    }

    @Override
    public void insert(Entity entity) {
        Order order = (Order) entity;

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "INSERT INTO `order` (`id_order`, `id_user`, `id_car`, `id_location_from`, `id_location_to`, `price`, `discount_price`, `distance`, `status`, `created_datetime`, `arrived_datetime`, `completed_datetime`) " +
                    "VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setLong(1, order.getIdUser().getIdUser());
                preparedStatement.setLong(2, order.getIdCar().getIdCar());
                preparedStatement.setLong(3, order.getIdLocationFrom().getIdLocation());
                preparedStatement.setLong(4, order.getIdLocationTo().getIdLocation());
                preparedStatement.setDouble(5, order.getPrice());
                preparedStatement.setDouble(6, order.getDiscountPrice());
                preparedStatement.setDouble(7, order.getDistance());
                preparedStatement.setString(8, order.getStatus().toString());

                if (null != order.getCreatedDatetime()) {
                    preparedStatement.setTimestamp(9, new Timestamp(order.getCreatedDatetime().getTime()));
                }

                if (null != order.getArrivedDatetime()) {
                    preparedStatement.setTimestamp(10, new Timestamp(order.getArrivedDatetime().getTime()));
                }

                if (null != order.getCompletedDatetime()) {
                    preparedStatement.setTimestamp(11, new Timestamp(order.getCompletedDatetime().getTime()));
                }

                preparedStatement.executeUpdate();

                order.setIdOrder(this.lastInsertId(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    @Override
    public void update(Entity entity) {
        Order order = (Order) entity;

        if (order.getIdOrder() < 1) {
            return;
        }

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "UPDATE `order` SET `id_user` = ?, `id_car` = ?, `id_location_from` = ?, "
                    + "`id_location_to` = ?, `price` = ?, `discount_price` = ?, `distance` = ?, `status` = ?, "
                    + "`created_datetime` = ?, `arrived_datetime` = ?, `completed_datetime` = ? WHERE `id_order` = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, order.getIdUser().getIdUser());
                preparedStatement.setLong(2, order.getIdCar().getIdCar());
                preparedStatement.setLong(3, order.getIdLocationFrom().getIdLocation());
                preparedStatement.setLong(4, order.getIdLocationTo().getIdLocation());
                preparedStatement.setDouble(5, order.getPrice());
                preparedStatement.setDouble(6, order.getDiscountPrice());
                preparedStatement.setDouble(7, order.getDistance());
                preparedStatement.setString(8, order.getStatus().toString());

                if (null != order.getCreatedDatetime()) {
                    preparedStatement.setTimestamp(9, new Timestamp(order.getCreatedDatetime().getTime()));
                }

                if (null != order.getArrivedDatetime()) {
                    preparedStatement.setTimestamp(10, new Timestamp(order.getArrivedDatetime().getTime()));
                }

                if (null != order.getCompletedDatetime()) {
                    preparedStatement.setTimestamp(11, new Timestamp(order.getCompletedDatetime().getTime()));
                }

                preparedStatement.setLong(12, order.getIdOrder());

                preparedStatement.executeUpdate();
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    @Override
    public Entity select(long id) {
        return this.selectById("order", "id_order", id);
    }

    @Override
    public List<Entity> selectAll() {
        return this.selectAllByTableName("order");
    }

    @Override
    public List<Entity> selectAll(Map<String, Object> filtersMap, Map<String, String> sortMap) {
        return this.selectAllByTableName("order", filtersMap, sortMap);
    }

    @Override
    public Paginator selectAllWithPaginator(Paginator paginator) {
        return this.selectAllByTableNameWithPaginator("order", paginator);
    }

    @Override
    public Paginator selectAllWithPaginator(Paginator paginator, Map<String, Object> filtersMap, Map<String, String> sortMap) {
        return this.selectAllByTableNameWithPaginator("order", paginator, filtersMap, sortMap);
    }

    @Override
    public boolean delete(Entity entity) {
        return this.deleteById("order", "id_order", ((Order) entity).getIdOrder());
    }

    public Entity selectActiveOrderForUser(long idUser) {
        if (idUser < 1) {
            throw new IllegalArgumentException("Invalid user ident passed");
        }

        String query = "SELECT * FROM `order` WHERE `id_user` = ? AND `status` <> ? LIMIT 1";

        try (Connection connection = this.dbManager.getConnection()) {
            connection.setAutoCommit(false);

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, idUser);
                preparedStatement.setString(2, Order.Status.completed.toString());

                List<Entity> res = this.getResultList(preparedStatement);

                return res.isEmpty() ? null : res.get(0);
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    public double selectTotalMoneySpentForUser(long idUser) {
        if (idUser < 1) {
            throw new IllegalArgumentException("Invalid user ident passed");
        }

        String query = "SELECT SUM(`price`) as `total_amount` FROM `order` WHERE `id_user` = ? AND `status` = ?";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                connection.setAutoCommit(false);

                preparedStatement.setLong(1, idUser);
                preparedStatement.setString(2, Order.Status.completed.toString());

                try (ResultSet resultSet = preparedStatement.executeQuery()) {

                    return resultSet.next() ? resultSet.getDouble("total_amount") : 0;
                } catch (Exception ex) {
                    throw new SQLException(ex);
                }
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    public void updateScheduledOrdersStatus() {
        String selectArrivedQuery = "SELECT `id_order` FROM `order` WHERE `status` = ? AND `arrived_datetime` <= ?";
        String selectCompletedQuery = "SELECT `id_order` FROM `order` WHERE `status` = ? AND `completed_datetime` <= ?";

        String updateArrivedQuery = "UPDATE `order` SET `status` = ? WHERE `status` = ? AND `arrived_datetime` <= ?";
        String updateCompletedQuery = "UPDATE `order` SET `status` = ? WHERE `status` = ? AND `completed_datetime` <= ?";
        try (Connection connection = this.dbManager.getConnection()) {
            try (
                PreparedStatement selectArrivedStatement = connection.prepareStatement(selectArrivedQuery);
                PreparedStatement selectCompletedStatement = connection.prepareStatement(selectCompletedQuery);
                PreparedStatement updateArrivedStatement = connection.prepareStatement(updateArrivedQuery);
                PreparedStatement updateCompletedStatement = connection.prepareStatement(updateCompletedQuery)
            ) {
                connection.setAutoCommit(false);

                selectArrivedStatement.setString(1, Order.Status.pending.toString());
                selectArrivedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));

                selectCompletedStatement.setString(1, Order.Status.process.toString());
                selectCompletedStatement.setTimestamp(2, new Timestamp(new Date().getTime()));

                List<String> idArrivedList = new ArrayList<>();
                List<String> idCompletedList = new ArrayList<>();

                try (ResultSet selectArrivedRes = selectArrivedStatement.executeQuery();
                    ResultSet selectCompletedRes = selectCompletedStatement.executeQuery()
                ) {
                    while (selectArrivedRes.next()) {
                        idArrivedList.add(selectArrivedRes.getString(1));
                    }

                    while (selectCompletedRes.next()) {
                        idCompletedList.add(selectCompletedRes.getString(1));
                    }
                } catch (Exception ex) {
                    throw new SQLException(ex);
                }

                LogManager.getRootLogger().info("Arrived objects found " + idArrivedList.size() + " " + idArrivedList.toString());
                LogManager.getRootLogger().info("Completed objects found " + idCompletedList.size() + " " + idCompletedList.toString());

                int ares = 0;
                int cres = 0;

                if (!idArrivedList.isEmpty()) {
                    updateArrivedStatement.setString(1, Order.Status.process.toString());
                    updateArrivedStatement.setString(2, Order.Status.pending.toString());
                    updateArrivedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
                    ares = updateArrivedStatement.executeUpdate();
                }

                if (!idCompletedList.isEmpty()) {
                    updateCompletedStatement.setString(1, Order.Status.completed.toString());
                    updateCompletedStatement.setString(2, Order.Status.process.toString());
                    updateCompletedStatement.setTimestamp(3, new Timestamp(new Date().getTime()));
                    cres = updateCompletedStatement.executeUpdate();
                }

                LogManager.getRootLogger().info("Arrived res: " + ares);
                LogManager.getRootLogger().info("Completed res: " + cres);

                connection.setAutoCommit(true);
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }
}
