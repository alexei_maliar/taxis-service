package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDao extends AbstractDao {
    public CarDao(DBManager dbManager, ObjectMapper mapper) {
        super(dbManager, mapper);
    }

    @Override
    public void insert(Entity entity) {
        Car car = (Car) entity;

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "INSERT INTO `car` (`id_car`, `name`, `category`, `seat_count`, `status`) VALUES (null, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, car.getName());
                preparedStatement.setString(2, car.getCategory().toString());
                preparedStatement.setInt(3, car.getSeatCount());
                preparedStatement.setString(4, car.getStatus().toString());

                preparedStatement.executeUpdate();

                car.setIdCar(this.lastInsertId(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    @Override
    public void update(Entity entity) {
        Car car = (Car) entity;

        if (car.getIdCar() < 1) {
            return;
        }

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "UPDATE `car` SET `name` = ?, `category` = ?, `seat_count` = ?, `status` = ? WHERE `id_car` = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, car.getName());
                preparedStatement.setString(2, car.getCategory().toString());
                preparedStatement.setInt(3, car.getSeatCount());
                preparedStatement.setString(4, car.getStatus().toString());
                preparedStatement.setLong(5, car.getIdCar());

                preparedStatement.executeUpdate();
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    @Override
    public Entity select(long id) {
        return this.selectById("car", "id_car", id);
    }

    @Override
    public List<Entity> selectAll() {
        return this.selectAllByTableName("car");
    }

    @Override
    public Paginator selectAllWithPaginator(Paginator paginator) {
        return this.selectAllByTableNameWithPaginator("car", paginator);
    }

    @Override
    public boolean delete(Entity entity) {
        return this.deleteById("car", "id_car", ((Car) entity).getIdCar());
    }

    public Entity selectCorrespondingCar(int seatCount, String category) {
        if (seatCount < 1 || category == null) {
            throw new IllegalArgumentException("Some passed value is incorrect");
        }

        String query = "SELECT * FROM `car` "
                + "WHERE `seat_count` = ? AND `category` = ? AND `status` = ? "
                + "LIMIT 1";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, seatCount);
                preparedStatement.setString(2, category);
                preparedStatement.setString(3, Car.Status.pending_order.toString());

                List<Entity> res = this.getResultList(preparedStatement);

                if (res.size() > 0) {
                    return res.get(0);
                }
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }

        return null;
    }

    public List<Entity> selectSimilarCars(int seatCount, String category) {
        if (seatCount < 1 || category == null) {
            throw new IllegalArgumentException("Some passed value is incorrect");
        }

        String query = "SELECT * FROM `car` "
                + "WHERE ((`seat_count` >= ? AND `category` <> ?) OR (`seat_count` >= ? and `category` = ?)) AND `status` = ? LIMIT 8";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, seatCount);
                preparedStatement.setString(2, category);
                preparedStatement.setInt(3, seatCount);
                preparedStatement.setString(4, category);
                preparedStatement.setString(5, Car.Status.pending_order.toString());

                return this.getResultList(preparedStatement);
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    public boolean couldCarBeDelete(long idCar) {
        String query = "SELECT COUNT(`order`.`id_order`) as `order_count` FROM `car` " +
                "INNER JOIN `order` USING (`id_car`) " +
                "WHERE `id_car` = ? " +
                "GROUP BY `order`.`id_order`";

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, idCar);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return resultSet.getLong("order_count") == 0;
                    } else {
                        return true;
                    }
                } catch (Exception ex) {
                    throw new SQLException(ex);
                }
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }
}

