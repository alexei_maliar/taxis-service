package com.maliar.alexei.TaxiService.model.validator;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Validator's inherits uses DTOValidation
 * Contains error messages keys
 */
abstract public class AbstractValidator {
    public static final String RU = "ru";
    public static final String EN = "en";

    protected static final String ERROR_UNSUPPORTED_TYPE = "unsupported_type";

    private static final ResourceBundle ruMessages;
    private static final ResourceBundle enMessages;

    static {
        ruMessages = ResourceBundle.getBundle("validation-messages", new Locale("ru", "RU"));
        enMessages = ResourceBundle.getBundle("validation-messages", new Locale("en", "US"));
    }

    abstract public Set<String> validate(Object object, String lang);

    protected static String getErrorMessage(String msgKey, String lang) {
        if (!lang.equals(RU) && !lang.equals(EN)) {
            throw new IllegalArgumentException("Invalid language passed");
        }

        ResourceBundle bundle = lang.equals(EN) ? enMessages : ruMessages;

        msgKey = "validation." + msgKey;

        if (!bundle.containsKey(msgKey)) {
            throw new IllegalArgumentException("Invalid message key passed");
        }

        return bundle.getString(msgKey);
    }
}
