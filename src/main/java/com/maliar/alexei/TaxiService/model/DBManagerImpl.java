package com.maliar.alexei.TaxiService.model;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Facade to DB connection pool
 */
public class DBManagerImpl implements DBManager {
    private final DataSource dataSource;

    public DBManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = this.dataSource.getConnection();
        connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        return connection;
    }
}
