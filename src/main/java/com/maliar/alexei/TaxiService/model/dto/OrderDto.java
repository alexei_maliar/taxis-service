package com.maliar.alexei.TaxiService.model.dto;

import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;
import com.maliar.alexei.TaxiService.model.validator.MaxValidator;
import com.maliar.alexei.TaxiService.model.validator.MinValidator;
import com.maliar.alexei.TaxiService.model.validator.NotNullValidator;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class OrderDto implements Dto {
    private long idLocationFrom;

    private long idLocationTo;

    private int seatCount;

    private String category;

    public long getIdLocationFrom() {
        return idLocationFrom;
    }

    public void setIdLocationFrom(long idLocationFrom) {
        this.idLocationFrom = idLocationFrom;
    }

    public long getIdLocationTo() {
        return idLocationTo;
    }

    public void setIdLocationTo(long idLocationTo) {
        this.idLocationTo = idLocationTo;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public Map<String, List<AbstractValidator>> getValidatorsMap() {
        Map<String, List<AbstractValidator>> map = new HashMap<>();

        List<AbstractValidator> idLocationValidators = new ArrayList<>();
        idLocationValidators.add(new MinValidator(1));

        map.put("id_location_from", idLocationValidators);
        map.put("id_location_to", idLocationValidators);

        List<AbstractValidator> seatCountValidators = new ArrayList<>();
        seatCountValidators.add(new MinValidator(1));
        seatCountValidators.add(new MaxValidator(4));

        map.put("seat_count", seatCountValidators);

        List<AbstractValidator> categoryValidators = new ArrayList<>();
        categoryValidators.add(new NotNullValidator());

        map.put("category", categoryValidators);

        return map;
    }
}
