package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dao.AbstractDao;
import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.dto.OrderDto;
import com.maliar.alexei.TaxiService.model.entity.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class OrderObjectMapper implements ObjectMapper {
    private final AbstractDao userDao;

    private final AbstractDao carDao;

    private final AbstractDao locationDao;

    public OrderObjectMapper(AbstractDao userDao, AbstractDao carDao, AbstractDao locationDao) {
        this.userDao = userDao;
        this.carDao = carDao;
        this.locationDao = locationDao;
    }

    @Override
    public Entity entityFromResultSet(ResultSet resultSet) throws SQLException {
        Order order = new Order();

        order.setIdOrder(resultSet.getLong("id_order"));
        order.setPrice(resultSet.getDouble("price"));
        order.setDiscountPrice(resultSet.getDouble("discount_price"));
        order.setDistance(resultSet.getDouble("distance"));

        Entity car = this.carDao.select(resultSet.getLong("id_car"));
        if (null != car) {
            order.setIdCar((Car) car);
        }

        Entity user = this.userDao.select(resultSet.getLong("id_user"));
        if (null != user) {
            order.setIdUser((User) user);
        }

        Entity locationFrom = this.locationDao.select(resultSet.getLong("id_location_from"));
        if (null != locationFrom) {
            order.setIdLocationFrom((Location) locationFrom);
        }

        Entity locationTo = this.locationDao.select(resultSet.getLong("id_location_to"));
        if (null != locationTo) {
            order.setIdLocationTo((Location) locationTo);
        }

        try {
            order.setStatus(Order.Status.valueOf(resultSet.getString("status")));
        } catch (Exception ignored) {
        }

        order.setCreatedDatetime(new Date(resultSet.getTimestamp("created_datetime").getTime()));
        order.setArrivedDatetime(new Date(resultSet.getTimestamp("arrived_datetime").getTime()));
        order.setCompletedDatetime(new Date(resultSet.getTimestamp("completed_datetime").getTime()));

        return order;
    }

    @Override
    public Dto dtoFromRequest(HttpServletRequest request) {
        OrderDto dto = new OrderDto();

        try {
            dto.setIdLocationFrom(Long.parseLong(request.getParameter("id_location_from")));
        } catch (Exception ex) {
            dto.setIdLocationFrom(0);
        }

        try {
            dto.setIdLocationTo(Long.parseLong(request.getParameter("id_location_to")));
        } catch (Exception ex) {
            dto.setIdLocationTo(0);
        }

        try {
            dto.setSeatCount(Integer.parseInt(request.getParameter("seat_count")));
        } catch (Exception ex) {
            dto.setSeatCount(0);
        }

        dto.setCategory(request.getParameter("category") != null ? request.getParameter("category") : "");

        return dto;
    }

    @Override
    public Entity entityFromDto(Dto dto) {
        OrderDto orderDto = (OrderDto) dto;

        Order order = new Order();

        Entity locationFrom = this.locationDao.select(orderDto.getIdLocationFrom());
        if (null != locationFrom) {
            order.setIdLocationFrom((Location) locationFrom);
        }

        Entity locationTo = this.locationDao.select(orderDto.getIdLocationTo());
        if (null != locationTo) {
            order.setIdLocationTo((Location) locationTo);
        }

        return order;
    }
}
