package com.maliar.alexei.TaxiService.model.validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MaxValidator extends AbstractValidator {
    private static final String ERROR_INVALID_MAX_LENGTH = "max_invalid_length";
    private static final String ERROR_INVALID_MAX_LENGTH_OR_EQUALS = "max_invalid_length_or_equals";

    private final long value;

    private final boolean inclusive;

    public MaxValidator(long value) {
        this(value, true);
    }

    public MaxValidator(long value, boolean inclusive) {
        this.value = value;
        this.inclusive = inclusive;
    }

    @Override
    public Set<String> validate(Object object, String lang) {
        long length;

        Set<String> messagesSet = Collections.synchronizedSet(new HashSet<>());

        if (object instanceof String) {
            length = ((String) object).length();
        } else if (object instanceof Number) {
            length = ((Number) object).longValue();
        } else {
            messagesSet.add(getErrorMessage(ERROR_UNSUPPORTED_TYPE, lang));
            return messagesSet;
        }

        if (inclusive && length > this.value) {
            messagesSet.add(getErrorMessage(ERROR_INVALID_MAX_LENGTH_OR_EQUALS, lang).replaceAll("%value%", String.valueOf(this.value)));
        }

        if (!inclusive && length >= this.value) {
            messagesSet.add(getErrorMessage(ERROR_INVALID_MAX_LENGTH, lang).replaceAll("%value%", String.valueOf(this.value)));
        }

        return messagesSet;
    }
}
