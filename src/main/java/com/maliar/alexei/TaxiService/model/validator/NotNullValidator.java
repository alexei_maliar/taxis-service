package com.maliar.alexei.TaxiService.model.validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class NotNullValidator extends AbstractValidator {
    private static final String ERROR_INVALID_VALUE = "not_null_invalid_value";

    @Override
    public Set<String> validate(Object object, String lang) {
        Set<String> messagesSet = Collections.synchronizedSet(new HashSet<>());

        if (object == null) {
            messagesSet.add(getErrorMessage(ERROR_INVALID_VALUE, lang));
        }

        return messagesSet;
    }
}
