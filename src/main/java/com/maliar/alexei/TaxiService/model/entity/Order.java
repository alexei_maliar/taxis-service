package com.maliar.alexei.TaxiService.model.entity;

import java.util.Date;
import java.util.Objects;

public class Order implements Entity {
    private long idOrder;

    private User idUser;

    private Car idCar;

    private Location idLocationFrom;

    private Location idLocationTo;

    private double price = 0;

    private double discountPrice = 0;

    private double distance = 0;

    private Status status = Status.pending;

    private Date createdDatetime;

    private Date arrivedDatetime;

    private Date completedDatetime;

    public long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(long idOrder) {
        this.idOrder = idOrder;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    public Car getIdCar() {
        return idCar;
    }

    public void setIdCar(Car idCar) {
        this.idCar = idCar;
    }

    public Location getIdLocationFrom() {
        return idLocationFrom;
    }

    public void setIdLocationFrom(Location idLocationFrom) {
        this.idLocationFrom = idLocationFrom;
    }

    public Location getIdLocationTo() {
        return idLocationTo;
    }

    public void setIdLocationTo(Location idLocationTo) {
        this.idLocationTo = idLocationTo;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(Date createdDatetime) {
        if (null != createdDatetime) {
            this.createdDatetime = createdDatetime;
        }
    }

    public Date getArrivedDatetime() {
        return arrivedDatetime;
    }

    public void setArrivedDatetime(Date arrivedDatetime) {
        if (null != arrivedDatetime) {
            this.arrivedDatetime = arrivedDatetime;
        }
    }

    public Date getCompletedDatetime() {
        return completedDatetime;
    }

    public void setCompletedDatetime(Date completedDatetime) {
        if (null != completedDatetime) {
            this.completedDatetime = completedDatetime;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return idOrder == order.idOrder;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOrder);
    }

    @Override
    public String toString() {
        return "Order{" +
                "idOrder=" + idOrder +
                ", idUser=" + idUser +
                ", idCar=" + idCar +
                ", idLocationFrom=" + idLocationFrom +
                ", idLocationTo=" + idLocationTo +
                ", price=" + price +
                ", discountPrice=" + discountPrice +
                ", distance=" + distance +
                ", status=" + status +
                ", createdDatetime=" + createdDatetime +
                ", arrivedDatetime=" + arrivedDatetime +
                ", completedDatetime=" + completedDatetime +
                '}';
    }

    public enum Status {
        pending, process, completed
    }
}
