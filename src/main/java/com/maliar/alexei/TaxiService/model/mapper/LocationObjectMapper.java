package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Location;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationObjectMapper implements ObjectMapper {
    @Override
    public Entity entityFromResultSet(ResultSet resultSet) throws SQLException {
        Location location = new Location();

        location.setIdLocation(resultSet.getLong("id_location"));
        location.setNameEn(resultSet.getString("name_en"));
        location.setNameRu(resultSet.getString("name_ru"));
        location.setX(resultSet.getInt("x"));
        location.setY(resultSet.getInt("y"));

        return location;
    }

    @Override
    public Dto dtoFromRequest(HttpServletRequest request) {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public Entity entityFromDto(Dto dto) {
        throw new IllegalStateException("Not implemented");
    }
}
