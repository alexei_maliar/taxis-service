package com.maliar.alexei.TaxiService.model.entity;

import java.util.Objects;

public class Car implements Entity {
    private long idCar;

    private String name;

    private Category category;

    private int seatCount = 1;

    private Status status;

    public long getIdCar() {
        return idCar;
    }

    public void setIdCar(long idCar) {
        this.idCar = idCar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Category {
        economy, standard, comfort
    }

    public enum Status {
        pending_order, in_cruise, inactive
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return idCar == car.idCar;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCar);
    }

    @Override
    public String toString() {
        return "Car{" +
                "idCar=" + idCar +
                ", name='" + name + '\'' +
                ", category=" + category +
                ", seatCount=" + seatCount +
                ", status=" + status +
                '}';
    }
}
