package com.maliar.alexei.TaxiService.model.entity;

import java.util.Objects;

public class Location implements Entity {
    private long idLocation;

    private String nameEn;

    private String nameRu;

    private int x = 1;

    private int y = 1;

    public long getIdLocation() {
        return idLocation;
    }

    public void setIdLocation(long idLocation) {
        this.idLocation = idLocation;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return idLocation == location.idLocation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idLocation);
    }

    @Override
    public String toString() {
        return "Location{" +
                "idLocation=" + idLocation +
                ", nameEn='" + nameEn + '\'' +
                ", nameRu='" + nameRu + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
