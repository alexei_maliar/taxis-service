package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper for converting one data representation to another
 */
public interface ObjectMapper {
    Entity entityFromResultSet(ResultSet resultSet) throws SQLException;

    Entity entityFromDto(Dto dto);

    Dto dtoFromRequest(HttpServletRequest request);
}
