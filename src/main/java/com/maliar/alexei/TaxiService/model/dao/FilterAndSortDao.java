package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

abstract class FilterAndSortDao extends AbstractDao {
    protected static final String ORDER_ASC = "ASC";
    protected static final String ORDER_DESC = "DESC";

    public FilterAndSortDao(DBManager dbManager, ObjectMapper mapper) {
        super(dbManager, mapper);
    }

    abstract Paginator selectAllWithPaginator(Paginator paginator, Map<String, Object> filtersMap, Map<String, String> sortMap);

    abstract List<Entity> selectAll(Map<String, Object> filtersMap, Map<String, String> sortMap);

    protected Paginator selectAllByTableNameWithPaginator(String tableName, Paginator paginator, Map<String, Object> filtersMap, Map<String, String> ordersMap) {
        try (Connection connection = this.dbManager.getConnection()) {
            paginator = this.initPaginator(tableName, paginator, connection, filtersMap);

            List<Object> params = new ArrayList<>();
            StringBuilder query = new StringBuilder();

            query.append("SELECT * FROM `");
            query.append(tableName);
            query.append("` ");

            this.initFilterAndOrder(query, filtersMap, ordersMap, params);

            query.append(" LIMIT ?, ?");

            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                this.setFilterParamsToPreparedStatement(preparedStatement, params);

                preparedStatement.setInt(params.isEmpty() ? 1 : params.size() + 1, paginator.getOffset());
                preparedStatement.setInt(params.isEmpty() ? 2 : params.size() + 2, paginator.getItemsCountPerPage());

                paginator.setItems(this.getResultList(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }

        return paginator;
    }

    protected List<Entity> selectAllByTableName(String tableName, Map<String, Object> filtersMap, Map<String, String> sortMap) {
        StringBuilder query = new StringBuilder();

        query.append("SELECT * FROM `");
        query.append(tableName);
        query.append("` ");

        List<Object> params = new ArrayList<>();

        this.initFilterAndOrder(query, filtersMap, sortMap, params);

        try (Connection connection = this.dbManager.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                this.setFilterParamsToPreparedStatement(preparedStatement, params);
                return this.getResultList(preparedStatement);
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    protected void initFilterAndOrder(StringBuilder query, Map<String, Object> filtersMap, Map<String, String> ordersMap, List<Object> params) {
        if (null == filtersMap) {
            throw new IllegalArgumentException("Filter should be passed");
        }

        if (!filtersMap.isEmpty()) {
            query.append("WHERE ");
        }

        int fCount = 0;
        for (Map.Entry<String, Object> filterEntry : filtersMap.entrySet()) {
            if (fCount > 0) {
                query.append("AND ");
            }

            // crazy solution =)
            if (filterEntry.getValue() instanceof Date) {
                query.append("DATE(`");
                query.append(filterEntry.getKey());
                query.append("`) = ? ");
            } else {
                query.append("`");
                query.append(filterEntry.getKey());
                query.append("` = ? ");
            }

            params.add(filterEntry.getValue());

            fCount++;
        }

        if (null != ordersMap) {
            if (!ordersMap.isEmpty()) {
                query.append("ORDER BY ");
            }

            int oCount = 0;
            for (Map.Entry<String, String> orderEntry : ordersMap.entrySet()) {

                query.append("`");
                query.append(orderEntry.getKey());
                query.append("` ");

                if (orderEntry.getValue().equalsIgnoreCase(ORDER_ASC) || orderEntry.getValue().equalsIgnoreCase(ORDER_DESC)) {
                    query.append(orderEntry.getValue());
                } else {
                    query.append(ORDER_ASC);
                }

                if (oCount > 0 && (ordersMap.size() - 1) > oCount) {
                    query.append(", ");
                }

                oCount++;
            }
        }
    }

    protected void setFilterParamsToPreparedStatement(PreparedStatement preparedStatement, List<Object> params) throws SQLException {
        for (int i = 0; i < params.size(); i++) {
            if (params.get(i) instanceof Date) {
                preparedStatement.setDate(i + 1, new java.sql.Date(((Date) params.get(i)).getTime()));
            } else {
                preparedStatement.setObject(i + 1, params.get(i));
            }
        }
    }

    protected Paginator initPaginator(String tableName, Paginator paginator, Connection connection, Map<String, Object> filterMap) throws SQLException {
        int rowsCount;

        StringBuilder query = new StringBuilder();

        query.append("SELECT COUNT(*) FROM `");
        query.append(tableName);
        query.append("` ");

        List<Object> params = new ArrayList<>();

        this.initFilterAndOrder(query, filterMap, null, params);

        try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
            this.setFilterParamsToPreparedStatement(preparedStatement, params);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    rowsCount = resultSet.getInt(1);
                } else {
                    rowsCount = 0;
                }
            } catch (Exception ex) {
                throw new SQLException(ex);
            }
        } catch (Exception ex) {
            throw new SQLException(ex);
        }

        paginator = this.calculatePropertiesForPaginator(rowsCount, paginator);

        return paginator;
    }
}
