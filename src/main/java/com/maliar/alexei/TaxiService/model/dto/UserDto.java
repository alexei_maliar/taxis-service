package com.maliar.alexei.TaxiService.model.dto;

import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;
import com.maliar.alexei.TaxiService.model.validator.EmailValidator;
import com.maliar.alexei.TaxiService.model.validator.MinValidator;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class UserDto implements Dto {
    private String email;

    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Map<String, List<AbstractValidator>> getValidatorsMap() {
        Map<String, List<AbstractValidator>> map = new HashMap<>();

        List<AbstractValidator> emailValidators = new ArrayList<>();
        emailValidators.add(new EmailValidator());
        map.put("email", emailValidators);

        List<AbstractValidator> passwordValidators = new ArrayList<>();
        passwordValidators.add(new MinValidator(6));
        map.put("password", passwordValidators);

        return map;
    }
}