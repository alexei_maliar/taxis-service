package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dto.CarDto;
import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarObjectMapper implements ObjectMapper {
    @Override
    public Entity entityFromResultSet(ResultSet resultSet) throws SQLException {
        Car car = new Car();

        car.setIdCar(resultSet.getLong("id_car"));
        car.setName(resultSet.getString("name"));
        car.setCategory(Car.Category.valueOf(resultSet.getString("category")));
        car.setStatus(Car.Status.valueOf(resultSet.getString("status")));
        car.setSeatCount(resultSet.getInt("seat_count"));

        return car;
    }

    @Override
    public Dto dtoFromRequest(HttpServletRequest request) {
        CarDto carDto = new CarDto();

        try {
            carDto.setIdCar(Long.parseLong(request.getParameter("id_car")));
        } catch (Exception ex) {
            carDto.setIdCar(0);
        }

        try {
            carDto.setSeatCount(Integer.parseInt(request.getParameter("seat_count")));
        } catch (Exception ex) {
            carDto.setSeatCount(0);
        }

        carDto.setName(request.getParameter("name") != null ? request.getParameter("name") : "");

        carDto.setCategory(request.getParameter("category") != null ? request.getParameter("category") : "");

        carDto.setStatus(request.getParameter("status") != null ? request.getParameter("status") : "");

        return carDto;
    }

    @Override
    public Entity entityFromDto(Dto dto) {
        CarDto carDto = (CarDto) dto;

        Car car = new Car();

        car.setIdCar(carDto.getIdCar());
        car.setName(carDto.getName());

        try {
             car.setCategory(Car.Category.valueOf(carDto.getCategory()));
        } catch (Exception ignored) {
        }

        try {
            car.setStatus(Car.Status.valueOf(carDto.getStatus()));
        } catch (Exception ignored) {
        }

        car.setSeatCount(carDto.getSeatCount());

        return car;
    }
}
