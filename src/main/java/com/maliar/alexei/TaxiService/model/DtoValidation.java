package com.maliar.alexei.TaxiService.model;

import com.maliar.alexei.TaxiService.model.dto.Dto;

import java.util.List;
import java.util.Map;

public interface DtoValidation {
    Map<String, List<String>> validate(Dto dtoObject, String lang);
}
