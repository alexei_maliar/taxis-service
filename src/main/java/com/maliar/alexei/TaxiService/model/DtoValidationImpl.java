package com.maliar.alexei.TaxiService.model;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class could validate passed data into Data Transfer Objects
 */
public class DtoValidationImpl implements DtoValidation {
    @Override
    public Map<String, List<String>> validate(Dto dtoObject, String lang)  {
        Map<String, List<AbstractValidator>> validatorsMap = dtoObject.getValidatorsMap();

        Class<? extends Dto> dtoClass = dtoObject.getClass();

        Map<String, List<String>> errorsMap = new HashMap<>();

        for (Field field: dtoClass.getDeclaredFields()) {
            String fieldName = field.getName();

            Object fieldValue = this.getDtoFieldValue(fieldName, dtoObject);

            this.validateField(fieldName, fieldValue, lang, validatorsMap, errorsMap);
        }

        return errorsMap;
    }

    protected Object getDtoFieldValue(String fieldName, Dto dtoObject) {
        String getterMethodName = "get"
                + fieldName.substring(0, 1).toUpperCase()
                + fieldName.substring(1);

        try {
            return dtoObject.getClass().getDeclaredMethod(getterMethodName).invoke(dtoObject);
        } catch (Exception ex) {
            return null;
        }
    }

    protected void validateField(
            String fieldName,
            Object fieldValue,
            String lang,
            Map<String, List<AbstractValidator>> validatorsMap,
            Map<String, List<String>> errorsMap
    ) {
        fieldName = this.camelToSnake(fieldName);

        if (!validatorsMap.containsKey(fieldName)) {
            return;
        }

        for (AbstractValidator validator: validatorsMap.get(fieldName)) {
            Set<String> validationResult = validator.validate(fieldValue, lang);

            if (!validationResult.isEmpty()) {
                List<String> errors = errorsMap.getOrDefault(fieldName, new Vector<>());

                errors.addAll(validationResult);

                errorsMap.put(fieldName, errors);
            }
        }
    }

    protected String camelToSnake(String str) {
        StringBuilder result = new StringBuilder();

        char c = str.charAt(0);
        result.append(Character.toLowerCase(c));

        for (int i = 1; i < str.length(); i++) {

            char ch = str.charAt(i);
            if (Character.isUpperCase(ch)) {
                result.append('_');
                result.append(Character.toLowerCase(ch));
            }

            else {
                result.append(ch);
            }
        }

        return result.toString();
    }
}
