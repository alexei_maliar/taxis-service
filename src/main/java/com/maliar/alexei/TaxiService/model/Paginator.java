package com.maliar.alexei.TaxiService.model;

import com.maliar.alexei.TaxiService.model.entity.Entity;

import java.util.List;

public interface Paginator {
    void setCurrentPage(int page);

    int getCurrentPage();

    void setPageCount(int count);

    int getPageCount();

    void setItemsCountPerPage(int count);

    int getItemsCountPerPage();

    void setItems(List<Entity> items);

    void setOffset(int offset);

    int getOffset();

    List<Entity> getItems();

    static Paginator getInstance(int currentPage, int perPage) {
        return new PaginatorImpl(currentPage, perPage);
    }

    static Paginator getInstance(int currentPage) {
        return new PaginatorImpl(currentPage);
    }
}
