package com.maliar.alexei.TaxiService.model.dto;

import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminUserDto implements Dto {
    private long idUser;

    private String role;

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public Map<String, List<AbstractValidator>> getValidatorsMap() {
        return new HashMap<>();
    }
}
