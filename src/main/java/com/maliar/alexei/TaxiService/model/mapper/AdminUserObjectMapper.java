package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dto.AdminUserDto;
import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;

public class AdminUserObjectMapper implements ObjectMapper {
    @Override
    public Entity entityFromResultSet(ResultSet resultSet) {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public Entity entityFromDto(Dto dto) {
        AdminUserDto adminUserDto = (AdminUserDto) dto;

        User user = new User();

        user.setIdUser(adminUserDto.getIdUser());
        user.setRole(User.Role.valueOf(adminUserDto.getRole()));

        return user;
    }

    @Override
    public Dto dtoFromRequest(HttpServletRequest request) {
        AdminUserDto dto = new AdminUserDto();

        try {
            dto.setIdUser(Long.parseLong(request.getParameter("id_user")));
        } catch (Exception ex) {
            dto.setIdUser(0);
        }

        dto.setRole(request.getParameter("role") != null ? request.getParameter("role") : "");

        return dto;
    }
}
