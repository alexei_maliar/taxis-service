package com.maliar.alexei.TaxiService.model.dto;

import com.maliar.alexei.TaxiService.model.validator.AbstractValidator;

import java.util.List;
import java.util.Map;

public interface Dto {
    String MODE_ADD = "add";
    String MODE_EDIT = "edit";

    Map<String, List<AbstractValidator>> getValidatorsMap();
}
