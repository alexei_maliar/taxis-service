package com.maliar.alexei.TaxiService.model.mapper;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.dto.UserDto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class UserObjectMapper implements ObjectMapper {
    @Override
    public Entity entityFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setIdUser(resultSet.getLong("id_user"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));

        try {
            user.setRole(User.Role.valueOf(resultSet.getString("role")));
        } catch (Exception ignored) {
        }

        user.setRegistrationDate(new Date(resultSet.getTimestamp("registration_date").getTime()));

        return user;
    }

    @Override
    public Dto dtoFromRequest(HttpServletRequest request) {
        UserDto dto = new UserDto();

        dto.setEmail(request.getParameter("email") != null ? request.getParameter("email") : "");
        dto.setPassword(request.getParameter("password") != null ? request.getParameter("password") : "");

        return dto;
    }

    @Override
    public Entity entityFromDto(Dto dto) {
        UserDto userDto = (UserDto) dto;

        User user = new User();

        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());

        return user;
    }
}

