package com.maliar.alexei.TaxiService.model.dao;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

import java.sql.*;
import java.util.List;

public class UserDao extends AbstractDao {
    public UserDao(DBManager dbManager, ObjectMapper mapper) {
        super(dbManager, mapper);
    }

    @Override
    public void insert(Entity entity) {
        User user = (User) entity;

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "INSERT INTO `user` (`id_user`, `email`, `password`, `role`, `registration_date`) VALUES (null, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setString(1, user.getEmail());
                preparedStatement.setString(2, user.getPassword());
                preparedStatement.setString(3, user.getRole().toString());
                preparedStatement.setTimestamp(4, new Timestamp(user.getRegistrationDate().getTime()));

                preparedStatement.executeUpdate();

                user.setIdUser(this.lastInsertId(preparedStatement));
            } catch (Exception ex) {
                throw new SQLException();
            }
        } catch (Exception ex) {
            throw new InternalException();
        }
    }

    @Override
    public void update(Entity entity) {
        User user = (User) entity;

        if (user.getIdUser() < 1) {
            return;
        }

        try (Connection connection = this.dbManager.getConnection()) {
            String query = "UPDATE `user` SET `email` = ?, `password` = ?, `role` = ?, `registration_date` = ? WHERE `id_user` = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, user.getEmail());
                preparedStatement.setString(2, user.getPassword());
                preparedStatement.setString(3, user.getRole().toString());
                preparedStatement.setTimestamp(4, new Timestamp(user.getRegistrationDate().getTime()));
                preparedStatement.setLong(5, user.getIdUser());

                preparedStatement.executeUpdate();
            } catch (Exception ex) {
                throw new SQLException();
            }
        } catch (Exception ex) {
            throw new InternalException();
        }
    }

    @Override
    public Entity select(long id) {
        return this.selectById("user", "id_user", id);
    }

    @Override
    public List<Entity> selectAll() {
        return this.selectAllByTableName("user");
    }

    @Override
    public Paginator selectAllWithPaginator(Paginator paginator) {
        return this.selectAllByTableNameWithPaginator("user", paginator);
    }

    public Entity selectByEmail(String email) {
        return this.selectByCol("user", "email", email);
    }

    @Override
    public boolean delete(Entity entity) {
        User user = (User) entity;
        return this.deleteById("user", "id_user", user.getIdUser());
    }
}