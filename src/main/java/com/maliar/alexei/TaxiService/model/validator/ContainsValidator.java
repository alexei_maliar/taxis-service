package com.maliar.alexei.TaxiService.model.validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ContainsValidator extends AbstractValidator {
    private static final String ERROR_INVALID_VALUE = "invalid_value";

    private final List<Object> sequenceForAssert;

    public ContainsValidator(List<Object> sequenceForAssert) {
        this.sequenceForAssert = sequenceForAssert;
    }

    @Override
    public Set<String> validate(Object object, String lang) {
        Set<String> messagesSet = Collections.synchronizedSet(new HashSet<>());

        if (!this.sequenceForAssert.contains(object)) {
            messagesSet.add(getErrorMessage(ERROR_INVALID_VALUE, lang));
        }

        return messagesSet;
    }
}
