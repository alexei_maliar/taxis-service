package com.maliar.alexei.TaxiService.router;

import com.maliar.alexei.TaxiService.ConfigRegistry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

/**
 * Passer of configuration file with description routes to controllers
 */
public class ConfigParser {
    public static Map<Object, Object> parse() {
        Map<Object, Object> result = new Hashtable<>();

        URL configUrl = ConfigParser.class.getClassLoader().getResource(ConfigRegistry.getRoutesConfigFilename());

        if (null == configUrl) {
            throw new IllegalArgumentException("Routes config file does not exist or could not find");
        }

        XMLEventReader parser = getParser(configUrl.getPath());

        boolean bUrl = false;
        boolean bController = false;

        try {
            Object url = null;
            Object controller = null;

            while (parser.hasNext()) {
                XMLEvent event = parser.nextEvent();

                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("route")) {
                        url = null;
                        controller = null;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("url")) {
                        bUrl = true;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("controller")) {
                        bController = true;
                    }
                }

                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("route")) {
                        result.put(url, controller);
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("url")) {
                        bUrl = false;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("controller")) {
                        bController = false;
                    }
                }

                if (event.isCharacters()) {
                    Characters element = (Characters) event;

                    if (bUrl) {
                        url = element.getData();
                    }

                    if (bController) {
                        controller = element.getData();
                    }
                }
            }
        } catch (Exception ex) {
        }

        return result;
    }

    private static XMLEventReader getParser(String configDirectoryPath) {
        File file = new File(configDirectoryPath);

        if (!file.exists()) {
            throw new IllegalStateException("System could not find routes config");
        }

        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
            return factory.createXMLEventReader(new FileReader(file));
        } catch (Exception ex) {
            throw new IllegalStateException("System could not init config parser");
        }
    }
}
