package com.maliar.alexei.TaxiService.router;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.exception.ControllerNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

/**
 * Router for matching needed controllers for dispatching request
 *
 * Based on request path, it try to find controller, which associated with this path.
 *
 * Depends on config file with routes.
 * Name of this file should be defined in servlet context init params
 */
public class RouterImpl implements Router {
    private final ServiceContainer serviceContainer;

    private final Map<Object, Object> routesMap;

    public RouterImpl(ServiceContainer serviceContainer, Map<Object, Object> routesMap) {
        this.serviceContainer = serviceContainer;
        this.routesMap = routesMap;
    }

    @Override
    public ActionController match(HttpServletRequest request) {
        if (routesMap == null || routesMap.isEmpty()) {
            throw new IllegalStateException("Routes config map is missing");
        }

        String path = request.getRequestURI().substring(request.getContextPath().length());

        String[] pathParts = path.split("/");

        String url;

        if (pathParts.length == 0) {
            // main page controller
            url = "/";
        } else {
            url = Arrays.stream(pathParts)
                    .filter(s -> !s.isEmpty())
                    .reduce("", (s, s2) -> s + "/" + s2);
        }

        if (!this.routesMap.containsKey(url)) {
            throw new ControllerNotFoundException();
        }

        return (ActionController) this.serviceContainer.get(this.routesMap.get(url));
    }
}

