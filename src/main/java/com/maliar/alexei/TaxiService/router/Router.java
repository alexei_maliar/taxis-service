package com.maliar.alexei.TaxiService.router;

import com.maliar.alexei.TaxiService.controller.ActionController;
import javax.servlet.http.HttpServletRequest;

public interface Router {
    ActionController match(HttpServletRequest request);
}
