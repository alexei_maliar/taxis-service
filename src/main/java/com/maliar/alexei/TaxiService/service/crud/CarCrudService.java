package com.maliar.alexei.TaxiService.service.crud;

import com.maliar.alexei.TaxiService.exception.DtoValidationException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.dto.CarDto;
import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.mapper.CarObjectMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CarCrudService implements CrudService {
    private final CarDao carDao;

    private final CarObjectMapper carObjectMapper;

    private final DtoValidation dtoValidation;

    public CarCrudService(CarDao carDao, CarObjectMapper carObjectMapper, DtoValidation dtoValidation) {
        this.carDao = carDao;
        this.carObjectMapper = carObjectMapper;
        this.dtoValidation = dtoValidation;
    }

    @Override
    public Entity readOne(HttpServletRequest request) {
        return this.carDao.select(this.getId(request, "id_car"));
    }

    @Override
    public Paginator readAll(HttpServletRequest request) {
        return this.carDao.selectAllWithPaginator(Paginator.getInstance(this.getCurrentPage(request)));
    }

    @Override
    public void create(HttpServletRequest request) throws DtoValidationException, LogicException {
        CarDto carDto = (CarDto) this.carObjectMapper.dtoFromRequest(request);

        this.getCurrentControllerFromRequest(request).saveSubmitDataToSession(request, carDto);

        Map<String, List<String>> errorMap = this.dtoValidation.validate(carDto, "en");

        try {
            Car.Category.valueOf(carDto.getCategory());
        } catch (Exception ex) {
            List<String> categoryErrors = new ArrayList<>();
            categoryErrors.add("Invalid category passed");

            errorMap.put("category", categoryErrors);
        }

        if (Car.Status.in_cruise.toString().equals(carDto.getStatus())) {
            List<String> statusErrors = new ArrayList<>();
            statusErrors.add("Invalid status passed");

            errorMap.put("status", statusErrors);
        }

        if (!errorMap.isEmpty()) {
            throw new DtoValidationException(errorMap);
        }

        Car car = (Car) this.carObjectMapper.entityFromDto(carDto);

        this.carDao.insert(car);

        if (car.getIdCar() < 1) {
            throw new LogicException("Unavailable to save the car");
        }
    }

    @Override
    public void update(HttpServletRequest request) throws DtoValidationException, LogicException {
        Entity entity = this.readOne(request);

        if (null == entity) {
            throw new LogicException("Invalid car requested");
        }

        if (((Car) entity).getStatus() == Car.Status.in_cruise) {
            throw new LogicException("Could not to edit car when it in cruise");
        }

        CarDto carDto = (CarDto) this.carObjectMapper.dtoFromRequest(request);

        this.getCurrentControllerFromRequest(request).saveSubmitDataToSession(request, carDto);

        Map<String, List<String>> errorMap = this.dtoValidation.validate(carDto, "en");

        try {
            Car.Category.valueOf(carDto.getCategory());
        } catch (Exception ex) {
            List<String> categoryErrors = new ArrayList<>();
            categoryErrors.add("Invalid category passed");

            errorMap.put("category", categoryErrors);
        }

        if (Car.Status.in_cruise.toString().equals(carDto.getStatus())) {
            List<String> statusErrors = new ArrayList<>();
            statusErrors.add("Invalid status passed");

            errorMap.put("status", statusErrors);
        }

        if (!errorMap.isEmpty()) {
            throw new DtoValidationException(errorMap);
        }

        Car car = (Car) this.carObjectMapper.entityFromDto(carDto);

        car.setIdCar(((Car) entity).getIdCar());

        this.carDao.update(car);
    }

    @Override
    public void delete(HttpServletRequest request) throws LogicException {
        Entity entity = this.carDao.select(this.getId(request, "id_car"));

        if (null == entity) {
            throw new LogicException("Invalid car passed");
        }

        if (!this.carDao.couldCarBeDelete(((Car) entity).getIdCar())) {
            throw new LogicException("Car could not be delete, cause orders exists");
        }

        this.carDao.delete(entity);
    }
}
