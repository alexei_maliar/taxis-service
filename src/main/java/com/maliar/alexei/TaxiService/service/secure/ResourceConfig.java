package com.maliar.alexei.TaxiService.service.secure;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.router.ConfigParser;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Config of access resources provider
 *
 * Depends on servlet context init param of access config filename
 */
public class ResourceConfig {
    private List<String> privilegedRoles;

    private List<String> privilegedResources;

    private String authenticationResource;

    private String registrationResource;

    public ResourceConfig() {
        this.init();
    }

    public List<String> getPrivilegedRoles() {
        return privilegedRoles;
    }

    public List<String> getPrivilegedResources() {
        return privilegedResources;
    }

    public String getAuthenticationResource() {
        return authenticationResource;
    }

    public String getRegistrationResource() {
        return registrationResource;
    }

    private void init() {
        URL configUrl = ConfigParser.class.getClassLoader().getResource(ConfigRegistry.getAccessConfigFilename());

        if (null == configUrl) {
            throw new IllegalArgumentException("Access config file does not exist or could not find");
        }

        XMLEventReader parser = getParser(configUrl.getPath());

        boolean bPrivilegedRole = false;
        boolean bPrivilegedResource = false;
        boolean bAuthenticationResource = false;
        boolean bRegistrationResource = false;

        try {
            List<String> roles = new ArrayList<>();
            List<String> resources = new ArrayList<>();
            String authRes = null;
            String regRes = null;

            while (parser.hasNext()) {
                XMLEvent event = parser.nextEvent();

                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedRoles")) {
                        roles = new ArrayList<>();
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedRole")) {
                        bPrivilegedRole = true;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedResources")) {
                        resources = new ArrayList<>();
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedResource")) {
                        bPrivilegedResource = true;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("authenticationResource")) {
                        bAuthenticationResource = true;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("registrationResource")) {
                        bRegistrationResource = true;
                    }
                }

                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedRoles")) {
                        this.privilegedRoles = roles;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedRole")) {
                        bPrivilegedRole = false;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedResources")) {
                        this.privilegedResources = resources;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("privilegedResource")) {
                        bPrivilegedResource = false;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("authenticationResource")) {
                        bAuthenticationResource = false;
                        this.authenticationResource = authRes;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("registrationResource")) {
                        bRegistrationResource = false;
                        this.registrationResource = regRes;
                    }
                }

                if (event.isCharacters()) {
                    Characters element = (Characters) event;

                    if (bPrivilegedRole) {
                        roles.add(element.getData());
                    }

                    if (bPrivilegedResource) {
                        resources.add(element.getData());
                    }

                    if (bAuthenticationResource) {
                        authRes = element.getData();
                    }
                    
                    if (bRegistrationResource) {
                        regRes = element.getData();
                    }
                }
            }
        } catch (Exception ex) {
            throw new IllegalStateException("Unavailable to parse access config");
        }
    }

    private XMLEventReader getParser(String configDirectoryPath) {
        File file = new File(configDirectoryPath);

        if (!file.exists()) {
            throw new IllegalStateException("System could not find access config");
        }

        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
            return factory.createXMLEventReader(new FileReader(file));
        } catch (Exception ex) {
            throw new IllegalStateException("System could not init config parser");
        }
    }
}
