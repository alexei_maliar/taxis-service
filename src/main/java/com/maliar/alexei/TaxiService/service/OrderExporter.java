package com.maliar.alexei.TaxiService.service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.Order;

import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service which helps to export list of orders into few file formats
 *
 * Result of export methods will be a new created file
 */
public class OrderExporter {
    /**
     * @param orders list of orders
     * @param request request
     * @return file name of new created file
     */
    public static String exportPdf(List<Entity> orders, HttpServletRequest request) {
        Path path = Paths.get(request.getServletContext().getRealPath("/temp_data"));

        try {
            Files.createDirectory(path);
        } catch (Exception ex) {
        }

        String result = "export_" + (new Date()).getTime() + ".pdf";

        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        try {
            Document document = new Document();

            PdfWriter.getInstance(
                    document,
                    new FileOutputStream(path.toString() + "/" + result)
            );

            document.open();

            Stream<String> cols = getColsStream(request);

            PdfPTable table = new PdfPTable(12);

            cols.forEach(col -> {
                PdfPCell header = new PdfPCell();
                header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                header.setBorderWidth(3);
                header.setPhrase(new Phrase(col));
                table.addCell(header);
            });

            for (Entity e: orders) {
                Order order = (Order) e;

                table.addCell(escapeSpecialCharacters(String.valueOf(order.getIdOrder())));
                table.addCell(escapeSpecialCharacters(order.getIdUser().getEmail()));
                table.addCell(escapeSpecialCharacters(order.getIdCar().getName()));

                if (multiLanguageProvider.getLang(request, true).equals(MultiLanguageProvider.RU)) {
                    table.addCell(escapeSpecialCharacters(order.getIdLocationFrom().getNameRu()));
                    table.addCell(escapeSpecialCharacters(order.getIdLocationTo().getNameRu()));
                } else {
                    table.addCell(escapeSpecialCharacters(order.getIdLocationFrom().getNameEn()));
                    table.addCell(escapeSpecialCharacters(order.getIdLocationTo().getNameEn()));
                }

                table.addCell(escapeSpecialCharacters(String.valueOf(order.getPrice())));
                table.addCell(escapeSpecialCharacters(String.valueOf(order.getDiscountPrice())));
                table.addCell(escapeSpecialCharacters(String.valueOf(order.getDistance())));
                table.addCell(escapeSpecialCharacters(order.getStatus().toString()));
                table.addCell(escapeSpecialCharacters(order.getCreatedDatetime().toString()));
                table.addCell(escapeSpecialCharacters(order.getArrivedDatetime().toString()));
                table.addCell(escapeSpecialCharacters(order.getCompletedDatetime().toString()));
            }

            document.add(table);
            document.close();
            return result;
        } catch (Exception ex) {
            throw new InternalException(ex);
        }
    }

    /**
     * @param orders list of orders
     * @param request request
     * @return file name of new created file
     */
    public static String exportCsv(List<Entity> orders, HttpServletRequest request) {
        Path path = Paths.get(request.getServletContext().getRealPath("/temp_data"));

        try {
            Files.createDirectory(path);
        } catch (Exception ex) {
        }

        String result = "export_" + (new Date()).getTime() + ".csv";

        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        List<String> lines = new ArrayList<>();

        lines.add(getColsStream(request).collect(Collectors.joining(",")));

        for (Entity e: orders) {
            Order order = (Order) e;

            String line = String.join(",",
                    escapeSpecialCharacters(String.valueOf(order.getIdOrder())),
                    escapeSpecialCharacters(order.getIdUser().getEmail()),
                    escapeSpecialCharacters(order.getIdCar().getName()),
                    escapeSpecialCharacters(multiLanguageProvider.getLang(request, false).equals(MultiLanguageProvider.RU) ? order.getIdLocationFrom().getNameRu() : order.getIdLocationFrom().getNameEn()),
                    escapeSpecialCharacters(multiLanguageProvider.getLang(request, false).equals(MultiLanguageProvider.RU) ? order.getIdLocationTo().getNameRu() : order.getIdLocationTo().getNameEn()),
                    escapeSpecialCharacters(String.valueOf(order.getPrice())),
                    escapeSpecialCharacters(String.valueOf(order.getDiscountPrice())),
                    escapeSpecialCharacters(String.valueOf(order.getDistance())),
                    escapeSpecialCharacters(order.getStatus().toString()),
                    escapeSpecialCharacters(order.getCreatedDatetime().toString()),
                    escapeSpecialCharacters(order.getArrivedDatetime().toString()),
                    escapeSpecialCharacters(order.getCompletedDatetime().toString())
            );

            lines.add(line);
        }

        try {
            Files.write(Paths.get(path.toString() + "/" + result), String.join(System.lineSeparator(), lines).getBytes());

            return result;
        } catch (Exception ex) {
            throw new InternalException();
        }
    }

    protected static Stream<String> getColsStream(HttpServletRequest request) {
        MultiLanguageProvider multiLanguageProvider = (MultiLanguageProvider) ServiceContainer.getInstance().get(MultiLanguageProvider.class.getName());

        return Stream.of(
                multiLanguageProvider.translate("id", request),
                multiLanguageProvider.translate("user", request),
                multiLanguageProvider.translate("car", request),
                multiLanguageProvider.translate("location_from", request),
                multiLanguageProvider.translate("location_to", request),
                multiLanguageProvider.translate("price", request),
                multiLanguageProvider.translate("discount_price", request),
                multiLanguageProvider.translate("distance", request),
                multiLanguageProvider.translate("status", request),
                multiLanguageProvider.translate("created_datetime", request),
                multiLanguageProvider.translate("arrived_datetime", request),
                multiLanguageProvider.translate("completed_datetime", request)
        );
    }

    protected static String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
}
