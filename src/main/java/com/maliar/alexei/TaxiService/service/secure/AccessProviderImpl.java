package com.maliar.alexei.TaxiService.service.secure;

import com.maliar.alexei.TaxiService.exception.InternalException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.dao.AbstractDao;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;

/**
 * Basic class implementation which provides user registration, access restriction and authentication features
 * Supports password hashing
 */
public class AccessProviderImpl implements AccessProvider {
    public static final String SESSION_PRINCIPAL_KEY = "principal";

    private final UserDao userDao;

    private final ResourceConfig resourceConfig;

    public AccessProviderImpl(AbstractDao userDao, ResourceConfig resourceConfig) {
        this.userDao = (UserDao) userDao;
        this.resourceConfig = resourceConfig;
    }

    @Override
    public boolean hasPrincipal(HttpServletRequest request) {
        return this.getPrincipal(request) != null;
    }

    @Override
    public Principal getPrincipal(HttpServletRequest request) {
        return (Principal) request.getSession(true).getAttribute(SESSION_PRINCIPAL_KEY);
    }

    @Override
    public void register(Entity entity, HttpServletRequest request) {
        User user = (User) entity;

        if (this.userDao.selectByEmail(user.getEmail()) != null) {
            throw new LogicException("User already exists");
        }

        user.setPassword(PasswordEncoder.getPasswordHash(user.getPassword()));

        this.userDao.insert(user);
    }

    @Override
    public void authenticate(Entity entity, HttpServletRequest request) {
        User user = (User) entity;

        String hash = PasswordEncoder.getPasswordHash(user.getPassword());

        user = (User) this.userDao.selectByEmail(user.getEmail());

        if (null == user) {
            throw new LogicException("Passed email does not exist");
        }

        if (!user.getPassword().equals(hash)) {
            throw new LogicException("Incorrect password passed");
        }

        Principal principal = new UserPrincipal(user);

        request.getSession(true).setAttribute(SESSION_PRINCIPAL_KEY, principal);
    }

    @Override
    public void logout(HttpServletRequest request) {
        HttpSession session = request.getSession(true);

        session.removeAttribute(SESSION_PRINCIPAL_KEY);
        session.invalidate();
    }

    @Override
    public boolean isEndpointAllowed(HttpServletRequest request) {
        return this.isEndpointAllowed(this.getRequestedUri(request), request);
    }

    @Override
    public boolean isEndpointAllowed(String endpoint, HttpServletRequest request) {
        boolean needCheck = false;

        for (String resource: this.resourceConfig.getPrivilegedResources()) {
            if (endpoint.startsWith(resource)) {
                needCheck = true;
                break;
            }
        }

        return !needCheck || (this.hasPrincipal(request) && this.resourceConfig.getPrivilegedRoles().contains(this.getPrincipal(request).getRole()));
    }

    @Override
    public String getAuthenticationEndpoint() {
        return this.resourceConfig.getAuthenticationResource();
    }

    @Override
    public boolean isAuthenticationEndpoint(HttpServletRequest request) {
        return this.resourceConfig.getAuthenticationResource().equals(this.getRequestedUri(request));
    }

    @Override
    public boolean isRegistrationEndpoint(HttpServletRequest request) {
        return this.resourceConfig.getRegistrationResource().equals(this.getRequestedUri(request));
    }

    private String getRequestedUri(HttpServletRequest request) {
        return request.getRequestURI().substring(request.getContextPath().length());
    }

    /**
     * Used for password hashing
     */
    private static class PasswordEncoder {
        private static final String salt = "supersecretsalt";

        private static final SecretKeyFactory secretKeyFactory;

        static {
            try {
                secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            } catch (Exception ex) {
                throw new InternalException("Could not to create hash factory", ex);
            }
        }

        protected static String getPasswordHash(String password) {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(StandardCharsets.UTF_8), 65536, 128);

            try {
                return new String(secretKeyFactory.generateSecret(spec).getEncoded());
            } catch (Exception ex) {
                throw new InternalException("Could not to hash password", ex);
            }
        }
    }
}
