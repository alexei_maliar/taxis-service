package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Location;

import java.util.Date;

/**
 * TODO: make discount calculation
 */
public interface TripCalculator {
    void setTariffRate(float rate);

    float getTariffRate();

    void setAverageSpeed(int averageSpeed);

    int getAverageSpeed();

    float getCategoryRate(Car.Category category);

    double calculatePrice(double distance, Car.Category category);

    double calculateDiscountPrice(double totalMoneySpentAmount, double price);

    double calculateDistance(Location from, Location to);

    Date calculateDate(Location from, Location to, Date date);

    Date calculateDate(double distance, Date date);
}
