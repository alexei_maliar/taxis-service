package com.maliar.alexei.TaxiService.service.secure;

import com.maliar.alexei.TaxiService.model.entity.Entity;

import javax.servlet.http.HttpServletRequest;

public interface AccessProvider {
    boolean hasPrincipal(HttpServletRequest request);

    Principal getPrincipal(HttpServletRequest request);

    void register(Entity user, HttpServletRequest request);

    void authenticate(Entity entity, HttpServletRequest request);

    void logout(HttpServletRequest request);

    boolean isEndpointAllowed(HttpServletRequest request);

    boolean isEndpointAllowed(String endpoint, HttpServletRequest request);

    boolean isAuthenticationEndpoint(HttpServletRequest request);

    String getAuthenticationEndpoint();

    boolean isRegistrationEndpoint(HttpServletRequest request);
}
