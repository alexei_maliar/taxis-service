package com.maliar.alexei.TaxiService.service;

import com.maliar.alexei.TaxiService.di.ServiceContainer;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Service which provides multi language support in application
 *
 * It works with request and response
 *
 * Translations should be in properties files, name of files should pass into servlet context init params
 *
 * Translations could be taken by key
 */
public class MultiLanguageProviderImpl implements MultiLanguageProvider {
    private final static String LANG_KEY = "lang";

    private final Map<String, ResourceBundle> translationBundlesMap;

    public MultiLanguageProviderImpl(Map<String, ResourceBundle> translationBundlesMap) {
        this.translationBundlesMap = translationBundlesMap;
    }

    @Override
    public void setLang(String lang, HttpServletResponse response) {
        if (!this.isLangSupported(lang)) {
            throw new IllegalArgumentException("Unsupported lang");
        }

        ServletContext servletContext = ServiceContainer.getInstance().getServletContext();

        Cookie cookie = new Cookie(LANG_KEY, lang);
        cookie.setMaxAge(60 * 60);
        cookie.setPath(servletContext.getContextPath());

        response.addCookie(cookie);
    }

    @Override
    public String getLang(HttpServletRequest request, boolean defaultIfAbsent) {
        String currentLang = null;

        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (Cookie cookie: cookies) {
                if (cookie.getName().equals(LANG_KEY)) {
                    currentLang = cookie.getValue();
                    break;
                }
            }
        }

        if (null == currentLang && defaultIfAbsent) {
            return DEFAULT_LANG;
        }

        return currentLang;
    }

    @Override
    public String translate(String key, HttpServletRequest request) {
        String lang = this.getLang(request, true);

        String _key = "translations." + key;

        if (!this.translationBundlesMap.containsKey(lang)
            || !this.translationBundlesMap.get(lang).containsKey(_key)
        ) {
            return key;
        }


        return this.translationBundlesMap.get(lang).getString(_key);
    }
}
