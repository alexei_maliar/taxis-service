package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.service.trip.exception.CarNotFoundException;
import com.maliar.alexei.TaxiService.service.trip.exception.UnavailableOrderCreateException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OrderMaker {
    List<Entity> getAvailableLocationList();

    Entity findCorrespondingCar(Dto orderDto) throws CarNotFoundException;

    Entity findCorrespondingCar(long idCar) throws CarNotFoundException;

    List<Entity> findSimilarCars(Dto orderDto) throws CarNotFoundException;

    Entity makeOrder(Dto orderDto, Entity car, HttpServletRequest request) throws UnavailableOrderCreateException;

    void saveOrder(Entity newOrder) throws UnavailableOrderCreateException;

    Entity getActiveOrder(HttpServletRequest request);
}
