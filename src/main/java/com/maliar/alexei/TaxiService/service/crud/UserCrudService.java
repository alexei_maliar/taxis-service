package com.maliar.alexei.TaxiService.service.crud;

import com.maliar.alexei.TaxiService.exception.DtoValidationException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.dto.AdminUserDto;
import com.maliar.alexei.TaxiService.model.entity.Entity;
import com.maliar.alexei.TaxiService.model.entity.User;
import com.maliar.alexei.TaxiService.model.mapper.AdminUserObjectMapper;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserCrudService implements CrudService {
    private final UserDao userDao;

    private final AdminUserObjectMapper adminUserObjectMapper;

    private final DtoValidation dtoValidation;

    public UserCrudService(UserDao userDao, AdminUserObjectMapper adminUserObjectMapper, DtoValidation dtoValidation) {
        this.userDao = userDao;
        this.adminUserObjectMapper = adminUserObjectMapper;
        this.dtoValidation = dtoValidation;
    }

    @Override
    public Entity readOne(HttpServletRequest request) {
        return this.userDao.select(this.getId(request, "id_user"));
    }

    @Override
    public Paginator readAll(HttpServletRequest request) {
        return this.userDao.selectAllWithPaginator(Paginator.getInstance(this.getCurrentPage(request)));
    }

    @Override
    public void create(HttpServletRequest request) throws DtoValidationException, LogicException {
        throw new IllegalStateException("Not supported");
    }

    @Override
    public void update(HttpServletRequest request) throws DtoValidationException, LogicException {
        Entity entity = this.readOne(request);

        if (null == entity) {
            throw new LogicException("Invalid user requested");
        }

        AdminUserDto adminUserDto = (AdminUserDto) this.adminUserObjectMapper.dtoFromRequest(request);

        this.getCurrentControllerFromRequest(request).saveSubmitDataToSession(request, adminUserDto);

        Map<String, List<String>> errorMap = this.dtoValidation.validate(adminUserDto, "en");

        try {
            User.Role.valueOf(adminUserDto.getRole());
        } catch (Exception ex) {
            List<String> roleErrors = new ArrayList<>();
            roleErrors.add("Invalid role passed");

            errorMap.put("role", roleErrors);
        }

        if (!errorMap.isEmpty()) {
            throw new DtoValidationException(errorMap);
        }

        User user = (User) this.adminUserObjectMapper.entityFromDto(adminUserDto);

        user.setIdUser(((User) entity).getIdUser());
        user.setEmail(((User) entity).getEmail());
        user.setPassword(((User) entity).getPassword());
        user.setRegistrationDate(((User) entity).getRegistrationDate());

        this.userDao.update(user);
    }

    @Override
    public void delete(HttpServletRequest request) throws LogicException {
        throw new IllegalStateException("Not supported");
    }
}
