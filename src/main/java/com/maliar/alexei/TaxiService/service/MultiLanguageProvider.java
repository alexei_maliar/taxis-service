package com.maliar.alexei.TaxiService.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface MultiLanguageProvider {
    String EN = "en";
    String RU = "ru";

    String DEFAULT_LANG = EN;

    void setLang(String lang, HttpServletResponse response);

    String getLang(HttpServletRequest request, boolean defaultIfAbsent);

    String translate(String key, HttpServletRequest request);

    default boolean isLangSupported(String lang) {
        return lang.equals(EN) || lang.equals(RU);
    }
}
