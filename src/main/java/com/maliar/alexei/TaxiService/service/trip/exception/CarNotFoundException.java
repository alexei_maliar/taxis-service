package com.maliar.alexei.TaxiService.service.trip.exception;

public class CarNotFoundException extends Exception {
    public CarNotFoundException() {
    }

    public CarNotFoundException(String message) {
        super(message);
    }

    public CarNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
