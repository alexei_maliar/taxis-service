package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import org.apache.logging.log4j.LogManager;

/**
 * Service which switch status of orders.
 * Result of working this service depends on order's dates.
 *
 * It initialized as Runnable task of PoolThreadExecutor
 *
 * Logic of this service might be implemented as mysql event, but I decided
 * choose this approach
 *
 */
public class TripDispatcherImpl implements TripDispatcher {
    private final OrderDao orderDao;

    public TripDispatcherImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public synchronized void updateOrdersStatus() {
        LogManager.getRootLogger().info("Update orders status job started");

        try {
            this.orderDao.updateScheduledOrdersStatus();
        } catch (Exception ex) {
            LogManager.getRootLogger().error(ex.getMessage(), ex);
        }

        LogManager.getRootLogger().info("Update orders status job finished");
    }

    @Override
    public void run() {
        this.updateOrdersStatus();
    }
}

