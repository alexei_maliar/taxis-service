package com.maliar.alexei.TaxiService.service.crud;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.controller.admin.AbstractAdminController;
import com.maliar.alexei.TaxiService.exception.DtoValidationException;
import com.maliar.alexei.TaxiService.exception.LogicException;
import com.maliar.alexei.TaxiService.model.Paginator;
import com.maliar.alexei.TaxiService.model.entity.Entity;

import javax.servlet.http.HttpServletRequest;

/**
 * Service for Create Read Update and Delete operations in admin panel for defined entities
 */
public interface CrudService {
    Entity readOne(HttpServletRequest request);

    Paginator readAll(HttpServletRequest request);

    void create(HttpServletRequest request) throws DtoValidationException, LogicException;

    void update(HttpServletRequest request) throws DtoValidationException, LogicException;

    void delete(HttpServletRequest request) throws LogicException;

    default int getCurrentPage(HttpServletRequest request) {
        int page = 1;

        if (null == request.getParameter("page")) {
            return page;
        }

        try {
            page = Math.abs(Integer.parseInt(request.getParameter("page")));
        } catch (Exception ignored) {
        }

        return page;
    }

    default long getId(HttpServletRequest request, String name) {
        try {
            if (null == request.getParameter(name)) {
                throw new Exception();
            }

            return Math.abs(Long.parseLong(request.getParameter(name)));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid id passed to request");
        }
    }

    /**
     * Need for setting some submit data or error data into session
     *
     * Controllers passed into request in admin controller inherits
     *
     * @param request request
     * @return Current controller
     */
    default ActionController getCurrentControllerFromRequest(HttpServletRequest request) {
        Object controller = request.getAttribute(AbstractAdminController.CONTROLLER_KEY);

        if (!(controller instanceof AbstractAdminController)) {
            throw new IllegalStateException("Any controller has not been stored to request");
        }

        return (ActionController) controller;
    }
}
