package com.maliar.alexei.TaxiService.service.trip;

public interface TripDispatcher extends Runnable {
    void updateOrdersStatus();
}