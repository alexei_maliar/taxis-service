package com.maliar.alexei.TaxiService.service.trip.exception;

public class UnavailableOrderCreateException extends Exception {
    public UnavailableOrderCreateException() {
    }

    public UnavailableOrderCreateException(String message) {
        super(message);
    }

    public UnavailableOrderCreateException(Throwable cause) {
        super(cause);
    }

    public UnavailableOrderCreateException(String message, Throwable cause) {
        super(message, cause);
    }
}
