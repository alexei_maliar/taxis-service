package com.maliar.alexei.TaxiService.service.secure;

import com.maliar.alexei.TaxiService.model.entity.User;

public interface Principal {
    String getEmail();

    String getRole();

    User getUser();
}
