package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Location;

import java.text.NumberFormat;
import java.util.Date;

/**
 * Calculator of trip order properties like distance, price etc...
 *
 * Should be initialized in ContextListener
 * Params for initialization should be in properties file
 * Name of this files defined in servlet context init params
 */
public class TripCalculatorImpl implements TripCalculator {
    private static final long ONE_MINUTE_IN_MILLIS = 60000;

    private float tariffRate = 0;

    private int averageSpeed = 0;

    @Override
    public void setTariffRate(float rate) {
        if (0 == rate) {
            throw new IllegalArgumentException("Rate should be greater than 0");
        }

        this.tariffRate = rate;
    }

    @Override
    public float getTariffRate() {
        if (0 == this.tariffRate) {
            throw new IllegalStateException("Tariff rate should be initialized");
        }

        return this.tariffRate;
    }

    @Override
    public void setAverageSpeed(int averageSpeed) {
        if (0 == averageSpeed) {
            throw new IllegalArgumentException("Average speed should be greater than 0");
        }

        this.averageSpeed = averageSpeed;
    }

    @Override
    public int getAverageSpeed() {
        if (0 == this.averageSpeed) {
            throw new IllegalArgumentException("Average speed should be initialized");
        }

        return this.averageSpeed;
    }

    @Override
    public float getCategoryRate(Car.Category category) {
        if (category == Car.Category.economy) {
            return 0.85F;
        } else if (category == Car.Category.standard) {
            return 1F;
        } else {
            return 1.35F;
        }
    }

    @Override
    public double calculatePrice(double distance, Car.Category category) {
        return Math.round(distance * this.getTariffRate() * this.getCategoryRate(category) * 100.00) / 100.00;
    }

    @Override
    public double calculateDiscountPrice(double totalMoneySpentAmount, double price) {
        double multiplier = 0;

        if (totalMoneySpentAmount >= 250 && totalMoneySpentAmount < 500) {
            multiplier = 0.05;
        }

        if (totalMoneySpentAmount >= 500 && totalMoneySpentAmount < 1000) {
            multiplier = 0.1;
        }

        if (totalMoneySpentAmount > 1000) {
            multiplier = 0.15;
        }

        return Math.round(multiplier * price * 100.00) / 100.00;
    }

    @Override
    public double calculateDistance(Location from, Location to) {
        double res = Math.abs(to.getY() - from.getY()) + Math.abs(to.getX() - from.getX());

        if (0 == res) {
            return 1;
        }

        return res;
    }

    @Override
    public Date calculateDate(Location from, Location to, Date date) {
        return this.calculateDate(this.calculateDistance(from, to), date);
    }

    @Override
    public Date calculateDate(double distance, Date date) {
        if (distance <= 0) {
            distance = 1;
        }

        // from km/h to km/m
        double speed = 1.0 * this.getAverageSpeed() / 60;

        double timeInMinutes = distance / speed;

        return new Date((long) (date.getTime() + (timeInMinutes * ONE_MINUTE_IN_MILLIS)));
    }
}
