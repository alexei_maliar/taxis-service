package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.dao.LocationDao;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.dto.Dto;
import com.maliar.alexei.TaxiService.model.dto.OrderDto;
import com.maliar.alexei.TaxiService.model.entity.*;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;
import com.maliar.alexei.TaxiService.service.trip.exception.CarNotFoundException;
import com.maliar.alexei.TaxiService.service.trip.exception.UnavailableOrderCreateException;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Service for creating new orders
 *
 * Also it could returns available locations and finds needed cars
 */
public class OrderMakerImpl implements OrderMaker {
    private final LocationDao locationDao;

    private final CarDao carDao;

    private final OrderDao orderDao;

    private final OrderObjectMapper orderObjectMapper;

    private final TripCalculator tripCalculator;

    private final AccessProvider accessProvider;

    public OrderMakerImpl(
            LocationDao locationDao,
            CarDao carDao,
            OrderDao orderDao,
            OrderObjectMapper orderObjectMapper,
            TripCalculator tripCalculator,
            AccessProvider accessProvider
    ) {
        this.locationDao = locationDao;
        this.carDao = carDao;
        this.orderDao = orderDao;
        this.orderObjectMapper = orderObjectMapper;
        this.tripCalculator = tripCalculator;
        this.accessProvider = accessProvider;
    }
    @Override
    public List<Entity> getAvailableLocationList() {
        return this.locationDao.selectAll();
    }

    @Override
    public Entity findCorrespondingCar(Dto orderDto) throws CarNotFoundException {
        OrderDto dto = (OrderDto) orderDto;

        Entity car = this.carDao.selectCorrespondingCar(dto.getSeatCount(), dto.getCategory());

        if (null == car) {
            throw new CarNotFoundException();
        }

        return car;
    }

    @Override
    public Entity findCorrespondingCar(long idCar) throws CarNotFoundException {
        Entity car = this.carDao.select(idCar);

        if (null == car) {
            throw new CarNotFoundException();
        }

        return car;
    }

    @Override
    public List<Entity> findSimilarCars(Dto orderDto) throws CarNotFoundException {
        OrderDto dto = (OrderDto) orderDto;

        List<Entity> cars = this.carDao.selectSimilarCars(dto.getSeatCount(), dto.getCategory());

        if (cars.isEmpty()) {
            throw new CarNotFoundException();
        }

        return cars;
    }

    @Override
    public Entity makeOrder(Dto orderDto, Entity car, HttpServletRequest request) throws UnavailableOrderCreateException {
        OrderDto dto = (OrderDto) orderDto;

        User user = this.accessProvider.getPrincipal(request).getUser();

        if (car instanceof Car) {
            car = this.carDao.select(((Car) car).getIdCar());
        }

        if (null == car || !((Car) car).getStatus().equals(Car.Status.pending_order)) {
            throw new UnavailableOrderCreateException();
        }

        Entity locationFrom = this.locationDao.select(dto.getIdLocationFrom());
        Entity locationTo = this.locationDao.select(dto.getIdLocationTo());

        if (null == locationFrom || null == locationTo) {
            throw new UnavailableOrderCreateException();
        }

        double distance = this.tripCalculator.calculateDistance((Location) locationFrom, (Location) locationTo);

        double price = this.tripCalculator.calculatePrice(distance, ((Car) car).getCategory());

        double discountPrice = this.tripCalculator.calculateDiscountPrice(
                this.orderDao.selectTotalMoneySpentForUser(user.getIdUser()),
                price
        );

        if (discountPrice > 0) {
            price -= discountPrice;
        }

        Date createdDate = new Date();

        Date arrivedDate;

        Entity currentCarLocation = this.locationDao.selectCurrentLocationForCar(((Car) car).getIdCar());

        if (null != currentCarLocation) {
            arrivedDate = this.tripCalculator.calculateDate((Location) currentCarLocation, (Location) locationFrom, createdDate);
        } else {
            arrivedDate = this.tripCalculator.calculateDate(1, createdDate);
        }

        Date completedDate = this.tripCalculator.calculateDate((Location) locationFrom, (Location) locationTo, arrivedDate);

        Order order = (Order) this.orderObjectMapper.entityFromDto(dto);

        order.setIdUser(user);
        order.setIdCar((Car) car);
        order.setPrice(price);
        order.setDiscountPrice(discountPrice);
        order.setStatus(Order.Status.pending);
        order.setDistance(distance);
        order.setCreatedDatetime(createdDate);
        order.setArrivedDatetime(arrivedDate);
        order.setCompletedDatetime(completedDate);

        return order;
    }

    @Override
    public void saveOrder(Entity newOrder) throws UnavailableOrderCreateException {
        if (null == newOrder || ((Order) newOrder).getIdOrder() > 0) {
            throw new UnavailableOrderCreateException();
        }

        this.orderDao.insert(newOrder);

        if (((Order) newOrder).getIdOrder() == 0) {
            throw new UnavailableOrderCreateException();
        }
    }

    @Override
    public Entity getActiveOrder(HttpServletRequest request) {
        return this.orderDao.selectActiveOrderForUser(
                this.accessProvider.getPrincipal(request).getUser().getIdUser()
        );
    }
}
