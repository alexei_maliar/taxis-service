package com.maliar.alexei.TaxiService.service.secure;

import com.maliar.alexei.TaxiService.model.entity.User;

/**
 * Entity which provides description of current user and contains corresponding User entity for more details and actions
 */
public class UserPrincipal implements Principal {
    private final User user;

    public UserPrincipal(User user) {
        if (user == null || user.getIdUser() < 1) {
            throw new IllegalArgumentException("User should be exist.");
        }

        this.user = user;
    }

    @Override
    public String getEmail() {
        return this.user.getEmail();
    }

    @Override
    public String getRole() {
        return this.user.getRole().toString();
    }

    @Override
    public User getUser() {
        return this.user;
    }
}
