package com.maliar.alexei.TaxiService.exception;

import java.util.List;
import java.util.Map;

public class DtoValidationException extends Exception {
    private final Map<String, List<String>> errorMessagesMap;

    public DtoValidationException(Map<String, List<String>> errorMessagesMap) {
        if (errorMessagesMap.isEmpty()) {
            throw new IllegalArgumentException("Error messages map should not be empty");
        }

        this.errorMessagesMap = errorMessagesMap;
    }

    public Map<String, List<String>> getErrorMessagesMap() {
        return errorMessagesMap;
    }
}
