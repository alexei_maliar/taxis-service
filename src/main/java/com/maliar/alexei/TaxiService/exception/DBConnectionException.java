package com.maliar.alexei.TaxiService.exception;

public class DBConnectionException extends InternalException {
    public DBConnectionException() {
    }

    public DBConnectionException(String message) {
        super(message);
    }

    public DBConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
