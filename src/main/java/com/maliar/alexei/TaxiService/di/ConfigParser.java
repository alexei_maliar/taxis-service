package com.maliar.alexei.TaxiService.di;

import com.maliar.alexei.TaxiService.ConfigRegistry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

/**
 * Class for parsing config with all application dependencies
 */
public class ConfigParser {
    public static Map<Object, Object> parse() {
        Map<Object, Object> result = new Hashtable<>();

        URL configUrl = ConfigParser.class.getClassLoader().getResource(ConfigRegistry.getDiConfigFilename());

        if (null == configUrl) {
            throw new IllegalArgumentException("DI config file does not exist or could not find");
        }

        XMLEventReader parser = getParser(configUrl.getPath());

        boolean bKey = false;
        boolean bFactory = false;

        try {
            Object key = null;
            Object factory = null;

            while (parser.hasNext()) {
                XMLEvent event = parser.nextEvent();

                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("dependency")) {
                        key = null;
                        factory = null;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("key")) {
                        bKey = true;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("factory")) {
                        bFactory = true;
                    }
                }

                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;

                    if (element.getName().getLocalPart().equalsIgnoreCase("dependency")) {
                        result.put(key, factory);
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("key")) {
                        bKey = false;
                    }

                    if (element.getName().getLocalPart().equalsIgnoreCase("factory")) {
                        bFactory = false;
                    }
                }

                if (event.isCharacters()) {
                    Characters element = (Characters) event;

                    if (bKey) {
                        key = element.getData();
                    }

                    if (bFactory) {
                        factory = element.getData();
                    }
                }
            }
        } catch (Exception ex) {
            throw new IllegalStateException("Unavailable to parse dependencies config");
        }

        return result;
    }

    private static XMLEventReader getParser(String configDirectoryPath) {
        File file = new File(configDirectoryPath);

        if (!file.exists()) {
            throw new IllegalStateException("System could not find dependencies config");
        }

        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
           return factory.createXMLEventReader(new FileReader(file));
        } catch (Exception ex) {
            throw new IllegalStateException("System could not init config parser");
        }
    }
}
