package com.maliar.alexei.TaxiService.di;

/**
 * All factories should implements this interface
 */
public interface ServiceFactory {
    Object factory(Object requestedKey, ServiceContainer serviceContainer);
}
