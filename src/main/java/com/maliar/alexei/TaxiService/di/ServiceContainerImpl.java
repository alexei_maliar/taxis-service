package com.maliar.alexei.TaxiService.di;

import javax.servlet.ServletContext;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Dependency injection implementation
 * Uses ServiceLocator pattern
 *
 * Based on factory approach
 *
 * File name with map of config dependency name to factory class name should passed as servlet context init param
 */
public class ServiceContainerImpl implements ServiceContainer {
    private static final String SC_KEY = ServletContext.class.getName();

    private static volatile ServiceContainer instance;

    private final Map<Object, Object> instancesMap = new ConcurrentHashMap<>();

    private Map<Object, Object> configMap;

    private ServiceContainerImpl() {
        // default constructor
    }

    public static ServiceContainer getInstance() {
        ServiceContainer localInstance = instance;

        if (null == localInstance) {
            synchronized (ServiceContainerImpl.class) {

                localInstance = instance;

                if (null == localInstance) {
                    instance = localInstance = new ServiceContainerImpl();
                }
            }
        }

        return localInstance;
    }

    @Override
    public void setConfigMap(Map<Object, Object> configMap) {
        if (null != configMap && configMap.size() == 0) {
            throw new IllegalArgumentException("Dependencies config is empty");
        }

        this.configMap = configMap;
    }

    @Override
    public Object get(Object key) {
        if (!this.instancesMap.containsKey(key)) {
            this.instancesMap.put(key, this.create(key));
        }

        return this.instancesMap.get(key);
    }

    @Override
    public Object create(Object key) {
        if (null == this.configMap) {
            throw new IllegalStateException("Config map should be initialized");
        }

        if (!this.configMap.containsKey(key)) {
            throw new IllegalArgumentException("Service " + key + " is missing in dependencies config");
        }

        try {
            Class<?> factoryClass = Class.forName((String) this.configMap.get(key));

            ServiceFactory factory = (ServiceFactory) factoryClass.getConstructor().newInstance();

            return factory.factory(key, this);
        } catch (Exception ex) {
            throw new IllegalArgumentException("Factory is missing for service " + ex.getMessage(), ex);
        }
    }

    @Override
    public void set(Object key, Object service) {
        if (key == null || service == null) {
            throw new IllegalArgumentException("Null passed as key or service, object expected");
        }

        this.instancesMap.put(key, service);
    }

    @Override
    public void remove(Object key) {
        if (key == null) {
            throw new IllegalArgumentException("Null passed as key, object expected");
        }

        this.instancesMap.remove(key);
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.set(SC_KEY, servletContext);
    }

    @Override
    public ServletContext getServletContext() {
        Object context;

        if (!this.instancesMap.containsKey(SC_KEY)
                || (context = this.instancesMap.get(SC_KEY)) == null
                || !(context instanceof ServletContext)
        ) {
            throw new IllegalStateException("Servlet context has not initialized");
        }

        return (ServletContext) context;
    }
}