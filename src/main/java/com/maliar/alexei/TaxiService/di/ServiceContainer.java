package com.maliar.alexei.TaxiService.di;

import javax.servlet.ServletContext;
import java.util.Map;

public interface ServiceContainer {
    void setConfigMap(Map<Object, Object> configMap);

    Object get(Object key);
    
    Object create(Object key);

    void set(Object key, Object service);

    void remove(Object key);

    void setServletContext(ServletContext servletContext);

    ServletContext getServletContext();

    static ServiceContainer getInstance() {
        return ServiceContainerImpl.getInstance();
    }
}