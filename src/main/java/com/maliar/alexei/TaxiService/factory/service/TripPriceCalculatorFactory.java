package com.maliar.alexei.TaxiService.factory.service;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.trip.TripCalculatorImpl;

public class TripPriceCalculatorFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new TripCalculatorImpl();
    }
}
