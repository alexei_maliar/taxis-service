package com.maliar.alexei.TaxiService.factory.service.crud;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.mapper.CarObjectMapper;
import com.maliar.alexei.TaxiService.service.crud.CarCrudService;

public class CarCrudServiceFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new CarCrudService(
                (CarDao) serviceContainer.get(CarDao.class.getName()),
                (CarObjectMapper) serviceContainer.get(CarObjectMapper.class.getName()),
                (DtoValidation) serviceContainer.get(DtoValidation.class.getName())
        );
    }
}
