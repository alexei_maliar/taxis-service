package com.maliar.alexei.TaxiService.factory.model.dao;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.LocationDao;
import com.maliar.alexei.TaxiService.model.mapper.LocationObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

public class LocationDaoFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new LocationDao(
            (DBManager) serviceContainer.get(DBManager.class.getName()),
            (ObjectMapper) serviceContainer.get(LocationObjectMapper.class.getName())
        );
    }
}
