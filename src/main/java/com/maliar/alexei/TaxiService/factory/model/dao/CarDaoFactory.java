package com.maliar.alexei.TaxiService.factory.model.dao;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.mapper.CarObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

public class CarDaoFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new CarDao(
                (DBManager) serviceContainer.get(DBManager.class.getName()),
                (ObjectMapper) serviceContainer.get(CarObjectMapper.class.getName())
        );
    }
}
