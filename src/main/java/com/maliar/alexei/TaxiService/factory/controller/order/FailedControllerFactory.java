package com.maliar.alexei.TaxiService.factory.controller.order;

import com.maliar.alexei.TaxiService.controller.order.FailedController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;

public class FailedControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new FailedController(serviceContainer.getServletContext());
    }
}
