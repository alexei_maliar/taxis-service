package com.maliar.alexei.TaxiService.factory.service.secure;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.service.secure.AccessProviderImpl;
import com.maliar.alexei.TaxiService.service.secure.ResourceConfig;

public class AccessProviderFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new AccessProviderImpl((UserDao) serviceContainer.get(UserDao.class.getName()), new ResourceConfig());
    }
}
