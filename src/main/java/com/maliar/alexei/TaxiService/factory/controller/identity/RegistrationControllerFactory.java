package com.maliar.alexei.TaxiService.factory.controller.identity;

import com.maliar.alexei.TaxiService.controller.identity.RegistrationController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.UserObjectMapper;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.ServletContext;

public class RegistrationControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new RegistrationController(
                (ServletContext) serviceContainer.get(ServletContext.class.getName()),
                (ObjectMapper) serviceContainer.get(UserObjectMapper.class.getName()),
                (DtoValidation) serviceContainer.get(DtoValidation.class.getName()),
                (AccessProvider) serviceContainer.get(AccessProvider.class.getName())
        );
    }
}
