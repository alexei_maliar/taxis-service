package com.maliar.alexei.TaxiService.factory.service.crud;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.mapper.AdminUserObjectMapper;
import com.maliar.alexei.TaxiService.service.crud.UserCrudService;

public class UserCrudServiceFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new UserCrudService(
                (UserDao) serviceContainer.get(UserDao.class.getName()),
                (AdminUserObjectMapper) serviceContainer.get(AdminUserObjectMapper.class.getName()),
                (DtoValidation) serviceContainer.get(DtoValidation.class.getName())
        );
    }
}
