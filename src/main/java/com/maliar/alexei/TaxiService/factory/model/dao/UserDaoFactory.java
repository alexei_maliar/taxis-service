package com.maliar.alexei.TaxiService.factory.model.dao;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.UserObjectMapper;

public class UserDaoFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new UserDao(
                (DBManager) serviceContainer.get(DBManager.class.getName()),
                (ObjectMapper) serviceContainer.get(UserObjectMapper.class.getName())
        );
    }
}

