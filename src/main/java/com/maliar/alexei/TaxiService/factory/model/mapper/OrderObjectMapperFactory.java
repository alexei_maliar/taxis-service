package com.maliar.alexei.TaxiService.factory.model.mapper;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.dao.AbstractDao;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.dao.LocationDao;
import com.maliar.alexei.TaxiService.model.dao.UserDao;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;

public class OrderObjectMapperFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new OrderObjectMapper(
                (AbstractDao) serviceContainer.get(UserDao.class.getName()),
                (AbstractDao) serviceContainer.get(CarDao.class.getName()),
                (AbstractDao) serviceContainer.get(LocationDao.class.getName())
        );
    }
}
