package com.maliar.alexei.TaxiService.factory.controller.identity;

import com.maliar.alexei.TaxiService.controller.identity.LogoutController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;

import javax.servlet.ServletContext;

public class LogoutControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new LogoutController(
                (ServletContext) serviceContainer.get(ServletContext.class.getName()),
                (AccessProvider) serviceContainer.get(AccessProvider.class.getName())
        );
    }
}
