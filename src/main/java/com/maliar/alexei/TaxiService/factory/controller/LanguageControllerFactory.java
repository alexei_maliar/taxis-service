package com.maliar.alexei.TaxiService.factory.controller;

import com.maliar.alexei.TaxiService.controller.LanguageController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;

import javax.servlet.ServletContext;

public class LanguageControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new LanguageController(
                serviceContainer.getServletContext(),
                (MultiLanguageProvider) serviceContainer.get(MultiLanguageProvider.class.getName())
        );
    }
}
