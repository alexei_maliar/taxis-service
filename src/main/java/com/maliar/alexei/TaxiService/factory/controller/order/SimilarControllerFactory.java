package com.maliar.alexei.TaxiService.factory.controller.order;

import com.maliar.alexei.TaxiService.controller.order.SimilarController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;

public class SimilarControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new SimilarController(serviceContainer.getServletContext(), (OrderMaker) serviceContainer.get(OrderMaker.class.getName()));
    }
}
