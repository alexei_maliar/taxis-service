package com.maliar.alexei.TaxiService.factory.service;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.service.trip.TripDispatcherImpl;

public class TripDispatcherFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new TripDispatcherImpl((OrderDao) serviceContainer.get(OrderDao.class.getName()));
    }
}


