package com.maliar.alexei.TaxiService.factory.controller.order;

import com.maliar.alexei.TaxiService.controller.order.ConfirmationController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;

public class ConfirmationControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new ConfirmationController(serviceContainer.getServletContext(), (OrderMaker) serviceContainer.get(OrderMaker.class.getName()));
    }
}
