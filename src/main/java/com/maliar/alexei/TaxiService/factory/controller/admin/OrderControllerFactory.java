package com.maliar.alexei.TaxiService.factory.controller.admin;

import com.maliar.alexei.TaxiService.controller.admin.OrderController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.dao.UserDao;

public class OrderControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new OrderController(
                serviceContainer.getServletContext(),
                (OrderDao) serviceContainer.get(OrderDao.class.getName()),
                (UserDao) serviceContainer.get(UserDao.class.getName())
        );
    }
}
