package com.maliar.alexei.TaxiService.factory.service;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.MultiLanguageProvider;
import com.maliar.alexei.TaxiService.service.MultiLanguageProviderImpl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class MultiLanguageProviderFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        Map<String, ResourceBundle> map = new HashMap<>();

        map.put(MultiLanguageProvider.EN, ResourceBundle.getBundle(ConfigRegistry.getInternalizationConfigFilename(), new Locale("en", "US")));
        map.put(MultiLanguageProvider.RU, ResourceBundle.getBundle(ConfigRegistry.getInternalizationConfigFilename(), new Locale("ru", "RU")));

        return new MultiLanguageProviderImpl(map);
    }
}
