package com.maliar.alexei.TaxiService.factory.controller.admin;

import com.maliar.alexei.TaxiService.controller.admin.CarController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.crud.CarCrudService;

public class CarControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new CarController(
                serviceContainer.getServletContext(),
                (CarCrudService) serviceContainer.get(CarCrudService.class.getName())
        );
    }
}
