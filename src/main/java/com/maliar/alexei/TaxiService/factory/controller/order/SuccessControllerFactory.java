package com.maliar.alexei.TaxiService.factory.controller.order;

import com.maliar.alexei.TaxiService.controller.order.SuccessController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;

public class SuccessControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new SuccessController(serviceContainer.getServletContext(), (OrderMaker) serviceContainer.get(OrderMaker.class.getName()));
    }
}
