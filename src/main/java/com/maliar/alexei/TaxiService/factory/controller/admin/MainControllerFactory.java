package com.maliar.alexei.TaxiService.factory.controller.admin;

import com.maliar.alexei.TaxiService.controller.admin.MainController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;

public class MainControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new MainController(serviceContainer.getServletContext());
    }
}
