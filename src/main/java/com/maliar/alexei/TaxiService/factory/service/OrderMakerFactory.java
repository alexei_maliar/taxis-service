package com.maliar.alexei.TaxiService.factory.service;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.dao.LocationDao;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;
import com.maliar.alexei.TaxiService.service.trip.OrderMakerImpl;
import com.maliar.alexei.TaxiService.service.trip.TripCalculator;

public class OrderMakerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new OrderMakerImpl(
                (LocationDao) serviceContainer.get(LocationDao.class.getName()),
                (CarDao) serviceContainer.get(CarDao.class.getName()),
                (OrderDao) serviceContainer.get(OrderDao.class.getName()),
                (OrderObjectMapper) serviceContainer.get(OrderObjectMapper.class.getName()),
                (TripCalculator) serviceContainer.get(TripCalculator.class.getName()),
                (AccessProvider) serviceContainer.get(AccessProvider.class.getName())
        );
    }
}
