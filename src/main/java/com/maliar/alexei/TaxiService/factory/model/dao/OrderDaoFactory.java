package com.maliar.alexei.TaxiService.factory.model.dao;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;

public class OrderDaoFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new OrderDao(
                (DBManager) serviceContainer.get(DBManager.class.getName()),
                (ObjectMapper) serviceContainer.get(OrderObjectMapper.class.getName())
        );
    }
}

