package com.maliar.alexei.TaxiService.factory.model;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManagerImpl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBManagerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        try {
            Context context = new InitialContext();

            return new DBManagerImpl((DataSource) context.lookup("java:comp/env/jdbc/letsGoPool"));
        } catch (Exception ex) {
            throw new IllegalStateException("Could not create DB manager", ex);
        }
    }
}

