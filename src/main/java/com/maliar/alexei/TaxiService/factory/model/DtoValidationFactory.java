package com.maliar.alexei.TaxiService.factory.model;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DtoValidationImpl;

public class DtoValidationFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new DtoValidationImpl();
    }
}
