package com.maliar.alexei.TaxiService.factory.controller;

import com.maliar.alexei.TaxiService.controller.MainController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;

import javax.servlet.ServletContext;

public class MainControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new MainController((ServletContext) serviceContainer.get(ServletContext.class.getName()));
    }
}
