package com.maliar.alexei.TaxiService.factory.model;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.AbstractDao;

public class AbstractDaoFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        try {
            Class<?> daoClass = Class.forName((String) requestedKey);

            if (!daoClass.getSuperclass().isAssignableFrom(AbstractDao.class)) {
                throw new IllegalArgumentException("Invalid instance implementation passed");
            }

            return daoClass.getConstructor(DBManager.class).newInstance((DBManager) serviceContainer.get(DBManager.class.getName()));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unavailable to create dao:" + requestedKey);
        }
    }
}

