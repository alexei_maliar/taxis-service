package com.maliar.alexei.TaxiService.factory.model;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;

public class AbstractObjectMapperFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        try {
            Class<?> objectMapperClass = Class.forName((String) requestedKey);

            if (!objectMapperClass.getSuperclass().isAssignableFrom(ObjectMapper.class)) {
                throw new IllegalArgumentException("Invalid instance implementation passed");
            }

            return objectMapperClass.getConstructor().newInstance();
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unavailable to create dao:" + requestedKey, ex);
        }
    }
}
