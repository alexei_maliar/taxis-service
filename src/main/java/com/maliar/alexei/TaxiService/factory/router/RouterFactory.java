package com.maliar.alexei.TaxiService.factory.router;

import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.router.ConfigParser;
import com.maliar.alexei.TaxiService.router.RouterImpl;

public class RouterFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new RouterImpl(serviceContainer, ConfigParser.parse());
    }
}
