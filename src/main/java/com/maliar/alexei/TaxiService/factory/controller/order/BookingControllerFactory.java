package com.maliar.alexei.TaxiService.factory.controller.order;

import com.maliar.alexei.TaxiService.controller.order.BookingController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.model.DtoValidation;
import com.maliar.alexei.TaxiService.model.mapper.ObjectMapper;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;
import com.maliar.alexei.TaxiService.service.trip.OrderMaker;

public class BookingControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new BookingController(
                serviceContainer.getServletContext(),
                (OrderMaker) serviceContainer.get(OrderMaker.class.getName()),
                (ObjectMapper) serviceContainer.get(OrderObjectMapper.class.getName()),
                (DtoValidation) serviceContainer.get(DtoValidation.class.getName())
        );
    }
}

