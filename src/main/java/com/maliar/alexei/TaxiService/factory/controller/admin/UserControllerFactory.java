package com.maliar.alexei.TaxiService.factory.controller.admin;

import com.maliar.alexei.TaxiService.controller.admin.UserController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.di.ServiceFactory;
import com.maliar.alexei.TaxiService.service.crud.UserCrudService;

public class UserControllerFactory implements ServiceFactory {
    @Override
    public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
        return new UserController(
                serviceContainer.getServletContext(),
                (UserCrudService) serviceContainer.get(UserCrudService.class.getName())
        );
    }
}
