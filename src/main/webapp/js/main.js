class Main {
    constructor() {
        this.run();
    }

    run = () => {
        $(document).ready(() => {
            this.#initFormElements();
            this.#initDropdown();
            this.#initSideNav();
        });
    }

    #initDropdown = () => {
        $(".dropdown-trigger").dropdown();
    }

    #initFormElements = () => {
       $('select').formSelect();
       $('.datepicker').datepicker({format: "yyyy-mm-dd"});
    }

    #initSideNav = () => {
        $('.sidenav').sidenav();
    }
}

new Main();