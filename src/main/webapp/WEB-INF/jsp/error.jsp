<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>
<% response.setStatus(404); %>
<t:layout>
    <jsp:body>
        <div class="container text-accent-1">
            <div class="col s12 center-align">
                <h1><e:translate message="something_went_wrong" /></h1>
            </div>
        </div>
    </jsp:body>
</t:layout>