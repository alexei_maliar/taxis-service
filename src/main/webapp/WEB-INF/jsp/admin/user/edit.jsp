<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:admin-layout>
    <jsp:body>
        <div class="admin-car-container container">
            <div class="row">
                <div class="col s12">
                    <e:link value="back" url="/admin/user"/>
                </div>
            </div>
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="user_edit"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="user"/>:
                        <c:out value="${entity.getEmail()}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="registration_date"/>:
                        <c:out value="${entity.getRegistrationDate()}" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <form action="" method="post" name="admin-user-edit">
                        <c:set var="idUserVal"><e:formElementValue getterSubmitData="getIdUser" getterEntity="getIdUser"/></c:set>

                        <input type="hidden" name="id_user" value="${idUserValue}">

                        <div class="row">
                            <div class="input-field col s12">
                                <c:set var="role"><e:formElementValue getterSubmitData="getRole" getterEntity="getRole"/></c:set>

                                <select name="role" id="role" required class="validate">
                                    <c:forEach items="${roleList}" var="role">
                                        <c:if test="${role == roleValue}">
                                            <option value="${role}" selected>${role}</option>
                                        </c:if>

                                        <c:if test="${role != roleValue}">
                                            <option value="${role}">${role}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                                <label for="role"><e:translate message="role"/></label>
                                <e:formElementError name="role"/>
                            </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="waves-effect waves-light btn"><e:translate message="edit"/></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </jsp:body>
</t:admin-layout>
