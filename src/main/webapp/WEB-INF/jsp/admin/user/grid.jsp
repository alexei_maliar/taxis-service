<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:admin-layout>
    <jsp:body>
        <div class="admin-user-container">

            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="users"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col s12 m12">
                    <table class="responsive-table">
                        <tr>
                            <th><e:translate message="id"/></th>
                            <th><e:translate message="email"/></th>
                            <th><e:translate message="role"/></th>
                            <th><e:translate message="registration_date"/></th>
                            <th><e:translate message="actions"/></th>
                        </tr>
                        <c:forEach var="entity" items="${paginator.getItems()}">
                            <tr>
                                <td>
                                    <c:out value="${entity.getIdUser()}"/>
                                </td>
                                <td>
                                    <c:out value="${entity.getEmail()}"/>
                                </td>
                                <td>
                                    <c:out value="${entity.getRole()}"/>
                                </td>
                                <td>
                                    <c:out value="${entity.getRegistrationDate()}"/>
                                </td>
                                <td>
                                    <e:link value="edit">
                                        /admin/user/edit?id_user=${entity.getIdUser()}
                                    </e:link>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <c:if test="${paginator.getPageCount() > 1}">
                <div class="row paginator">
                    <div class="col s0 m4"></div>
                    <div class="col s12 m4">
                        <c:if test="${paginator.getCurrentPage() != 1 && paginator.getCurrentPage() > 1}">
                            <a href="${pageContext.request.contextPath}/admin/user?page=1">
                                <<
                            </a>
                        </c:if>
                        <c:if test="${paginator.getCurrentPage() - 1 > 0}">
                            <a href="${pageContext.request.contextPath}/admin/user?page=${paginator.getCurrentPage() - 1}">
                                    ${paginator.getCurrentPage() - 1}
                            </a>
                        </c:if>
                        <div class="current">
                                ${paginator.getCurrentPage()}
                        </div>
                        <c:if test="${paginator.getCurrentPage() + 1 <= paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/user?page=${paginator.getCurrentPage() + 1}">
                                    ${paginator.getCurrentPage() + 1}
                            </a>
                        </c:if>
                        <c:if test="${paginator.getPageCount() != paginator.getCurrentPage() && paginator.getCurrentPage() < paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/user?page=${paginator.getPageCount()}">
                                >>
                            </a>
                        </c:if>
                    </div>
                    <div class="col s0 m4"></div>
                </div>
            </c:if>
        </div>
    </jsp:body>
</t:admin-layout>