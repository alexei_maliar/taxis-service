<%@ page import="com.maliar.alexei.TaxiService.controller.admin.AbstractAdminController" %>
<%@ page import="com.maliar.alexei.TaxiService.controller.admin.OrderController" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.maliar.alexei.TaxiService.model.Paginator" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<%
    HttpSession reqSession = request.getSession(true);

    Map<String, Object> filterMap =  (reqSession.getAttribute(OrderController.FILTER_KEY) instanceof Map) ? (Map<String, Object>) reqSession.getAttribute(OrderController.FILTER_KEY) : null;
    request.setAttribute("filterMap", filterMap);

    Map<String, String> sortMap = (reqSession.getAttribute(OrderController.SORT_KEY) instanceof Map) ? (Map<String, String>) reqSession.getAttribute(OrderController.SORT_KEY) : null;
    request.setAttribute("sortMap", sortMap);

    String createdDateValue = "";
    String arrivedDateValue = "";
    String completedDateValue = "";

    try {
        if (null != filterMap) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            if (filterMap.containsKey("created_datetime")) {
                createdDateValue = dateFormat.format(filterMap.get("created_datetime"));
            }

            if (filterMap.containsKey("arrived_datetime")) {
                arrivedDateValue = dateFormat.format(filterMap.get("arrived_datetime"));
            }

            if (filterMap.containsKey("completed_datetime")) {
                completedDateValue = dateFormat.format(filterMap.get("completed_datetime"));
            }
        }
    } catch (Exception ignored) {
    }

    request.setAttribute("createdDateValue", createdDateValue);
    request.setAttribute("arrivedDateValue", arrivedDateValue);
    request.setAttribute("completedDateValue", completedDateValue);
%>

<t:admin-layout>
    <jsp:body>
        <div class="admin-order-container">
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="orders"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col m4 s12">
                    <div style="margin: 10px auto; font-size: 18px; color: #a7a796; font-weight: 600;">
                        <e:translate message="filter"/>
                    </div>
                    <form action="${pageContext.request.contextPath}/admin/order/filter" method="get">
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="id_user" id="id_user" class="validate">
                                    <option value="">-</option>
                                    <c:forEach items="${userEntrySet}" var="userEntry">
                                        <c:if test="${null != filterMap && filterMap.containsKey('id_user') && filterMap.get('id_user') == userEntry.getKey()}">
                                            <option selected value="${userEntry.getKey()}">${userEntry.getValue()}</option>
                                        </c:if>

                                        <c:if test="${null == filterMap || !filterMap.containsKey('id_user')}">
                                            <option value="${userEntry.getKey()}">${userEntry.getValue()}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                                <label for="id_user">
                                    <e:translate message="user"/>
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" id="created_datetime" class="datepicker" value="${createdDateValue}" name="created_datetime">
                                <label for="created_datetime">
                                    <e:translate message="created_date"/>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" id="arrived_datetime" class="datepicker" value="${arrivedDateValue}" name="arrived_datetime">
                                <label for="arrived_datetime">
                                    <e:translate message="arrived_datetime"/>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" id="completed_datetime" class="datepicker" value="${completedDateValue}" name="completed_datetime">
                                <label for="completed_datetime">
                                    <e:translate message="completed_datetime"/>
                                </label>
                            </div>
                        </div>

                        <div class="row filter-buttons">
                            <div class="col s12 m6">
                                <e:link className="waves-effect waves-light btn" value="clear" url="/admin/order/filter?clear=true"/>
                            </div>
                            <div class="col s12 m6">
                                <button type="submit" class="waves-effect waves-light btn">
                                    <e:translate message="filter"/>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col m8 s0"></div>
            </div>

            <c:if test="${!paginator.getItems().isEmpty()}">
                <div class="row">
                    <div class="col m2 s12 export-links">
                        <e:link value="export_csv" url="/admin/order/export?type=csv"/>
                        <e:link value="export_pdf" url="/admin/order/export?type=pdf"/>
                    </div>
                    <div class="col m5 s0"></div>
                    <div class="col m5 s0"></div>
                </div>
            </c:if>

            <div class="row">
                <div class="col m12 s12">
                    <table class="responsive-table">
                        <tr>
                            <th>
                                <c:if test="${!sortMap.containsKey('id_order')}">
                                    <a href="${pageContext.request.contextPath}/admin/order/sort?id_order=asc">
                                        <e:translate message="id"/>
                                    </a>
                                </c:if>
                                <c:if test="${sortMap.containsKey('id_order')}">
                                    <c:if test="${sortMap.get('id_order') == 'asc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?id_order=desc">
                                            &#8595; <e:translate message="id"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${sortMap.get('id_order') == 'desc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?id_order=">
                                            &#8593; <e:translate message="id"/>
                                        </a>
                                    </c:if>
                                </c:if>
                            </th>
                            <th><e:translate message="user"/></th>
                            <th><e:translate message="car"/></th>
                            <th><e:translate message="location_from"/></th>
                            <th><e:translate message="location_to"/></th>
                            <th>
                                <c:if test="${!sortMap.containsKey('price')}">
                                    <a href="${pageContext.request.contextPath}/admin/order/sort?price=asc">
                                        <e:translate message="price"/>
                                    </a>
                                </c:if>
                                <c:if test="${sortMap.containsKey('price')}">
                                    <c:if test="${sortMap.get('price') == 'asc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?price=desc">
                                            &#8595; <e:translate message="price"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${sortMap.get('price') == 'desc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?price=">
                                            &#8593; <e:translate message="price"/>
                                        </a>
                                    </c:if>
                                </c:if>
                            </th>
                            <th>
                                <c:if test="${!sortMap.containsKey('discount_price')}">
                                    <a href="${pageContext.request.contextPath}/admin/order/sort?discount_price=asc">
                                        <e:translate message="discount_price"/>
                                    </a>
                                </c:if>
                                <c:if test="${sortMap.containsKey('discount_price')}">
                                    <c:if test="${sortMap.get('discount_price') == 'asc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?discount_price=desc">
                                            &#8595; <e:translate message="discount_price"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${sortMap.get('discount_price') == 'desc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?discount_price=">
                                            &#8593; <e:translate message="discount_price"/>
                                        </a>
                                    </c:if>
                                </c:if>
                            </th>
                            <th><e:translate message="distance"/></th>
                            <th><e:translate message="status"/></th>
                            <th>
                                <c:if test="${!sortMap.containsKey('created_datetime')}">
                                    <a href="${pageContext.request.contextPath}/admin/order/sort?created_datetime=asc">
                                        <e:translate message="created_date"/>
                                    </a>
                                </c:if>
                                <c:if test="${sortMap.containsKey('created_datetime')}">
                                    <c:if test="${sortMap.get('created_datetime') == 'asc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?created_datetime=desc">
                                            &#8595; <e:translate message="created_date"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${sortMap.get('created_datetime') == 'desc'}">
                                        <a href="${pageContext.request.contextPath}/admin/order/sort?created_datetime=">
                                            &#8593; <e:translate message="created_date"/>
                                        </a>
                                    </c:if>
                                </c:if>
                            </th>
                            <th><e:translate message="arrived_date"/></th>
                            <th><e:translate message="completed_date"/></th>
                        </tr>
                        <c:forEach var="entity" items="${paginator.getItems()}">
                            <tr>
                                <td><c:out value="${entity.getIdOrder()}"/></td>
                                <td>
                                    <a href="${pageContext.request.contextPath}//admin/user/edit?id_user=${entity.getIdUser().getIdUser()}">
                                        <c:out value="${entity.getIdUser().getEmail()}"/>
                                    </a>
                                </td>
                                <td>
                                    <a href="${pageContext.request.contextPath}//admin/car/edit?id_car=${entity.getIdCar().getIdCar()}">
                                        <c:out value="${entity.getIdCar().getName()}"/>
                                    </a>
                                </td>
                                <td><c:out value="${entity.getIdLocationFrom().getNameEn()}"/></td>
                                <td><c:out value="${entity.getIdLocationTo().getNameEn()}"/></td>
                                <td><c:out value="${entity.getPrice()}"/></td>
                                <td><c:out value="${entity.getDiscountPrice()}"/></td>
                                <td><c:out value="${entity.getDistance()}"/></td>
                                <td><c:out value="${entity.getStatus()}"/></td>
                                <td><c:out value="${entity.getCreatedDatetime()}"/></td>
                                <td><c:out value="${entity.getArrivedDatetime()}"/></td>
                                <td><c:out value="${entity.getCompletedDatetime()}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

            <c:if test="${paginator.getPageCount() > 1}">
                <div class="row paginator">
                    <div class="col s0 m4"></div>
                    <div class="col s12 m4">
                        <c:if test="${paginator.getCurrentPage() != 1 && paginator.getCurrentPage() > 1}">
                            <a href="${pageContext.request.contextPath}/admin/order?page=1">
                                    <<
                            </a>
                        </c:if>
                        <c:if test="${paginator.getCurrentPage() - 1 > 0}">
                            <a href="${pageContext.request.contextPath}/admin/order?page=${paginator.getCurrentPage() - 1}">
                                ${paginator.getCurrentPage() - 1}
                            </a>
                        </c:if>
                        <div class="current">
                            ${paginator.getCurrentPage()}
                        </div>
                        <c:if test="${paginator.getCurrentPage() + 1 <= paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/order?page=${paginator.getCurrentPage() + 1}">
                                ${paginator.getCurrentPage() + 1}
                            </a>
                        </c:if>
                        <c:if test="${paginator.getPageCount() != paginator.getCurrentPage() && paginator.getCurrentPage() < paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/order?page=${paginator.getPageCount()}">
                                >>
                            </a>
                        </c:if>
                    </div>
                    <div class="col s0 m4"></div>
                </div>
            </c:if>

        </div>
    </jsp:body>
</t:admin-layout>