<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<t:admin-layout>
    <jsp:body>
        <div class="admin-car-container container">
            <div class="row">
                <div class="col s12">
                    <e:link value="back" url="/admin/car"/>
                </div>
            </div>
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="car_add"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <form action="" method="post" name="admin-car-add">
                        <div class="row">
                            <div class="input-field col s12">
                                <c:set var="nameVal"><e:formElementValue getterSubmitData="getName"/></c:set>
                                <input value="${nameVal}" id="name" name="name" type="text" required class="validate">
                                <label for="name"><e:translate message="name"/></label>
                                <e:formElementError name="name"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select name="category" id="category" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <c:forEach items="${categoryList}" var="category">
                                        <option value="${category}">${category}</option>
                                    </c:forEach>
                                </select>
                                <label for="category"><e:translate message="category"/></label>
                                <e:formElementError name="category"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select name="seat_count" id="seat_count" required class="validate">
                                    <option value="" disabled selected>-</option>

                                    <c:forEach var="seat" items="${seatCountList}">
                                        <option value="${seat}">${seat}</option>
                                    </c:forEach>
                                </select>
                                <label for="seat_count"><e:translate message="seat_count"/></label>
                                <e:formElementError name="seat_count"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select name="status" id="status" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <c:forEach items="${statusList}" var="status">
                                        <option value="${status}">${status}</option>
                                    </c:forEach>
                                </select>
                                <label for="category"><e:translate message="status"/></label>
                                <e:formElementError name="status"/>
                            </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="waves-effect waves-light btn"><e:translate message="add"/></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </jsp:body>
</t:admin-layout>