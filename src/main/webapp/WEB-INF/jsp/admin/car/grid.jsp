<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:admin-layout>
    <jsp:body>
        <div class="admin-car-container">
            <div class="row">
                <div class="col m12 s12">
                    <div class="page-title">
                        <e:translate message="cars"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col m12 s12">
                    <e:link value="add" url="/admin/car/add"/>
                </div>
            </div>

            <div class="row">
                <div class="col m12 s12">
                    <table class="responsive-table">
                        <tr>
                            <th><e:translate message="id"/></th>
                            <th><e:translate message="name"/></th>
                            <th><e:translate message="category"/></th>
                            <th><e:translate message="seat_count"/></th>
                            <th><e:translate message="status"/></th>
                            <th><e:translate message="actions"/></th>
                        </tr>
                        <c:forEach var="entity" items="${paginator.getItems()}">
                            <tr>
                                <td>
                                    <c:out value="${entity.getIdCar()}" />
                                </td>
                                <td>
                                    <c:out value="${entity.getName()}" />
                                </td>
                                <td>
                                    <c:out value="${entity.getCategory()}" />
                                </td>
                                <td>
                                    <c:out value="${entity.getStatus()}" />
                                </td>
                                <td>
                                    <c:out value="${entity.getSeatCount()}" />
                                </td>
                                <td>
                                    <e:link value="edit">
                                        /admin/car/edit?id_car=${entity.getIdCar()}
                                    </e:link>
                                    <e:link value="delete">
                                        /admin/car/delete?id_car=${entity.getIdCar()}
                                    </e:link>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

            <c:if test="${paginator.getPageCount() > 1}">
                <div class="row paginator">
                    <div class="col s0 m4"></div>
                    <div class="col s12 m4">
                        <c:if test="${paginator.getCurrentPage() != 1 && paginator.getCurrentPage() > 1}">
                            <a href="${pageContext.request.contextPath}/admin/car?page=1">
                                <<
                            </a>
                        </c:if>
                        <c:if test="${paginator.getCurrentPage() - 1 > 0}">
                            <a href="${pageContext.request.contextPath}/admin/car?page=${paginator.getCurrentPage() - 1}">
                                    ${paginator.getCurrentPage() - 1}
                            </a>
                        </c:if>
                        <div class="current">
                                ${paginator.getCurrentPage()}
                        </div>
                        <c:if test="${paginator.getCurrentPage() + 1 <= paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/car?page=${paginator.getCurrentPage() + 1}">
                                    ${paginator.getCurrentPage() + 1}
                            </a>
                        </c:if>
                        <c:if test="${paginator.getPageCount() != paginator.getCurrentPage() && paginator.getCurrentPage() < paginator.getPageCount()}">
                            <a href="${pageContext.request.contextPath}/admin/car?page=${paginator.getPageCount()}">
                                >>
                            </a>
                        </c:if>
                    </div>
                    <div class="col s0 m4"></div>
                </div>
            </c:if>
        </div>
    </jsp:body>
</t:admin-layout>