<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:layout>
    <jsp:body>
        <div class="filed-container container">
            <div class="row">
                <div class="col m1 s0"></div>
                <div class="col m10 s0 center-align">
                    <div class="page-title">
                        <e:translate message="sorry_could_not_find_any_free_car"/>
                    </div>

                    <div class="info-content">
                        <e:translate message="please_try_again_after_some_time"/>
                    </div>
                <div class="col m1 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>