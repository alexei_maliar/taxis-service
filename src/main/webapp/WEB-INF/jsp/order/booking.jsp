<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<t:layout>
    <jsp:body>
        <div class="booking-container container">
            <div class="row center-align">
                <div class="col m3 s0"></div>
                <div class="col s12 m6">
                    <div class="page-title">
                        <e:translate message="select_trip_criteria"/>
                    </div>
                    <form method="post" action="" name="order-booking-form">
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="id_location_from" id="id_location_from" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <c:forEach items="${locationList}" var="location">
                                        <option value="${location.getIdLocation()}">${location.getNameEn()}</option>
                                    </c:forEach>
                                </select>
                                <label for="id_location_from"><e:translate message="location_from"/></label>
                                <e:formElementError name="id_location_from"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="id_location_to" id="id_location_to" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <c:forEach items="${locationList}" var="location">
                                        <option value="${location.getIdLocation()}">${location.getNameEn()}</option>
                                    </c:forEach>
                                </select>
                                <label for="id_location_to"><e:translate message="location_to"/></label>
                                <e:formElementError name="id_location_to"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select name="seat_count" id="seat_count" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <label for="seat_count"><e:translate message="seat_count"/></label>
                                <e:formElementError name="seat_count"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <select name="category" id="category" required class="validate">
                                    <option value="" disabled selected>-</option>
                                    <c:forEach items="${categoryList}" var="category">
                                        <option value="${category}">${category}</option>
                                    </c:forEach>
                                </select>
                                <label for="category"><e:translate message="category"/></label>
                                <e:formElementError name="category"/>
                            </div>
                        </div>

                        <button type="submit" class="waves-effect waves-light btn"><e:translate message="order"/></button>
                    </form>
                </div>
                <div class="col m3 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>