<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:layout>
    <jsp:body>
        <div class="success-container container">
            <div class="row">
                <div class="col m2 s0"></div>
                <div class="col m8 s0 center-align">
                    <div class="page-title">
                        <c:if test="${order.getStatus() == 'pending'}">
                            <e:translate message="please_wait_your_car"/>
                        </c:if>

                        <c:if test="${order.getStatus() == 'process'}">
                            <e:translate message="you_are_in_cruise"/>
                        </c:if>
                    </div>

                    <div class="info-content">
                        <c:if test="${order.getStatus() == 'pending'}">
                            <e:translate message="approximate_time_waiting"/>
                        </c:if>
                        <c:if test="${order.getStatus() == 'process'}">
                            <e:translate message="you_will_arrive_to_your_location_in"/>
                        </c:if>
                        <strong>
                            ${time}
                            <span class="minute"><e:translate message="minute"/></span>
                        </strong>
                    </div>
                <div class="col m2 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>