<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:layout>
    <jsp:body>
        <div class="similar-container container">
            <div class="row center-align">
                <div class="col m1 s0"></div>
                <div class="col m10 s0">
                    <div class="page-title">
                        <e:translate message="similar_car_page_title"/>
                    </div>

                    <c:if test="${similarCategoryCars.size() > 0}">
                        <div class="cars-wrapper">
                            <div class="title">
                                <e:translate message="similar_car_category_page_description"/>
                            </div>
                            <c:forEach items="${similarCategoryCars}" var="car">
                                <div class="row">
                                    <div class="s10 car-cell">
                                        <div class="value">
                                            <c:out value="${car.getName()}"/>
                                        </div>
                                        <div class="value">
                                            <e:translate message="seat_count"/>
                                            :
                                            <c:out value="${car.getSeatCount()}"/>
                                        </div>
                                        <div class="value">
                                            <e:translate message="category"/>
                                            :
                                            <c:out value="${car.getCategory()}"/>
                                        </div>
                                    </div>
                                    <div class="s10">
                                        <form method="post">
                                            <input type="hidden" name="id_car" value="${car.getIdCar()}"/>
                                            <button class="waves-effect btn" type="submit"><e:translate message="order"/></button>
                                        </form>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>

                    <c:if test="${similarSeatCountCars.size() > 0}">
                        <div class="cars-wrapper">
                            <div class="title">
                                <e:translate message="similar_car_seat_count_page_description"/>
                            </div>
                            <c:forEach items="${similarSeatCountCars}" var="car">
                                <div class="row">
                                    <div class="s10 car-cell">
                                        <div class="value">
                                            <c:out value="${car.getName()}"/>
                                        </div>
                                        <div class="value">
                                            <e:translate message="seat_count"/>
                                            :
                                            <c:out value="${car.getSeatCount()}"/>
                                        </div>
                                        <div class="value">
                                            <e:translate message="category"/>
                                            :
                                            <c:out value="${car.getCategory()}"/>
                                        </div>
                                    </div>
                                    <div class="s10">
                                        <form method="post">
                                            <input type="hidden" name="id_car" value="${car.getIdCar()}"/>
                                            <button class="waves-effect btn" type="submit"><e:translate
                                                    message="order"/></button>
                                        </form>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
                <div class="col m1 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>