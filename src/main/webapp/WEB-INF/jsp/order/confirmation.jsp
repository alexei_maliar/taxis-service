<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<t:layout>
    <jsp:body>
        <div class="confirmation-container container">
            <div class="row">
                <div class="col m1 s0"></div>
                <div class="col m10 s12">
                    <div class="page-title">
                        <e:translate message="please_confirm_your_trip"/>
                    </div>

                    <div class="row">
                        <div class="col s12 m6">
                            <e:translate message="from"/>
                        </div>
                        <div class="col s12 m6">
                            <c:if test="${lang == 'en'}">
                                ${order.getIdLocationFrom().getNameEn()}
                            </c:if>
                            <c:if test="${lang == 'ru'}">
                                ${order.getIdLocationFrom().getNameRu()}
                            </c:if>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12 m6">
                            <e:translate message="to"/>
                        </div>
                        <div class="col s12 m6">
                            <c:if test="${lang == 'en'}">
                                ${order.getIdLocationTo().getNameEn()}
                            </c:if>
                            <c:if test="${lang == 'ru'}">
                                ${order.getIdLocationTo().getNameRu()}
                            </c:if>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12 m6">
                            <e:translate message="distance"/>
                        </div>
                        <div class="col s12 m6">
                                ${order.getDistance()}
                            km
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12 m6">
                            <e:translate message="car"/>
                        </div>
                        <div class="col s12 m6">
                                ${order.getIdCar().getName()}
                            (${order.getIdCar().getCategory()})
                        </div>
                    </div>

                    <div class="row price">
                        <div class="col s12 m6">
                            <e:translate message="price"/>
                        </div>
                        <div class="col s12 m6">
                                ${order.getPrice()}
                            <c:if test="${order.getDiscountPrice() > 0}">
                                <span class="discount">
                                    <e:translate message="discount"/>
                                    ${order.getDiscountPrice()}
                                </span>
                            </c:if>
                        </div>
                    </div>

                    <div class="row price">
                        <div class="col s12 m6">
                            <e:translate message="approximate_time_waiting"/>
                        </div>
                        <div class="col s12 m6" style="text-transform: lowercase;">
                                ${time} <e:translate message="minute"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12 m6">
                            <e:link value="cancel" url="/order/booking" className="waves-effect btn"/>
                        </div>
                        <div class="col s12 m6">
                            <form action="" method="post">
                                <button type="submit" class="waves-effect waves-light btn">
                                    <e:translate message="confirm"/>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col m1 s0"></div>
            </div>

        </div>
    </jsp:body>
</t:layout>