<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<t:layout>
    <jsp:body>
        <div class="index-container">
            <div class="row">
                <div class="col m2 s0"></div>
                <div class="col m8 s12">
                    <e:link value="order_your_trip_now" url="/order/booking" className="book-cell"/>
                </div>
                <div class="col m2 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>