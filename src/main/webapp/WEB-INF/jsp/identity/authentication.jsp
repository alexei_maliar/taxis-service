<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<t:layout>
    <jsp:body>
        <div class="authentication-container container">
            <div class="row center-align">
                <div class="col m3 s0"></div>
                <div class="col m6 s12">
                    <div class="page-title">
                        <e:translate message="please_sign_in"/>
                    </div>
                    <form method="post" action="" name="identity-authentication-form">
                        <div class="row">
                            <c:set var="emailVal"><e:formElementValue getterSubmitData="getEmail"/></c:set>
                            <div class="input-field col s12">
                                <input value="${emailVal}" id="email" name="email" type="email" required class="validate">
                                <label for="email"><e:translate message="email"/></label>
                                <e:formElementError name="email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password" name="password" required class="validate" value="">
                                <label for="password"><e:translate message="password"/></label>
                                <e:formElementError name="password"/>
                            </div>
                        </div>

                        <div class="row">
                            <e:link value="sign_up" url="/identity/registration"/>
                        </div>
                        <button type="submit" class="waves-effect btn"><e:translate message="sign_in"/></button>
                    </form>
                </div>
                <div class="col m3 s0"></div>
            </div>
        </div>
    </jsp:body>
</t:layout>