<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<t:layout>
    <jsp:body>
        <div class="registration-container container">
            <div class="row">
                <div class="page-backlink">
                    <e:link value="back" url="/identity/authentication"/>
                </div>
            </div>
            <div class="row center-align">
                <div class="col m3 s0"></div>
                <div class="col m6 s12">
                    <div class="page-title">
                        <e:translate message="fill_for_sign_up"/>
                    </div>
                    <form method="post" action="" name="identity-registration-form">
                        <div class="row">
                            <div class="input-field col s12">
                                <c:set var="emailVal"><e:formElementValue getterSubmitData="getEmail"/></c:set>
                                <input value="${emailVal}" id="email" name="email" type="email" required class="validate">
                                <label for="email">Email</label>
                                <e:formElementError name="email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" type="password" name="password" required class="validate" value="">
                                <label for="password"><e:translate message="password"/></label>
                                <e:formElementError name="password"/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input id="repeat_password" type="password" name="repeat_password" required class="validate" value="">
                                <label for="repeat_password"><e:translate message="repeat_password"/></label>
                            </div>
                        </div>
                        <button type="submit" class="waves-effect btn"><e:translate message="sign_up"/></button>
                    </form>
                </div>
                <div class="col m3 s0"></div>
            </div>
        </div>
        <script>
            $('form[name="identity-registration-form"]').on('submit', e => {
                const data = new FormData(e.currentTarget);

                if (data.get("password") !== data.get("repeat_password")) {
                    $('#repeat_password').addClass("invalid");
                    e.preventDefault();
                }
            });
        </script>
    </jsp:body>
</t:layout>