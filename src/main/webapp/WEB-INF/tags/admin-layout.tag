<%@tag description="layout" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/custom-taglib.tld" prefix="e" %>

<!DOCTYPE html>
<html>
<c:set var="context" value="${pageContext.request.contextPath}" />
<head>
    <title>Taxi service</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <jsp:include page="/styleAndScriptLoader.jsp"></jsp:include>
</head>
<body>
<nav class="indigo darken-4" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">
        <e:translate message="admin_panel"/>
    </a>
        <ul class="right hide-on-med-and-down">
            <li>
                <e:link value="orders" url="/admin/order"/>
            </li>
            <li>
                <e:link value="users" url="/admin/user"/>
            </li>
            <li>
                <e:link value="cars" url="/admin/car"/>
            </li>
            <li>
                <e:link value="front" url="/"/>
            </li>
            <li>
                <e:link value="logout" url="/identity/logout"/>
            </li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
<main class="main-container admin-container">
    <e:notice/>
    <jsp:doBody/>
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>