<%@ tag import="com.maliar.alexei.TaxiService.service.MultiLanguageProvider" %>
<%@ tag import="com.maliar.alexei.TaxiService.di.ServiceContainer" %>
<%@tag description="layout" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="e" uri="/WEB-INF/custom-taglib.tld" %>

<!DOCTYPE html>
<html>
<head>
    <title><e:translate message="taxi_service"/></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <jsp:include page="/styleAndScriptLoader.jsp"></jsp:include>
</head>
<body>
<nav class="green accent-4" role="navigation">
    <ul id="dropdown1" class="dropdown-content">
        <c:if test="${null != user && user.getRole() == 'admin'}">
            <li>
                <e:link value="admin_panel" url="/admin"/>
            </li>
        </c:if>

        <c:if test="${null != user}">
            <li><e:link value="logout" url="/identity/logout"/></li>
        </c:if>
    </ul>
    <div class="nav-wrapper container"><a id="logo-container" href="${pageContext.request.contextPath}/" class="brand-logo"><e:translate message="taxi_service"/></a>
        <ul class="right hide-on-med-and-down">
            <li>
                <c:if test="${lang == 'ru'}">
                    <e:link value="en" url="/en"/>
                </c:if>
                <c:if test="${lang == 'en'}">
                    <e:link value="ru" url="/ru"/>
                </c:if>
                <c:if test="${lang == null}">
                    <e:link value="ru" url="/ru"/>
                </c:if>
            </li>
            <c:if test="${null != user}">
                <li>
                    <a class="dropdown-trigger" href="#!" data-target="dropdown1">
                        <c:out value="${user.getEmail()}"/>
                        <i class="material-icons right">arrow_drop_down</i>
                    </a>
                </li>
            </c:if>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li>
                <c:if test="${lang == 'ru'}">
                    <e:link value="en" url="/en"/>
                </c:if>
                <c:if test="${lang == 'en'}">
                    <e:link value="ru" url="/ru"/>
                </c:if>
            </li>
            <c:if test="${null != user && user.getRole() == 'admin'}">
                <li>
                    <e:link value="admin_panel" url="/admin"/>
                </li>
            </c:if>

            <c:if test="${null != user}">
                <li><e:link value="logout" url="/identity/logout"/></li>
            </c:if>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
<main class="main-container container">
    <e:notice/>

    <jsp:doBody/>
</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>