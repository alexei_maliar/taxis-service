package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.di.ConfigParser;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.model.dao.CarDao;
import com.maliar.alexei.TaxiService.model.dao.LocationDao;
import com.maliar.alexei.TaxiService.model.dao.OrderDao;
import com.maliar.alexei.TaxiService.model.dto.OrderDto;
import com.maliar.alexei.TaxiService.model.entity.*;
import com.maliar.alexei.TaxiService.model.mapper.OrderObjectMapper;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;
import com.maliar.alexei.TaxiService.service.secure.AccessProviderImpl;
import com.maliar.alexei.TaxiService.service.secure.Principal;
import com.maliar.alexei.TaxiService.service.trip.exception.CarNotFoundException;
import com.maliar.alexei.TaxiService.service.trip.exception.UnavailableOrderCreateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class OrderMakerImplTest {
    static ServiceContainer serviceContainer;

    static LocationDao locationDaoMock;

    static CarDao carDaoMock;

    static OrderDao orderDaoMock;

    static TripCalculator tripCalculator;

    static AccessProvider accessProviderMock;

    static OrderMaker orderMaker;

    static OrderObjectMapper orderObjectMapperMock;

    @BeforeAll
    public static void setUp() {
        ConfigRegistry.setDiConfigFilename("dependencies.xml");
        ConfigRegistry.setAccessConfigFilename("access.xml");
        serviceContainer = ServiceContainer.getInstance();

        serviceContainer.setConfigMap(ConfigParser.parse());

        locationDaoMock = Mockito.mock(LocationDao.class);
        carDaoMock = Mockito.mock(CarDao.class);
        orderDaoMock = Mockito.mock(OrderDao.class);
        accessProviderMock = Mockito.mock(AccessProvider.class);
        orderObjectMapperMock = Mockito.mock(OrderObjectMapper.class);

        serviceContainer.set(DBManager.class.getName(), Mockito.mock(DBManager.class));

        TripCalculator tripCalculator = new TripCalculatorImpl();
        tripCalculator.setAverageSpeed(50);
        tripCalculator.setTariffRate(2.25F);

        serviceContainer.set(LocationDao.class.getName(), locationDaoMock);
        serviceContainer.set(CarDao.class.getName(), carDaoMock);
        serviceContainer.set(OrderDao.class.getName(), orderDaoMock);
        serviceContainer.set(TripCalculator.class.getName(), tripCalculator);
        serviceContainer.set(AccessProvider.class.getName(), accessProviderMock);
        serviceContainer.set(OrderObjectMapper.class.getName(), orderObjectMapperMock);

        orderMaker = (OrderMaker) serviceContainer.get(OrderMaker.class.getName());
    }

    @Test
    public void shouldReturnAvailableLocations() {
        List<Entity> locationsList = new ArrayList<>();
        Location location = new Location();
        location.setIdLocation(5);
        locationsList.add(location);

        Mockito.when(locationDaoMock.selectAll()).thenReturn(locationsList);

        Assertions.assertEquals(locationsList, orderMaker.getAvailableLocationList());
    }

    @Test
    public void shouldReturnCorrespondingCar() throws CarNotFoundException {
        Car car = new Car();
        car.setIdCar(5);
        car.setCategory(Car.Category.economy);
        car.setSeatCount(5);

        Mockito.when(carDaoMock.selectCorrespondingCar(5, "economy")).thenReturn(car);
        Mockito.when(carDaoMock.select(5)).thenReturn(car);

        OrderDto orderDto = new OrderDto();
        orderDto.setSeatCount(5);
        orderDto.setCategory("economy");

        Assertions.assertEquals(car, orderMaker.findCorrespondingCar(orderDto));
        Assertions.assertEquals(car, orderMaker.findCorrespondingCar(5));
    }

    @Test
    public void shouldThrowsExceptionWhenCorrespondingCarDoesNotExists() {
        Mockito.when(carDaoMock.selectCorrespondingCar(3, "economy")).thenReturn(null);

        Assertions.assertThrows(CarNotFoundException.class, () -> {
            OrderDto orderDto = new OrderDto();

            orderDto.setCategory("economy");
            orderDto.setSeatCount(3);
            orderMaker.findCorrespondingCar(orderDto);
        });

        Mockito.when(carDaoMock.select(4)).thenReturn(null);

        Assertions.assertThrows(CarNotFoundException.class, () -> {
            orderMaker.findCorrespondingCar(4);
        });
    }

    @Test
    public void shouldFindSimilarCars() throws CarNotFoundException {
        List<Entity> similarCars = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Car car = new Car();
            car.setIdCar(i);
            car.setSeatCount(i);
            car.setCategory(Car.Category.comfort);

            similarCars.add(car);
        }

        Mockito.when(carDaoMock.selectSimilarCars(2, "economy")).thenReturn(similarCars);

        OrderDto orderDto = new OrderDto();
        orderDto.setSeatCount(2);
        orderDto.setCategory("economy");

        Assertions.assertEquals(similarCars, orderMaker.findSimilarCars(orderDto));
    }

    @Test
    public void shouldThrowsExceptionWhenSimilarCarsAbsent() {
        Mockito.when(carDaoMock.selectSimilarCars(1, "economy")).thenReturn(new ArrayList<>());

        OrderDto orderDto = new OrderDto();
        orderDto.setSeatCount(1);
        orderDto.setCategory("economy");

        Assertions.assertThrows(CarNotFoundException.class, () -> {
            orderMaker.findSimilarCars(orderDto);
        });
    }

    @Test
    public void shouldMakeOrder() throws UnavailableOrderCreateException {
        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);

        User user = new User();
        user.setIdUser(5);

        Principal principalMock = Mockito.mock(Principal.class);
        Mockito.when(principalMock.getUser()).thenReturn(user);

        Mockito.when(accessProviderMock.getPrincipal(requestMock)).thenReturn(principalMock);

        Car car = new Car();
        car.setIdCar(5);
        car.setStatus(Car.Status.pending_order);

        Mockito.when(carDaoMock.select(car.getIdCar())).thenReturn(car);

        Location locationFrom = new Location();
        locationFrom.setIdLocation(5);

        Location locationTo = new Location();
        locationFrom.setIdLocation(6);

        Mockito.when(locationDaoMock.select(locationFrom.getIdLocation())).thenReturn(locationFrom);
        Mockito.when(locationDaoMock.select(locationTo.getIdLocation())).thenReturn(locationTo);

        Mockito.when(orderDaoMock.selectTotalMoneySpentForUser(user.getIdUser())).thenReturn(1000D);

        OrderDto orderDto = new OrderDto();
        orderDto.setIdLocationFrom(locationFrom.getIdLocation());
        orderDto.setIdLocationTo(locationTo.getIdLocation());

        Order order = new Order();
        Mockito.when(orderObjectMapperMock.entityFromDto(orderDto)).thenReturn(order);

        Assertions.assertTrue(orderMaker.makeOrder(orderDto, car, requestMock) instanceof Order);
    }

    @Test
    public void shouldMakeOrderThrowsException() {
        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);

        User user = new User();
        user.setIdUser(5);

        Principal principalMock = Mockito.mock(Principal.class);
        Mockito.when(principalMock.getUser()).thenReturn(user);

        Mockito.when(accessProviderMock.getPrincipal(requestMock)).thenReturn(principalMock);

        Car car = new Car();
        car.setIdCar(5);
        car.setStatus(Car.Status.pending_order);

        Mockito.when(carDaoMock.select(car.getIdCar())).thenReturn(car);

        Location locationFrom = new Location();
        locationFrom.setIdLocation(5);

        Location locationTo = new Location();
        locationFrom.setIdLocation(6);

        Mockito.when(locationDaoMock.select(locationFrom.getIdLocation())).thenReturn(null);
        Mockito.when(locationDaoMock.select(locationTo.getIdLocation())).thenReturn(locationTo);

        Mockito.when(orderDaoMock.selectTotalMoneySpentForUser(user.getIdUser())).thenReturn(1000D);

        OrderDto orderDto = new OrderDto();
        orderDto.setIdLocationTo(locationTo.getIdLocation());
        orderDto.setIdLocationFrom(locationFrom.getIdLocation());

        Order order = new Order();
        Mockito.when(orderObjectMapperMock.entityFromDto(orderDto)).thenReturn(order);

        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.makeOrder(orderDto, car, requestMock);
        });

        Mockito.when(locationDaoMock.select(locationFrom.getIdLocation())).thenReturn(locationFrom);
        Mockito.when(locationDaoMock.select(locationTo.getIdLocation())).thenReturn(null);

        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.makeOrder(orderDto, car, requestMock);
        });

        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.makeOrder(orderDto, null, requestMock);
        });
    }

    @Test
    public void shouldSaveOrder() throws UnavailableOrderCreateException {
        Order order = new Order();
        Mockito.doAnswer(i -> {
           Order orderArg = i.getArgument(0);
           orderArg.setIdOrder(1);
           return null;
        }).when(orderDaoMock).insert(order);

        orderMaker.saveOrder(order);
    }

    @Test
    public void shouldSaveOrderThrowsException() {
        Order order = new Order();

        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.saveOrder(null);
        });

        order.setIdOrder(12);
        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.saveOrder(order);
        });

        Mockito.doAnswer(i -> {
            Order orderArg = i.getArgument(0);
            orderArg.setIdOrder(0);
            return null;
        }).when(orderDaoMock).insert(order);

        order.setIdOrder(0);
        Assertions.assertThrows(UnavailableOrderCreateException.class, () -> {
            orderMaker.saveOrder(order);
        });
    }

    @Test
    public void shouldReturnActiveOrder() {
        User user = new User();
        user.setIdUser(1);

        Principal principalMock = Mockito.mock(Principal.class);
        Mockito.when(principalMock.getUser()).thenReturn(user);

        HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);

        Mockito.when(accessProviderMock.getPrincipal(requestMock)).thenReturn(principalMock);

        Mockito.when(orderDaoMock.selectActiveOrderForUser(1)).thenReturn(new Order());
        Mockito.when(orderDaoMock.selectActiveOrderForUser(2)).thenReturn(null);

        Assertions.assertTrue(orderMaker.getActiveOrder(requestMock) instanceof Order);

        user.setIdUser(2);

        Assertions.assertNull(orderMaker.getActiveOrder(requestMock));
    }
}
