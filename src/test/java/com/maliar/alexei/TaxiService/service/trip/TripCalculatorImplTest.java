package com.maliar.alexei.TaxiService.service.trip;

import com.maliar.alexei.TaxiService.model.entity.Car;
import com.maliar.alexei.TaxiService.model.entity.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class TripCalculatorImplTest {
    final static float TARIFF_RATE = 3;
    final static int AVERAGE_SPEED = 50;

    static TripCalculator tripCalculator;

    @BeforeAll
    public static void setUp() {
        tripCalculator = new TripCalculatorImpl();
        tripCalculator.setTariffRate(TARIFF_RATE);
        tripCalculator.setAverageSpeed(AVERAGE_SPEED);
    }

    @Test
    public void shouldCalculatePrice() {
        double distance = 225.25;

        double expected = Math.round(distance * TARIFF_RATE * 1.35F * 100.00) / 100.00;

        Assertions.assertEquals(expected, tripCalculator.calculatePrice(distance, Car.Category.comfort));
    }

    @Test
    public void shouldCalculateDiscountPrice() {
        double ex = Math.round(0.1 * 120 * 100.00) / 100.00;

        Assertions.assertEquals(ex, tripCalculator.calculateDiscountPrice(500, 120));
    }

    @Test
    public void shouldCalculateDistance() {
        Location locationFrom = new Location();
        Location locationTo = new Location();

        locationTo.setY(10);
        locationTo.setX(9);

        locationFrom.setY(9);
        locationFrom.setX(15);

        double ex = Math.abs(locationTo.getY() - locationFrom.getY()) + Math.abs(locationFrom.getX() - locationTo.getX());

        Assertions.assertEquals(ex, tripCalculator.calculateDistance(locationFrom, locationTo));

        locationFrom.setX(0);
        locationFrom.setY(0);

        locationTo.setX(0);
        locationTo.setY(0);

        Assertions.assertEquals(1, tripCalculator.calculateDistance(locationFrom, locationTo));
    }

    @Test
    public void shouldCalculateDate() {
        double distance = 4321;
        double speed = 1.0 * tripCalculator.getAverageSpeed() / 60;
        double time = distance / speed;

        Date date = new Date();
        Date ex = new Date((long) (date.getTime() + (time * 60000)));

        Assertions.assertEquals(ex, tripCalculator.calculateDate(distance, date));
    }
}
