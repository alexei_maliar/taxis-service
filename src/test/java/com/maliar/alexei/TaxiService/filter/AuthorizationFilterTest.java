package com.maliar.alexei.TaxiService.filter;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.di.ConfigParser;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.service.secure.AccessProviderImpl;
import com.maliar.alexei.TaxiService.service.secure.Principal;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthorizationFilterTest {
    static HttpServletRequest request;

    @BeforeClass
    public static void setUp() {
        ConfigRegistry.setDiConfigFilename("dependencies.xml");
        ConfigRegistry.setAccessConfigFilename("access.xml");

        ServiceContainer serviceContainer = ServiceContainer.getInstance();
        serviceContainer.setConfigMap(ConfigParser.parse());

        serviceContainer.set(DBManager.class.getName(), Mockito.mock(DBManager.class));

        request = Mockito.mock(HttpServletRequest.class);

        Mockito.when(request.getRequestURI()).thenReturn("/cp/admin");
        Mockito.when(request.getContextPath()).thenReturn("/cp");
    }

    @Test
    public void shouldRedirectWhenUserDoesNotHavePrivileges() throws IOException, ServletException {
        HttpSession session = Mockito.mock(HttpSession.class);
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getRole()).thenReturn("user");

        Mockito.when(session.getAttribute(AccessProviderImpl.SESSION_PRINCIPAL_KEY)).thenReturn(principal);
        Mockito.when(request.getSession(true)).thenReturn(session);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        String endpoint = request.getContextPath() + "/";
        Mockito.doThrow(RedirectException.class).when(response).sendRedirect(endpoint);

        AuthorizationFilter authorizationFilter = new AuthorizationFilter();

        Assertions.assertThrows(RedirectException.class, () -> {
            authorizationFilter.doFilter(request, response, Mockito.mock(FilterChain.class));
        });
    }

    @Test
    public void shouldDoNothingWhenUserHasPrivileges() throws IOException, ServletException {
        HttpSession session = Mockito.mock(HttpSession.class);
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getRole()).thenReturn("admin");

        Mockito.when(session.getAttribute(AccessProviderImpl.SESSION_PRINCIPAL_KEY)).thenReturn(principal);
        Mockito.when(request.getSession(true)).thenReturn(session);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        String endpoint = request.getContextPath() + "/";
        Mockito.doThrow(RedirectException.class).when(response).sendRedirect(endpoint);

        AuthorizationFilter authorizationFilter = new AuthorizationFilter();

        authorizationFilter.doFilter(request, response, Mockito.mock(FilterChain.class));
    }

    static class RedirectException extends RuntimeException {

    }
}
