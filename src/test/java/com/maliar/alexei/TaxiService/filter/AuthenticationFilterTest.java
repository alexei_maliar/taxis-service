package com.maliar.alexei.TaxiService.filter;

import com.maliar.alexei.TaxiService.ConfigRegistry;
import com.maliar.alexei.TaxiService.di.ConfigParser;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.model.DBManager;
import com.maliar.alexei.TaxiService.service.secure.AccessProvider;
import com.maliar.alexei.TaxiService.service.secure.AccessProviderImpl;
import com.maliar.alexei.TaxiService.service.secure.Principal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilterTest {
    @Test
    public void shouldRedirectWhenUserUnauthenticated() throws IOException, ServletException {
        ConfigRegistry.setDiConfigFilename("dependencies.xml");
        ConfigRegistry.setAccessConfigFilename("access.xml");

        ServiceContainer serviceContainer = ServiceContainer.getInstance();
        serviceContainer.setConfigMap(ConfigParser.parse());

        DBManager dbManagerMock = Mockito.mock(DBManager.class);

        serviceContainer.set(DBManager.class.getName(), dbManagerMock);

        AccessProvider accessProvider = (AccessProvider) serviceContainer.get(AccessProvider.class.getName());

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("/cp/admin");
        Mockito.when(request.getContextPath()).thenReturn("/cp");

        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(request.getSession(true)).thenReturn(session);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        String authEndpoint = request.getContextPath() + accessProvider.getAuthenticationEndpoint();
        Mockito.doThrow(AuthEndpointRedirectException.class).when(response).sendRedirect(authEndpoint);

        AuthenticationFilter authenticationFilter = new AuthenticationFilter();

        FilterChain filterChainMock = Mockito.mock(FilterChain.class);

        Assertions.assertThrows(AuthEndpointRedirectException.class , () -> {
            authenticationFilter.doFilter(request, response, filterChainMock);
        });
    }

    @Test
    public void shouldRedirectWhenUserAuthenticated() throws IOException {
        ConfigRegistry.setDiConfigFilename("dependencies.xml");
        ConfigRegistry.setAccessConfigFilename("access.xml");

        ServiceContainer serviceContainer = ServiceContainer.getInstance();
        serviceContainer.setConfigMap(ConfigParser.parse());

        DBManager dbManagerMock = Mockito.mock(DBManager.class);

        serviceContainer.set(DBManager.class.getName(), dbManagerMock);

        AccessProvider accessProvider = (AccessProvider) serviceContainer.get(AccessProvider.class.getName());

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);

        Mockito.when(request.getRequestURI()).thenReturn("/cp" + accessProvider.getAuthenticationEndpoint());
        Mockito.when(request.getContextPath()).thenReturn("/cp");

        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(session.getAttribute(AccessProviderImpl.SESSION_PRINCIPAL_KEY)).thenReturn(Mockito.mock(Principal.class));
        Mockito.when(request.getSession(true)).thenReturn(session);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        String endpoint = request.getContextPath() + "/";
        Mockito.doThrow(IndexEndpointRedirectException.class).when(response).sendRedirect(endpoint);

        AuthenticationFilter authenticationFilter = new AuthenticationFilter();

        FilterChain filterChainMock = Mockito.mock(FilterChain.class);

        Assertions.assertThrows(IndexEndpointRedirectException.class , () -> {
            authenticationFilter.doFilter(request, response, filterChainMock);
        });
    }

    static class IndexEndpointRedirectException extends RuntimeException {

    }

    static class AuthEndpointRedirectException extends RuntimeException {

    }
}
