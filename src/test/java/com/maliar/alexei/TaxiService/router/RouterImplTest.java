package com.maliar.alexei.TaxiService.router;

import com.maliar.alexei.TaxiService.controller.ActionController;
import com.maliar.alexei.TaxiService.di.ServiceContainer;
import com.maliar.alexei.TaxiService.exception.ControllerNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class RouterImplTest {
    @Test
    public void shouldMatchController() {
        ServiceContainer serviceContainer = Mockito.mock(ServiceContainer.class);
        Mockito.when(serviceContainer.get("testController")).thenReturn(Mockito.mock(TestController.class));

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("/contextPath/test");
        Mockito.when(request.getContextPath()).thenReturn("/contextPath");

        Map<Object, Object> routesMap = new HashMap<>();
        routesMap.put("/test", "testController");

        Router router = new RouterImpl(serviceContainer, routesMap);

        Assertions.assertTrue(router.match(request) instanceof TestController);
    }

    @Test
    public void shouldThrowsExceptionWhenPathNotFound() {
        ServiceContainer serviceContainer = Mockito.mock(ServiceContainer.class);
        Mockito.when(serviceContainer.get("testController")).thenReturn(Mockito.mock(TestController.class));

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        Mockito.when(request.getRequestURI()).thenReturn("/contextPath/test123");
        Mockito.when(request.getContextPath()).thenReturn("/contextPath");

        Map<Object, Object> routesMap = new HashMap<>();
        routesMap.put("/test", "testController");

        Router router = new RouterImpl(serviceContainer, routesMap);

        Assertions.assertThrows(ControllerNotFoundException.class, () -> {
            router.match(request);
        });
    }

    public static class TestController extends ActionController {
        public TestController(ServletContext servletContext) {
            super(servletContext);
        }
    }
}
