package com.maliar.alexei.TaxiService.di;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ServiceContainerImplTest {
    @BeforeEach
    public void clearServiceContainer() {
        ServiceContainer serviceContainer = ServiceContainer.getInstance();

        Map<Object, Object> configMap = new HashMap<>();
        configMap.put(SomeService.class.getName(), SomeServiceFactory.class.getName());

        serviceContainer.setConfigMap(configMap);
    }

    @Test
    public void shouldThrowsExceptionWhenConfigNotPassed() {
        ServiceContainer serviceContainer = ServiceContainerImpl.getInstance();

        try {
            Field field = serviceContainer.getClass().getDeclaredField("configMap");
            field.setAccessible(true);
            field.set(serviceContainer, null);

            Exception exception = Assertions.assertThrows(IllegalStateException.class, () -> {
                serviceContainer.get(SomeServiceFactory.class.getName());
            });

            Assertions.assertTrue(exception.getMessage().contains("Config map should be initialized"));
        } catch (Exception ignored) {
        }
    }

    @Test
    public void shouldReturnRightDependency() {
        ServiceContainer serviceContainer = ServiceContainerImpl.getInstance();

        Assertions.assertTrue(serviceContainer.get(SomeService.class.getName()) instanceof SomeService);
    }

    @Test
    public void shouldThrowsExceptionWhenServiceNotExists() {
        ServiceContainer serviceContainer = ServiceContainerImpl.getInstance();

        Map<Object, Object> configMap = new HashMap<>();
        configMap.put(SomeService.class.getName(), SomeServiceFactory.class.getName());

        serviceContainer.setConfigMap(configMap);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            serviceContainer.get(SomeServiceFactory.class.getName());
        });
    }

    @Test
    public void shouldSetCorrectDependency() {
        SomeServiceFactory serviceFactory = new SomeServiceFactory();

        ServiceContainer serviceContainer = ServiceContainer.getInstance();

        serviceContainer.set("test", serviceFactory);

        Assertions.assertEquals(serviceFactory, serviceContainer.get("test"));
    }

    @Test
    public void shouldRemoveCorrectDependency() {
        SomeServiceFactory serviceFactory = new SomeServiceFactory();

        ServiceContainer serviceContainer = ServiceContainer.getInstance();

        serviceContainer.set("test", serviceFactory);

        Assertions.assertEquals(serviceFactory, serviceContainer.get("test"));

        serviceContainer.remove("test");

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            serviceContainer.get("test");
        });
    }

    public static class SomeService {
    }

    public static class SomeServiceFactory implements ServiceFactory {
        @Override
        public Object factory(Object requestedKey, ServiceContainer serviceContainer) {
            return new SomeService();
        }
    }
}
